<html>

<?php 
//include('head.php');
?>

<body>
<h2>Modal Example</h2>


<div class="container w-full mt-5">
    <div>

         <!-- Trigger/Open The Modal -->
         <button id="myBtn" class="btn btn-rounded btn-primary">Open Modal</button>

        <!-- The Modal -->
        <div id="myModal" class="modal">

            <!-- Modal content -->
            <div class="modal-content flex">
                
                <div class="w-11/12 mt-3">
                    <?php 
                    include('modal2.php');
                    ?>
                </div>

                <div class="w-1/12 justify-end flex">
                    <span class="close">&times;</span>
                </div>
               

            </div>

        </div>

    </div>
</div>



<script>
    // Get the modal
    var modal = document.getElementById("myModal");

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks the button, open the modal 
    btn.onclick = function() {
    modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
    modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
    }
</script>

</body>
</html>