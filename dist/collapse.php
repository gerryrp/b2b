    <body>
            <button class="collapsible btn btn-sm btn-primary">Open Collapsible</button>
            <div class="collapsecontent">
                
                <div>
                    Price
                    <input type="text" class="input">
                    
                </div>

                <div class="container">
                    <div class="flex md-4">
                        <div class="w-1/3">
                            Filter Resort
                        </div>

                        <div class="w-1/3">
                            <p>Filter Meals</p>

                            <div class="w-full  p-4">
                                <div class="my-2">
                                    <input type="checkbox" class="check" id="Check11"  data-label="to">
                                        <label class="span span-blue1" for="Check11">
                                            No Meals
                                        </label>          
                                </div>
                
                                <div class="my-2">
                                    <input type="checkbox" class="check" id="Check12"  data-label="to">
                                        <label class="span span-blue1" for="Check12">
                                            Breakfast
                                        </label>                              
                                </div>
            
                                <div class="my-2">
                                    <input type="checkbox" class="check" id="Check13"  data-label="to">
                                        <label class="span span-blue1" for="Check13">
                                            Half Board
                                        </label>                              
                                </div>
                
                                <div class="my-2">
                                    <input type="checkbox" class="check" id="Check14"  data-label="to">
                                        <label class="span span-blue1" for="Check14">
                                            Full Board
                                        </label>                              
                                </div>
            
                                <div class="my-2">
                                    <input type="checkbox" class="check" id="Check15"  data-label="to">
                                        <label class="span span-blue1" for="Check15">
                                            All inclusive
                                        </label>                              
                                </div>
                
                        </div>
                    </div>

                    <div class="w-1/3">
                        <p>Filter Stars</p> 

                        <div class="w-full  p-4">
                            <div class="my-2">
                                <input type="checkbox" class="check" id="Check21"  data-label="to">
                                    <label class="span span-blue1" for="Check21">
                                        <i class="fas fa-star"></i>
                                    </label>          
                            </div> 
        
                            <div class="my-2">
                                <input type="checkbox" class="check" id="Check22"  data-label="to">
                                    <label class="span span-blue1" for="Check22">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </label>                              
                            </div>
        
                            <div class="my-2">
                                <input type="checkbox" class="check" id="Check23"  data-label="to">
                                    <label class="span span-blue1" for="Check23">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </label>                              
                            </div>
        
                            <div class="my-2">
                                <input type="checkbox" class="check" id="Check24"  data-label="to">
                                    <label class="span span-blue1" for="Check24">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </label>                              
                            </div>
        
                            <div class="my-2">
                                <input type="checkbox" class="check" id="Check25"  data-label="to">
                                    <label class="span span-blue1" for="Check25">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </label>                              
                            </div>
        
                        </div>
                    </div>

                </div>
                
                      
            </div>


            <script>
                var coll = document.getElementsByClassName("collapsible");
                var i;

                for (i = 0; i < coll.length; i++) {
                coll[i].addEventListener("click", function() {
                    this.classList.toggle("active");
                    var content = this.nextElementSibling;
                    if (content.style.display === "block") {
                    content.style.display = "none";
                    } else {
                    content.style.display = "block";
                    }
                });
                }
            </script>

    </body>