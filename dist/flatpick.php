<html>
        <?php 
        //include('head.php');
        ?>

    <body>

        <div class="w-1/2 mt-10 mx-auto ">
            <span>basic</span>
            <div>
                <input id="datepickr" type="text" class="flatpickr flatpickr-input input input-lg " placeholder="select date">
                <i class="far fa-calendar-alt"></i>
            </div>
        </div>

        <div class="w-1/2 mt-10 mx-auto ">
            <span>range</span>
            <div>
                <input id="datepickr2" type="text" class="flatpickr flatpickr-input input input-lg " placeholder="select date">
                <i class="far fa-calendar-alt"></i>
            </div>
            
        </div>
        


        <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
        <script> 
            var dateelement=document.getElementById("datepickr") 
            dateelement.flatpickr({})

            var dateelement=document.getElementById("datepickr2") 
            dateelement.flatpickr({  altInput: true,
                                    altFormat: "F j, Y",
                                    dateFormat: "Y-m-d",
                                    minDate: "today",
                                    mode: "range"
                                })    
        </script>
    </body>

</html>