<html>
        <?php 
        include('head.php');
        ?> 
        
        
    <body>

        <div class="container border-b-4 border-solid border-gray-400">
            <div class="flex justify-center">
                <span class="font-bold text-xl"><h2>DATE PICKER</h2></span>
            </div>  

            <div>
                <?php 
                    include('flatpick.php');
                ?>
            </div>
        </div>  

        <!-- <div class="container border-b-4 border-solid border-gray-400">
            <div class="flex justify-center">
                <span class="font-bold text-xl"><h2>SIDE MENU</h2></span>
            </div>
            
            <div>
                <?php 
                    // include('sidemenu.php');
                ?>
            </div>
        </div> -->

        <div class="container border-b-4 border-solid border-gray-400 mt-32">
            <div class="flex justify-center">
                <span class="font-bold text-xl"><h2>COLLAPSE</h2></span>
            </div>

            <div>
                <?php 
                    include('collapse.php');
                ?>
            </div>
        </div>

        
        

        <div class="container w-full mt-3 flex justify-center">
            
            <button onclick="window.location.href = 'desain_sistem5.php';" class="btn  btn-rounded btn-light"><i class="fas fa-undo"></i> back</button>
            <button onclick="window.location.href = '#';" class="btn  btn-rounded btn-light"><i class="fas fa-redo"></i> next</button>
                       
        </div>  

                
    </body>
</html>