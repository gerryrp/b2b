<html>


<body>
    
    
  <nav role="navigation" class="navigation">
    <div id="menuToggle" >
      
      <input type="checkbox" />
      
      <span></span>
      <span></span>
      <span></span>
      
      
        <ul id="menu" class="mainmenu">
          <li><a href="">Home</a></li>

          <li><a href="">Archives <i class="fas fa-angle-down"></i></a>
            <ul class="submenu">
              <li><a href="">Reservation List</a></li>
              <li><a href="">Int Reservation List</a></li>
              <li><a href="">Airline Reservation List</a></li>
              <li><a href="">Invoice List</a></li>
              <li><a href="">Int. Htl. List</a></li>
              <li><a href="">Airline Invoice List</a></li>
              <li><a href="">Credit Limit List</a></li>
              <li><a href="">Top Up List</a></li>
              <li><a href="">User Session List</a></li>
              <li><a href="">Sign Up List</a></li>
              <li><a href="">Search Flight</a></li>
            </ul>
          
          </li>

          <li><a href="">Report <i class="fas fa-angle-down"></i></a>
            <ul class="submenu">
              <li><a href="">Haryono Sales</a></li>
              <li><a href="">Operation Production</a></li>
              <li><a href="">Customer Production <i class="fas fa-angle-down"></i></a>
                <ul class="thirdmenu">
                  <li><a href="">Booker Incentive</a></li>
                </ul>
              </li>
              <li><a href="">Hotel Production <i class="fas fa-angle-down"></i></a>
                <ul class="thirdmenu">
                  <li><a href="">Hotel Check-in</a></li>
                  <li><a href="">Hotel Allotment</a></li>
                </ul>
              </li>
              <li><a href="">Incentives Claim</a></li>
              <li><a href="">Analytics <i class="fas fa-angle-down"></i></a>
                <ul class="thirdmenu">
                  <li><a href="">Issued</a></li>
                  <li><a href="">Confirmation Speed</a></li>
                  <li><a href="">Activity</a></li>
                  <li><a href="">Security</a></li>
                  <li><a href="">User Booking Statistic</a></li>
                  <li><a href="">Customer Sales</a></li>
                  <li><a href="">Hotel Sales</a></li>
                  <li><a href="">All Sales</a></li>

                </ul>
              </li>
              <li><a href="">Log <i class="fas fa-angle-down"></i></a>
                <ul class="thirdmenu">
                  <li><a href="">Email Log</a></li>
                  <li><a href="">Session Log</a></li>
                  <li><a href="">Allotment Log</a></li>
                  <li><a href="">Hotel Rate Log</a></li>
                  <li><a href="">Credit Limit Log</a></li>
                  <li><a href="">Deposit Log</a></li>
                  <li><a href="">Public Link Log</a></li>
                  <li><a href="">Mandrill</a></li>
                </ul>
              </li>
              <li><a href="">Price List</a></li>
            </ul>
          </li>

          <li><a href="">Master <i class="fas fa-angle-down "></i></a>
            <ul class="submenu">
              <li><a href="">Hotel <i class="fas fa-angle-down"></i></a>
                <ul class="thirdmenu">
                  <li><a href="">Hotel Group</a></li>
                  <li><a href="">Room Type</a></li>
                  <li><a href="">Room Facility</a></li>
                </ul>
              </li>
              <li><a href="">Rate <i class="fas fa-angle-down"></i></a>
                <ul class="thirdmenu">
                  <li><a href="">Rate Type</a></li>
                </ul>
              </li>
              <li><a href="">Allotment <i class="fas fa-angle-down"></i></a>
                <ul class="thirdmenu">
                  <li><a href="">Rate Type Group</a></li>
                </ul>
              </li>
              <li><a href="">Customers</a></li>
              <li><a href="">User</a></li>
              <li><a href="">API User</a></li>
              <li><a href="">Branch</a></li>
              <li><a href="">Location <i class="fas fa-angle-down"></i></a>
                <ul class="thirdmenu">
                  <li><a href="">City</a></li>
                  <li><a href="">State</a></li>
                  <li><a href="">Country</a></li>
                </ul>
              </li>
            </ul>
          </li>

          <li><a href="">Setting <i class="fas fa-angle-down "></i></a>
            <ul class="submenu">
              <li><a href="">System</a></li>
              <li><a href="">Airline System</a></li>
              <li><a href="">Int Hotel</a></li>
              <li><a href="">Int Htl Mark Up</a></li>
              <li><a href="">API</a></li>
              <li><a href="">User Role</a></li>
              <li><a href="">Reservation <i class="fas fa-angle-down"></i></a>
                <ul class="thirdmenu">
                  <li><a href="">Email Template</a></li>
                  <li><a href="">Booking Status</a></li>
                </ul>
              </li>
              <li><a href="">News <i class="fas fa-angle-down"></i></a>
                <ul class="thirdmenu">
                  <li><a href="">Front Page</a></li>
                </ul>
              </li>
              <li><a href="">Menu <i class="fas fa-angle-down"></i></a>
                <ul class="thirdmenu">
                  <li><a href="">Page</a></li>
                </ul>
              </li>
            </ul>
          </li>
          
          <li><a href="">Profile <i class="fas fa-angle-down "></i></a>
            <ul class="submenu">
              <li><a href="">Edit Profile</a></li>
              <li><a href="">Switch User Role</a></li>
              <li><a href="">Log In As(Do Not Book)</a></li>
            </ul>
          </li>

        </ul>
      
  
    </div>
  </nav>

</body>
</html>