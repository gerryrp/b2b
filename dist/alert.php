<html>
<?php 
 //include('head.php');
?>

<body>

    <style>
          


    </style>

    <div id="id1" class="alert alert-danger alert-sm flex ">
        <div class="w-1/12 flex  alerticon alerticon-danger">
            <i class="fas fa-times-circle"></i>
        </div>

        <div class="w-11/12 alertbody items-center flex">
                alert for danger
        </div>
        
        <div id="close" class="flex items-start ">
            <button class="flex alertclose"><i class="fas fa-times"></i></button>
        </div>

    </div>

    <div id="id2" class="alert alert-warning flex mt-10">
        <div class="w-1/12 flex  alerticon alerticon-warning">
            <i class="fas fa-exclamation-circle"></i>
        </div>

        <div class="w-11/12 alertbody">
                alert for warning
        </div>
        
        <div id="close" class="flex items-start ">
            <button class="flex alertclose"><i class="fas fa-times"></i></button>
        </div>

    </div>

    
    <div id="id3" class="alert alert-success flex mt-10">
        <div class="w-1/12 flex  alerticon alerticon-success">
            <i class="fas fa-check-circle"></i>
        </div>

        <div class="w-11/12 alertbody">
                alert for success
        </div>
        
        <div id="close" class="flex items-start ">
            <button class="flex alertclose"><i class="fas fa-times"></i></button>
        </div>

    </div>

    
    <div id="id4" class="alert alert-info flex mt-10">
        <div class="w-1/12 flex  alerticon alerticon-info">
            <i class="fas fa-info-circle"></i>
        </div>

        <div class="w-11/12 alertbody">
                alert for info
        </div>
        
        <div id="close" class="flex items-start ">
            <button class="flex alertclose"><i class="fas fa-times"></i></button>
        </div>

    </div>

<!-- 
    <div id="id1" class="w-5/12 mx-auto bg-red-500 flex  mt-20 ">
        <div class="w-1/12 bg-red-700 flex items-center justify-center text-2xl ">
            <i class="fas fa-times-circle"></i>
        </div>

        <div class="w-11/12 py-5 px-3 flex items-center  ">
            <div id="" class="w-11/12">
                
                    Alert for danger
                
            </div>  
            
            <button id="id2"><i class="fas fa-times text-2xl mt-1 mr-1"></i></button>
        
            <div id="id2" class="w-1/12 flex justify-end">
                <i class="fas fa-times text-2xl mt-1 mr-1"></i>
            </div> 
        </div>
  
    </div> -->

    
</body>

</html>