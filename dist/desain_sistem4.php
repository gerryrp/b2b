<html>
        <?php 
        include('head.php');
        ?>

<body>

    <div class="container border-b-4 border-solid border-gray-400">
        <div class="flex justify-center">
            <span class="font-bold text-xl"><h2>LABEL</h2></span>
        </div>

        <div>
            <div class="flex mb-10">
                <span>label primary : </span>
                <div>
                    <span class="label label-sm label-primary">label sm</span>
                    <span class="label label-primary">label base</span>
                    <span class="label label-lg label-primary">label lg</span>
                    <span class="label label-xl label-primary">label xl</span>
                </div>
            </div>

            <div class="flex mb-10">
                <span>label success : </span>
                <div>
                    <span class="label label-sm label-success">label sm</span>
                    <span class="label label-success">label base</span>
                    <span class="label label-lg label-success">label lg</span>
                    <span class="label label-xl label-success">label xl</span>
                </div>
            </div>

            <div class="flex mb-10">
                <span>label secondary : </span>
                <div>
                    <span class="label label-sm label-secondary">label sm</span>
                    <span class="label label-secondary">label base</span>
                    <span class="label label-lg label-secondary">label lg</span>
                    <span class="label label-xl label-secondary">label xl</span>
                </div>
            </div>
            
            <div class="flex mb-10">
                <span>label danger : </span>
                <div>
                    <span class="label label-sm label-danger">label sm</span>
                    <span class="label label-danger">label base</span>
                    <span class="label label-lg label-danger">label lg</span>
                    <span class="label label-xl label-danger">label xl</span>
                </div>
            </div>

            <div class="flex mb-10">
                <span>label warning : </span>
                <div>
                    <span class="label label-sm label-warning">label sm</span>
                    <span class="label label-warning">label base</span>
                    <span class="label label-lg label-warning">label lg</span>
                    <span class="label label-xl label-warning">label xl</span>
                </div>
            </div>

            <div class="flex mb-10">
                <span>label info : </span>
                <div>
                    <span class="label label-sm label-info">label sm</span>
                    <span class="label label-info">label base</span>
                    <span class="label label-lg label-info">label lg</span>
                    <span class="label label-xl label-info">label xl</span>
                </div>
            </div>

            <div class="flex mb-10">
                <span>label other : </span>
                <div>
                    <span class="label label-sm label-other">label sm</span>
                    <span class="label label-other">label base</span>
                    <span class="label label-lg label-other">label lg</span>
                    <span class="label label-xl label-other">label xl</span>
                </div>
            </div>

        </div>

    </div>

    <div class="container border-b-4 border-solid border-gray-400">
        <div class="flex justify-center">
            <span class="font-bold text-xl"><h2>SPAN</h2></span>
        </div>

        <div>
            <div class="mb-5">
                <span>span title :</span>
                <span class="span span-title">span title</span>
            </div>

            <div class="mb-5">
                <span>span green :</span>
                <span class="span span-green">span green</span>
            </div>

            <div class="mb-5">
                <span>span yellow :</span>
                <span class="span span-yellow ">span yellow</span>
            </div>

            <div class="mb-5">
                <span>span purple :</span>
                <span class="span span-purple ">span purple</span>
            </div>

            <div class="mb-5">
                <span>span blue :</span>
                <span class="span span-blue">span blue</span>
            </div>

            <div class="mb-5">
                <span>span red :</span>
                <span class="span span-red">span red</span>
            </div>

            <div class="mb-5">
                <span>span blue_link :</span>
                <span class="span span span-bluelink">span blue_link</span>
            </div>

            <div class="mb-5">
                <span>span brown :</span>
                <span class="span span-brown">span brown</span>
            </div>

            <div class="mb-5">
                <span>span orange :</span>
                <span class="span span-orange">span orange</span>
            </div>

            <div class="mb-5">
                <span>span status :</span>
                <span class="span span-status">span status</span>
            </div>

            <div class="mb-5">
                <span>span danger :</span>
                <span class="span span-danger">span danger</span>
            </div>

            <div class="mb-5">
                <span>span primary :</span>
                <span class="span span-primary">span primary</span>
            </div>

            <div class="mb-5">
                <span>span success :</span>
                <span class="span span-success">span success</span>
            </div>

            <div class="mb-5">
                <span>span pink :</span>
                <span class="span span-pink">span pink</span>
            </div>

            
            <div class="mb-5">
                <span>span violet :</span>
                <span class="span span-violet">span violet</span>
            </div>
            
           
        </div>
    </div>

    <div class="container border-b-4 border-solid border-gray-400">
        <div class="flex justify-center">
            <span class="font-bold text-xl"><h2>TABEL</h2></span>
        </div>

        <div class="">
            <div class="container">
                <div class='flex headtable '>
                    <div class='w-2/12'>Header a</div>
                    <div class='w-2/12'>Header b</div>
                    <div class='w-2/12'>Header c</div>
                    <div class='w-2/12'>Header d</div>
                    <div class='w-2/12'>Header e</div>
                    <div class='w-2/12'>Header f</div>
                </div>

                <div class='rowtable flex'>
                    <div class='w-2/12'>
                        <div class=''>tabel 1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>tabel 1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>tabel 1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>tabel 1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>tabel 1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>tabel 1</div>
                    </div>
                </div>

                <div class='rowtable flex'>
                    <div class='w-2/12'>
                        <div class=''>tabel 2</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>tabel 2</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>tabel 2</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>tabel 2</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>tabel 2</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>tabel 2</div>
                    </div>
                </div>
            </div>

        </div>

    </div>   

    <div class="container w-full mt-3 flex justify-center">
            
            <button onclick="window.location.href = 'desain_sistem3.php';" class="btn  btn-rounded btn-light"><i class="fas fa-undo"></i> back</button>
            <button onclick="window.location.href = 'desain_sistem5.php';" class="btn  btn-rounded btn-light"><i class="fas fa-redo"></i> next</button>
                       
    </div>
</body>

</html>