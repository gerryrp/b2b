<html>

<?php
include(__DIR__ . '/../head.php');
?>



<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');

    ?>

    <!-- isi content adasd -->

    <div class="container bg-gray-200 mx-auto w-11/12 mt-10 p-5">

        <div class="container mt-4 ">

            <div class="flex md-4">

                <div class="flex md-4 ">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/alinvoice/alinvoice.php';" type="button" class="btn btn-sm btn-rounded btn-light mr-3 " value="Sign Up"><i class="fas fa-angle-double-left"></i> Invoice List</button>
                </div>

                <div class="flex md-4 ">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/alreservation/alreservation_detail.php';" type="button" class="btn btn-sm btn-rounded btn-light mr-3 " value="Sign Up"><i class="fas fa-angle-double-left"></i> Reservation Detail</button>
                </div>

            </div>

        </div>

        <div class="container max-w-full mt-3 flex">
            <div class="w-full">

                <h4 class="headtable">Invoice</h4>

                <div class="flex flex-wrap rowtable ">

                    <div class="w-2/12 text-left text-xs pt-2 font-bold ">
                        Invoice No.
                    </div>

                    <div class="w-2/12 text-xs items-center flex">
                        <span class="">TK0119001588</span>
                    </div>

                    <div class="w-6/12 items-center flex">

                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-print"></i> Incoive</button>

                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-print"></i> Ticket</button>

                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-file-pdf"></i> Incoive</button>

                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-file-pdf"></i> Ticket</button>

                    </div>

                </div>

                <div class="flex flex-wrap rowtable ">

                    <div class="w-2/12 text-left text-xs pt-2 font-bold ">
                        Invoice Date
                    </div>

                    <div class="w-2/12 text-xs items-center flex">
                        <span class="">31 Mar 2019 14:31</span>
                    </div>

                </div>

                <div class="flex flex-wrap rowtable ">

                    <div class="w-2/12 text-left text-xs pt-2 font-bold ">
                        Airline
                    </div>

                    <div class="w-2/12 text-xs items-center flex">
                        <span class="">Citilink</span>
                    </div>

                </div>

                <div class="flex flex-wrap rowtable ">

                    <div class="w-2/12 text-left text-xs pt-2 font-bold ">
                        Passenger Name
                    </div>

                    <div class="w-2/12 text-xs items-center flex">
                        <span class=""> MS tika nur widyani</span>
                    </div>

                </div>



            </div>

        </div>

        <div class="w-6/12 mt-8 bg-gray-200 border-0 border-b border-gray-500 ">
            <div>
                <span class="font-bold">
                    <h2>Passenger Detail</h2>
                </span>
            </div>

            <div class="container">

                <div class='headtable flex'>
                    <div class='w-2/12'>#.</div>
                    <div class='w-2/12'>Title</div>
                    <div class='w-2/12'>Name</div>
                    <div class='w-2/12'>Seat</div>
                    <div class='w-2/12'>Ticket Number</div>
                    <div class='w-2/12'>Special Request</div>
                </div>

                <div class='rowtable flex'>
                    <div class='w-2/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>MS</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>tika nur widyani</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''></div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''></div>
                    </div>

                </div>

            </div>
        </div>

        <div class="w-8/12 mt-8 bg-gray-200 border-0 border-b border-gray-500 ">
            <div>
                <span class="font-bold">
                    <h2>Passenger Detail</h2>
                </span>
            </div>

            <div class="container">

                <div class='headtable flex'>
                    <div class='w-2/12'>#.</div>
                    <div class='w-2/12'>Flight Number</div>
                    <div class='w-2/12'>Airport Form</div>
                    <div class='w-2/12'>Airport To</div>
                    <div class='w-2/12'>ST Departure</div>
                    <div class='w-2/12'>ST Arrival</div>
                    <div class='w-2/12'>Class</div>
                </div>

                <div class='rowtable flex'>
                    <div class='w-2/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>QG 164</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>HLP</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>MLG</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>1 Apr 2019 12:25</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>1 Apr 2019 14:00</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>A</div>
                    </div>
                </div>

            </div>
        </div>

        <div class="w-6/12 mt-8 bg-gray-200 border-0 border-b border-gray-500 ">
            <div>
                <span class="font-bold">
                    <h2>Payment</h2>
                </span>
            </div>

            <div class="container">

                <div class='headtable flex'>
                    <div class='w-2/12 text-left'>Payment Detail</div>
                    <div class='w-2/12'></div>
                </div>

                <div class='rowtable flex'>
                    <div class='w-2/12'>
                        <div class=' text-left'>
                            <span class="font-bold text-xs">Grand Total</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=' text-left'>
                            <span class="font-bold text-sm">IDR 1,362,000</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>


    </div>


    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>