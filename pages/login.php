<html>

<?php
include('head.php');
?>

<body>

    <?php
    include(base_path . '/component/headerb2b.php');
    ?>

    <div class="container max-w-full mt-10">
        <div class="flex flex-wrap overflow-hidden mb-4">

            <div class="w-1/4 overflow-hidden sm: md:w-1/6 lg:w-1/6 xl:w-1/4">

            </div>

            <div class="w-full overflow-hidden sm:w-full md:w-1/3 lg:w-1/4 xl:w-1/4">
                <div class=" border-0 border-r border-solid border-black">
                    <div class="mb-8">
                        <span class="font-bold text-lg">
                            <h6>
                                Business to Business<br>
                                Domestic Hotel Reservation<br>
                            </h6>
                        </span>
                    </div>

                    <div class="mb-3">
                        <span class="span span-green"> Made for Travel Agent, Corporate, and Hotel users</span>
                    </div>


                    <div class="mb-3">
                        <span class="span span-blue">
                            We are now accepting
                            <span>
                    </div>

                    <div>
                        <span class="span span-blue">
                            <a href="https://www.klikbca.com/KlikPay/klikpay.html" class="span span-blue">BCA KlikPay,</a>
                            <a href="" class="span span-blue"> CIMB Click</a> and
                            <a href="" class="span span-blue"> Mandiri ClickPay</a>
                            <br><br>
                        </span>
                    </div>

                    <span style="font-size:11px">
                        Built and run on Open Source Software<br>
                        <span class="font-bold">Compatible with</span>
                        <a href="https://www.google.com/intl/en/chrome/" class="span span-primary font-bold text-xs">Google Chrome</a>,
                        <a href="http://www.mozilla.org/en-US/firefox/new/" class="span span-primary font-bold text-xs">Mozilla Firefox</a>, &
                        <a href="https://support.microsoft.com/en-us/help/17621/internet-explorer-downloads" class="span span-primary font-bold text-xs">Internet Explorer 9</a>,
                        and above<br>
                        <span class="span span-brown">Not compatible with Internet Explorer 8 and below</span><br>
                        Need help? Contact us at inf...@haryono.co.id<br><br>
                        Copyright &copy;2014
                        <a href="https://www.haryonotours.com/" class="span span-blue1">www.haryonotours.com</a>
                        [BETA]
                    </span>

                </div>
            </div>

            <div class="w-full overflow-hidden sm:w-1/2 md:w-1/2 lg:w-1/2 xl:w-1/2 p-5">

                <div class="flex flex-wrap overflow-hidden mb-4  items-center ">

                    <div class="w-1/3  overflow-hidden sm:w-1/2 md:w-1/2 lg:w-1/3 xl:w-1/4 ">
                        <a href="forgot_pass.php" class="span span span-bluelink" style="text-decoration:none;vertical-align:middle">Forgot password ?</a>
                    </div>

                    <div class="w-1/2  overflow-hidden sm:w-1/2 md:w-1/2 lg:w-1/2 xl:w-1/2 ">
                        <button onclick="window.location.href = 'sign_up.php';" type="button" class="btn btn-sm btn-rounded btn-danger text-sm " value="Sign Up">Sign Up</button>
                    </div>

                </div>

                <div class="flex mb-4">

                    <div class="w-1/2">
                        <input type="text" name="username" class="input  input-lg w-64 " placeholder="Username atau email ..">
                    </div>

                </div>

                <div class="flex mb-4">

                    <div class="w-1/2">
                        <input type="text" name="username" class="input  input-lg w-64 " placeholder="Password ..">
                    </div>

                </div>

                <div class="flex mb-4">

                    <div class="w-1/2">
                        <input type="text" name="username" class="input  input-lg w-64 " placeholder="Company ID ..">
                    </div>

                </div>

                <div class="flex flex-wrap overflow-hidden mb-4 items-center ">

                    <div class="w-1/3 overflow-hidden sm:w-1/2 md:w-1/2 lg:w-1/3 xl:w-1/4">

                        <input type="checkbox" class="check" id="dropdownCheck2" data-label="remember me">
                        <label class="span span span-bluelink" for="dropdownCheck2">
                            Remember me
                        </label>

                    </div>

                    <div class="w-1/2 overflow-hidden sm:w-1/2 md:w-1/2 lg:w-1/2 xl:w-1/2">
                        <button onclick="window.location.href = 'home.php';" type="button" class="btn btn-sm text-sm btn-rounded btn-primary" value="Log in">Log in</button>
                    </div>

                </div>

            </div>

        </div>
    </div>




</body>

</html>