<html>
<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');
    ?>


    <!-- isi content -->

    <div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">

        <div class="w-full boxrow">
            <div class="flex md-4">
                <span class="span-title">
                    <h2>User Session List</h2>
                </span>
            </div>
        </div>

        <div class="flex md-4 mt-5">
            <div class="w-1/3 bg-red">
                <button class="btn btn-rounded btn-light">Clear Old Session</button>
            </div>

            <div class="w-1/2 items-center flex">

            </div>

            <div class="w-2/12 bg-blue items-center flex mt-1">

            </div>

        </div>


        <div class="w-full mt-1 bg-gray-200">
            <div class="container">

                <div class='flex headtable'>
                    <div class='w-1/12'>No.</div>
                    <div class='w-3/12'>Session Name</div>
                    <div class='w-2/12'>IP Address</div>
                    <div class='w-2/12'>User ID</div>
                    <div class='w-2/12'>Login Time</div>
                    <div class='w-2/12'>Last Activity</div>
                    <div class='w-2/12'></div>
                </div>

                <div class='flex rowtable '>
                    <div class='w-1/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>opsuv7uiqishkg97muulgdeof1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>116.84.110.21</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>01cbts@gmail.com</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>14 Dec 2018 11:56</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>14-12-2018 12:08</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <button class="btn btn-sm btn-rounded btn-light">Sign Out</button>
                        </div>
                    </div>
                </div>

                <div class='flex rowtable '>
                    <div class='w-1/12'>
                        <div class=''>2</div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>ktt2mdvfs5a3icjvo91dfphf45</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>172.68.144.157</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>02cbts@gmail.com</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>21 Feb 2019 14:59</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>21-02-2019 15:22</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <button class="btn btn-sm btn-rounded btn-light">Sign Out</button>
                        </div>
                    </div>
                </div>







            </div>
        </div>


    </div>



    <?php
    include(base_path . '/component/footer.php');
    ?>
</body>

</html>