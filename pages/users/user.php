<html>
<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');
    ?>


    <!-- isi content -->

    <div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">

        <div class="w-full boxrow">
            <div class="flex md-4">
                <span class="span-title">
                    <h2>Master User</h2>
                </span>
            </div>
        </div>

        <div class="flex md-4">
            <div class="w-4/12 bg-red">
                <?php
                include(base_path . '/component/padding.php');
                ?>
            </div>

            <div class="w-4/12 items-center justify-center flex">
                <div>
                    Search :
                    <input type="text" class="input">
                    <button class="btn btn-sm btn-rounded btn-light">Search</button>
                </div>
            </div>

            <div class="w-4/12 bg-blue items-center justify-end flex">
                <div class="mr-1">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/users/user_add.php';" class="btn btn-sm btn-rounded btn-success"><i class="fas fa-plus"></i> Add</button>
                    <button class="btn btn-sm btn-rounded btn-primary"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>
            </div>

        </div>

        <div class="w-full bg-gray-300 p-3 flex boxrow">

            <div class="w-1/2 flex text-sm">
                <div class=" items-center flex mx-2">
                    <input id="option1" type="radio" name="field" value="option1">
                    <label for="option1"><span><span></span></span>All
                    </label>
                </div>

                <div class=" items-center flex mx-2">
                    <input id="option2" type="radio" name="field" value="">
                    <label for="option2"><span><span></span></span>Branch
                    </label>
                </div>

                <div class=" items-center flex mx-2">
                    <input id="option3" type="radio" name="field" value="">
                    <label for="option3"><span><span></span></span>Agent
                    </label>
                </div>

                <div class=" items-center flex mx-2">
                    <input id="option4" type="radio" name="field" value="">
                    <label for="option4"><span><span></span></span>Corporate
                    </label>
                </div>

                <div class=" items-center flex mx-2">
                    <input id="option5" type="radio" name="field" value="">
                    <label for="option5"><span><span></span></span>Special Customer
                    </label>
                </div>

                <div class=" items-center flex mx-2">
                    <input id="option6" type="radio" name="field" value="">
                    <label for="option6"><span><span></span></span>FIT
                    </label>
                </div>

                <div class=" items-center flex mx-2">
                    <input id="option7" type="radio" name="field" value="">
                    <label for="option7"><span><span></span></span>Hotel
                    </label>
                </div>
            </div>

        </div>

        <div class="w-full bg-gray-300 p-3 flex boxrow">
            <span class="mx-3">Active Status : </span>
            <div class="w-1/2 flex text-sm">
                <div class=" items-center flex mx-2">
                    <input id="option8" type="radio" name="field" value="">
                    <label for="option8"><span><span></span></span>All
                    </label>
                </div>

                <div class=" items-center flex mx-2">
                    <input id="option9" type="radio" name="field" value="">
                    <label for="option9"><span><span></span></span>Active
                    </label>
                </div>

                <div class=" items-center flex mx-2">
                    <input id="option10" type="radio" name="field" value="">
                    <label for="option10"><span><span></span></span>Inactive
                    </label>
                </div>
            </div>

        </div>

        <div class="w-full bg-gray-300 p-3 flex">
            <div class=" items-center flex mx-3">
                <input type="checkbox" class="check" id="dropdownCheck11" data-label="All" checked>
                <label class="" for="dropdownCheck11">
                    Wihtout Role
                </label>
            </div>

        </div>

        <div class="w-full mt-1 bg-gray-200">
            <div class="container">

                <div class='flex headtable'>
                    <div class='w-1/12'>No.</div>
                    <div class='w-3/12'>User ID</div>
                    <div class='w-1/12'>Name</div>
                    <div class='w-3/12'>Email</div>
                    <div class='w-2/12'>Company Name</div>
                    <div class='w-2/12'>Company ID</div>
                    <div class='w-2/12'>Role</div>
                    <div class='w-1/12'>&nbsp</div>
                </div>

                <div class='rowtable flex'>
                    <div class='w-1/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>
                            <span onclick="window.location.href = '<?= root_path ?>/pages/users/user_detail.php';" class="span span span-bluelink">jambireservation@astonhotelsasia.com</span>
                        </div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>&nbsp</div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>jambiinfo@astonhotelsasia.com</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>ASTON JAMBI</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>astonjambi</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>Hotel Reservation</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-pencil-alt"></i></button>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                </div>






            </div>
        </div>

    </div>



    <?php
    include(base_path . '/component/footer.php');
    ?>
</body>

</html>