<html>
<?php
include(__DIR__ . "/../head.php");
?>


<body>

    <?php
    include(base_path . "/component/topnav.php");
    include(base_path . "/component/slider.php");
    ?>

    <div class="container w-11/12 mx-auto ">

        <?php include(base_path . '/component/vertical_tabs.php'); ?>

    </div>

    <!-- isi content adasd -->
    <div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">

        <div class="w-full border-0 border-b border-solid border-black">
            <div class="flex md-4">
                <span class="span-title">
                    <h2>Airline Reservation Roundtrip</h2>
                </span>
            </div>
        </div>

        <div class="w-full container mt-5">
            <div class="w-11/12 mx-auto headtable headtable-navy p-1">
                Fare Summary
            </div>

            <div class="w-11/12 mx-auto ">
                <div class="container">
                    <div class="headtable flex">
                        <div class="w-3/12"></div>
                        <div class="w-3/12 ">Bare fare</div>
                        <div class="w-3/12">Other fare</div>
                        <div class="w-3/12">Total</div>
                    </div>
                    <div id="depart" class="hidden">
                        <div class="rowtable flex">
                            <div class="w-3/12">Departure</div>
                            <div class="w-3/12">IDR.562,000</div>
                            <div class="w-3/12">IDR.149,200</div>
                            <div class="w-3/12">IDR.711,200</div>
                        </div>
                    </div>
                    <div id="return" class="hidden">
                        <div class="rowtable flex">
                            <div class="w-3/12">Return</div>
                            <div class="w-3/12">IDR.562,000</div>
                            <div class="w-3/12">IDR.159,200</div>
                            <div class="w-3/12">IDR.721,200</div>
                        </div>
                        <div class="rowtable flex">
                            <div class="w-2/12"></div>
                            <div class="w-5/12"></div>
                            <div class="w-2/12 justify-center flex">Total</div>
                            <div class="w-2/12 span span-status">IDR.1,432,400</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container mt-5">

            <h4 class=" w-11/12 mx-auto headtable headtable-navy p-2">
                DEPARTURE SURABAYA (SUB) ✈ DENPASAR (DPS) [Aug 19, 2019]
            </h4>

            <div class="w-11/12 mx-auto ">

                <div class="container">
                    <div class="headtable flex">
                        <div class="w-2/12">Airlines</div>
                        <div class="w-2/12">Flight#</div>
                        <div class="w-2/12">Depart</div>
                        <div class="w-2/12">Arrive</div>
                        <div class="w-2/12">Fare and Class</div>
                        <div class="w-2/12"></div>
                    </div>

                    <div class="rowtable flex">
                        <div class="w-2/12">
                            <div class="">Citilink</div>
                        </div>
                        <div class=" w-6/12">
                            <div class="flex">
                                <div class="w-4/12">
                                    QG-668
                                </div>
                                <div class="w-4/12">
                                    11.35 SURABAYA (SUB)
                                </div>
                                <div class="w-4/12">
                                    14.00 DENPASAR (DPS)
                                </div>
                            </div>
                            <div class="flex">
                                <div class="w-4/12">
                                    QG-696
                                </div>
                                <div class="w-4/12">
                                    12.55 SURABAYA (SUB)
                                </div>
                                <div class="w-4/12">
                                    14.55 DENPASAR (DPS)
                                </div>
                            </div>
                            <div class="flex">
                                <div class="w-4/12">
                                    QG-698
                                </div>
                                <div class="w-4/12">
                                    18.10 SURABAYA (SUB)
                                </div>
                                <div class="w-4/12">
                                    20.10 DENPASAR (DPS)
                                </div>
                            </div>
                        </div>
                        <div class="w-2/12 items-center flex">
                            <div class="">
                                <form>
                                    <select class="inputselect ">
                                        <option value="">562,000-A[9]</option>
                                    </select>
                                </form>
                            </div>
                        </div>
                        <div class="w-2/12 ">
                            <div class="justify-center flex">
                                <input onclick="hiddFunc()" id="radio1" type="radio" name="field1" value="1">
                                <label for="radio1"><span><span></span></span>&nbsp</label>
                            </div>
                            <div class="justify-center flex">
                                <input id="radio2" type="radio" name="field1" value="2">
                                <label for="radio2"><span><span></span></span>&nbsp</label>
                            </div>
                            <div class="justify-center flex  ">
                                <input id="radio3" type="radio" name="field1" value="3">
                                <label for="radio3"><span><span></span></span>&nbsp</label>
                            </div>
                        </div>
                    </div>

                    <div class="rowtable flex">
                        <div class="w-2/12">
                            Citilink
                        </div>
                        <div class="w-2/12">
                            QG-698
                        </div>
                        <div class="w-2/12">
                            18:10 SURABAYA (SUB)
                        </div> 
                        <div class="w-2/12">
                            20:10 DENPASAR (DPS)
                        </div>
                        <div class="w-2/12">
                            <form>
                                <select class="inputselect ">
                                    <option value="">562,000-A[9]</option>
                                </select>
                            </form>
                        </div>
                        <div class="w-2/12">
                            <div class="justify-center flex">
                                <input id="radio4" type="radio" name="field1" value="4">
                                <label for="radio4"><span><span></span></span>&nbsp</label>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

        <div class="container mt-5">

            <h4 class=" w-11/12 mx-auto headtable headtable-navy p-2">
                RETURN DENPASAR (DPS) ✈ SURABAYA (SUB) [Aug 20, 2019]
            </h4>

            <div class="w-11/12 mx-auto ">
                <div class="container">
                    <div class="headtable flex">
                        <div class="w-2/12">Airlines</div>
                        <div class="w-2/12">Flight#</div>
                        <div class="w-2/12">Depart</div>
                        <div class="w-2/12">Arrive</div>
                        <div class="w-2/12">Fare and Class</div>
                        <div class="w-2/12"></div>
                    </div>

                    <div class="rowtable flex">
                        <div class="w-2/12">
                            <div class="">Citilink</div>
                        </div>
                        <div class="w-2/12">
                            <div class="">QG-695</div>
                        </div>
                        <div class="w-2/12">
                            <div class="">10:15 DENPASAR (DPS)</div>
                        </div>
                        <div class="w-2/12">
                            <div class="">09:55 SURABAYA (SUB)</div>
                        </div>
                        <div class="w-2/12">
                            <form>
                                <select class="inputselect ">
                                    <option value="">562,000-A[9]</option>
                                </select>
                            </form>
                        </div>
                        <div class="w-2/12">
                            <div class="justify-center flex">
                                <input onclick="hiddFunc2()" id="radio5" type="radio" name="field2" value="5">
                                <label for="radio5"><span><span></span></span>&nbsp</label>
                            </div>
                        </div>

                    </div>

                    <div class="rowtable flex">
                        <div class="w-2/12">
                            <div class="">Citilink</div>
                        </div>
                        <div class="w-2/12">
                            <div class="">QG-669</div>
                        </div>
                        <div class="w-2/12">
                            <div class="">12:15 DENPASAR (DPS)</div>
                        </div>
                        <div class="w-2/12">
                            <div class="">12:15 SURABAYA (SUB)</div>
                        </div>
                        <div class="w-2/12">
                            <form>
                                <select class="inputselect ">
                                    <option value="">562,000-A[9]</option>
                                </select>
                            </form>
                        </div>
                        <div class="w-2/12">
                            <div class="justify-center flex">
                                <input id="radio6" type="radio" name="field2" value="radio6">
                                <label for="radio6"><span><span></span></span>&nbsp</label>
                            </div>
                        </div>

                    </div>

                    <div class="rowtable flex">
                        <div class="w-2/12">
                            <div class="">Citilink</div>
                        </div>
                        <div class="w-2/12">
                            <div class="">QG-699</div>
                        </div>
                        <div class="w-2/12">
                            <div class="">12:15 DENPASAR (DPS)</div>
                        </div>
                        <div class="w-2/12">
                            <div class="">12:15 SURABAYA (SUB)</div>
                        </div>
                        <div class="w-2/12">
                            <form>
                                <select class="inputselect ">
                                    <option value="">562,000-A[9]</option>
                                </select>
                            </form>
                        </div>
                        <div class="w-2/12">
                            <div class="justify-center flex">
                                <input id="radio7" type="radio" name="field2" value="7">
                                <label for="radio7"><span><span></span></span>&nbsp</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="container w-full mt-3 flex justify-center">

            <button onclick="window.location.href = '<?= root_path ?>/pages/bairline/bookrequest.php';" class="btn btn-rounded btn-light"><i class="fas fa-check"></i> BOOK NOW</button>

        </div>

    </div>

    <?php
    include(base_path . "/component/footer.php");
    ?>

    <script>
        function hiddFunc() {
            var x = document.getElementById("depart");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

        function hiddFunc2() {
            var x = document.getElementById("return");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    </script>

</body>

</html>