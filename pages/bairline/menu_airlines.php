<html>
<?php
include(__DIR__ . "/../head.php");
?>


<body>

    <?php
    include(base_path . "/component/topnav.php");
    include(base_path . "/component/slider.php");
    ?>

    <div class="container w-11/12 mx-auto ">

        <?php include(base_path . '/component/vertical_tabs.php'); ?>

    </div>

    <!-- isi content adasd -->
    <div class="container w-11/12 bg-gray-200 mx-auto mt-5 p-5">

        <div class="w-full border-0 border-b border-solid border-black">
            <div class="flex md-4">
                <span class="span-title">
                    <h2>Airline Reservation</h2>
                </span>
            </div>
        </div>

        <div class="w-full container mt-5">
            <div class="w-11/12 mx-auto headtable headtable-navy p-1">
                Fare Summary
            </div>

            <div class="w-11/12 mx-auto ">
                <div class="container">
                    <div class="headtable flex">
                        <div class="w-3/12"></div>
                        <div class="w-3/12 ">Bare fare</div>
                        <div class="w-3/12">Other fare</div>
                        <div class="w-3/12">Total</div>
                    </div>
                    <div id="depart" class="hidden">
                        <div class="rowtable flex">
                            <div class="w-3/12">Departure</div>
                            <div class="w-3/12">IDR.562,000</div>
                            <div class="w-3/12">IDR.149,200</div>
                            <div class="w-3/12">IDR.711,200</div>
                        </div>
                        <div class="rowtable flex">
                            <div class="w-2/12"></div>
                            <div class="w-5/12"></div>
                            <div class="w-2/12 justify-center flex">Total</div>
                            <div class="w-2/12 span span-status">IDR.711,200</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container mt-5">
            <div class="flex">

                <div class="w-11/12 mx-auto bg-gray-400 ">
                    <div class="container">
                        <div class="headtable flex">
                            <div class="w-2/12">Airlines</div>
                            <div class="w-2/12">Flight#</div>
                            <div class="w-2/12">Depart</div>
                            <div class="w-2/12">Arrive</div>
                            <div class="w-2/12">Fare and Class</div>
                            <div class="w-2/12"></div>
                        </div>


                        <div class="rowtable flex">
                            <div class="w-2/12">
                                <div class="">citilink</div>
                            </div>
                            <div class="w-2/12">
                                <div class="">QG-698</div>
                            </div>
                            <div class="w-2/12">
                                <div class="">18:10 SURABAYA (SUB)</div>
                            </div>
                            <div class="w-2/12">
                                <div class="">20:10 DENPASAR (DPS)</div>
                            </div>
                            <div class="w-2/12">
                                <div class="">
                                    <form>
                                        <select class="inputselect ">
                                            <option value="562,000-A[9]">562,000-A[9]</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                            <div class="w-2/12">
                                <div class="justify-center flex mb-2">
                                    <input onclick="hiddFunc()" id="radio1" type="radio" name="field1" value="1">
                                    <label for="radio1"><span><span></span></span>&nbsp</label>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>

        <div class="container w-full mt-3 flex justify-center">

            <button onclick="window.location.href = '<?= root_path ?>/pages/bairline/bookrequest.php';" class="btn btn-rounded btn-success"><i class="fas fa-check"></i> BOOK NOW</button>

        </div>

    </div>




    <div class="bg-white">
    </div>


    <?php
    include(base_path . "/component/footer.php");
    ?>

    <script>
        function hiddFunc() {
            var x = document.getElementById("depart");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    </script>
</body>

</html>