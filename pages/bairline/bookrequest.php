<html>
<?php
include(__DIR__ . "/../head.php");
?>


<body>

    <?php
    include(base_path . "/component/topnav.php");
    include(base_path . "/component/slider.php");

    ?>

    <div class="container w-11/12 mx-auto ">
        <?php include(base_path . '/component/vertical_tabs.php'); ?>
    </div>

    <div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">

        <div class="w-full boxrow flex">
            <span class="span-title">
                <h2>Book Request</h2>
            </span>
        </div>

        <div class="w-11/12 mx-auto mt-10">

            <div class="headtable p-2">Adult Pax</div>

            <div class="flex rowtable ">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    #1
                </div>
            </div>

            <div class="flex rowtable ">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Title :
                </div>
                <div class="w-1/2 items-center flex">
                    <form>
                        <select class="inputselect">
                            <option value="">Mr.</option>
                            <option value="">Mrs.</option>
                            <option value="">Ms.</option>
                        </select>
                    </form>
                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    First Name :
                </div>
                <div class="w-1/2 items-center flex">
                    <input type="text" class="input" value="">
                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Last Name :
                </div>
                <div class="w-1/2 items-center flex">
                    <input type="text" class="input" value="">
                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Date Birth :
                </div>
                <div class="w-8/12 items-center flex">

                    <div class=" flex w-1/2">
                        <p class="my-auto mr-2"></p>
                        <div>
                            <label for="dateofbirth"></label>
                        </div>
                        <div class="flex md-4 justify-center items-center">
                            <input class="flex" type="date" name="dateofbirth" id="dateofbirth">
                        </div>
                    </div>

                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Id Number :
                </div>
                <div class="w-8/12 items-center flex">
                    <input type="text" class="input" value="">
                </div>
            </div>

            <div class="rowtable">
                <div class="headtable bg-green-600 text-white">Baggage</div>

                <div class="flex rowtable ">
                    <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                        #1
                    </div>
                </div>
            </div>

            <div class="rowtable">
                <div class="headtable bg-green-600">Meals</div>

                <div class="flex rowtable ">
                    <div class="w-1/3 text-left text-xl font-bold ">
                        SUB - DPS

                        <div class="flex">
                            <input type="checkbox" class="check" id="meals1" data-label="">
                            <label class="text-sm" for="meals1">Garden Salad</label>
                        </div>

                    </div>
                </div>
            </div>

            <div class="flex rowtable ">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    #2
                </div>
            </div>

            <div class="flex rowtable ">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Title :
                </div>
                <div class="w-1/2 items-center flex">
                    <form>
                        <select class="inputselect">
                            <option value="">Mr.</option>
                            <option value="">Mrs.</option>
                            <option value="">Ms.</option>
                        </select>
                    </form>
                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    First Name :
                </div>
                <div class="w-1/2 items-center flex">
                    <input type="text" class="input" value="">
                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Last Name :
                </div>
                <div class="w-1/2 items-center flex">
                    <input type="text" class="input" value="">
                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Date Birth :
                </div>
                <div class="w-8/12 items-center flex">

                    <div class=" flex w-1/2">
                        <p class="my-auto mr-2"></p>
                        <div>
                            <label for="dateofbirth"></label>
                        </div>
                        <div class="flex md-4 justify-center items-center">
                            <input class="flex" type="date" name="dateofbirth" id="dateofbirth">
                        </div>
                    </div>

                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Id Number :
                </div>
                <div class="w-8/12 items-center flex">
                    <input type="text" class="input" value="">
                </div>
            </div>

            <div class="rowtable">
                <div class="headtable bg-green-600 text-white">Baggage</div>

                <div class="flex rowtable ">
                    <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                        #2
                    </div>
                </div>
            </div>

            <div class="rowtable">
                <div class="headtable bg-green-600">Meals</div>

                <div class="flex rowtable ">
                    <div class="w-1/3 text-left text-xl font-bold ">
                        SUB - DPS

                        <div class="flex">
                            <input type="checkbox" class="check" id="meals1" data-label="">
                            <label class="text-sm" for="meals1">Garden Salad</label>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div class="w-11/12 mx-auto mt-10">

            <div class="headtable p-2">Children Pax</div>

            <div class="flex rowtable ">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    #1
                </div>
            </div>

            <div class="flex rowtable ">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Title :
                </div>
                <div class="w-1/2 items-center flex">
                    <form>
                        <select class="inputselect">
                            <option value="">Mr.</option>
                            <option value="">Mrs.</option>
                            <option value="">Ms.</option>
                        </select>
                    </form>
                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    First Name :
                </div>
                <div class="w-1/2 items-center flex">
                    <input type="text" class="input" value="">
                </div>
            </div>
            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Last Name :
                </div>
                <div class="w-1/2 items-center flex">
                    <input type="text" class="input" value="">
                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Date Birth :
                </div>
                <div class="w-8/12 items-center flex">
                    <div class=" flex w-1/2">
                        <p class="my-auto mr-2"></p>
                        <div>
                            <label for="dateofbirth"></label>
                        </div>
                        <div class="flex md-4 justify-center items-center">
                            <input class="flex" type="date" name="dateofbirth" id="dateofbirth">
                        </div>
                    </div>
                </div>
            </div>

            <div class="rowtable">
                <div class="headtable bg-green-600">Baggage</div>

                <div class="flex rowtable ">
                    <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                        #1
                    </div>
                </div>
            </div>

            <div class="rowtable">
                <div class="headtable bg-green-600">Meals</div>

                <div class="flex rowtable ">
                    <div class="w-1/3 text-left text-xl font-bold ">
                        SUB - DPS

                        <div class="flex">
                            <input type="checkbox" class="check" id="meals1" data-label="">
                            <label class="text-sm" for="meals1">Garden Salad</label>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <div class="w-11/12 mx-auto mt-10">

            <div class="headtable p-2">Infant Pax</div>

            <div class="flex rowtable ">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    #1
                </div>
            </div>

            <div class="flex rowtable ">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Title :
                </div>
                <div class="w-1/2 items-center flex">
                    <form>
                        <select class="inputselect">
                            <option value="">Mr.</option>
                            <option value="">Mrs.</option>
                            <option value="">Ms.</option>
                        </select>
                    </form>
                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    First Name :
                </div>
                <div class="w-1/2 items-center flex">
                    <input type="text" class="input" value="">
                </div>
            </div>
            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Last Name :
                </div>
                <div class="w-1/2 items-center flex">
                    <input type="text" class="input" value="">
                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Date Birth :
                </div>
                <div class="w-8/12 items-center flex">
                    <div class=" flex w-1/2">
                        <p class="my-auto mr-2"></p>
                        <div>
                            <label for="dateofbirth"></label>
                        </div>
                        <div class="flex md-4 justify-center items-center">
                            <input class="flex" type="date" name="dateofbirth" id="dateofbirth">
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="w-11/12 mx-auto mt-10">

            <div class="headtable p-2">Contact</div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Title :
                </div>
                <div class="w-1/2 items-center flex">
                    <form>
                        <select class="inputselect">
                            <option value="">Mr.</option>
                            <option value="">Mrs.</option>
                            <option value="">Ms.</option>
                        </select>
                    </form>
                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    First Name :
                </div>
                <div class="w-1/2 items-center flex">
                    <input type="text" class="input" value="">
                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Last Name :
                </div>
                <div class="w-1/2 items-center flex">
                    <input type="text" class="input" value="">
                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Phone :
                </div>
                <div class="w-8/12 items-center flex">
                    <input type="text" class="input" value="">
                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Address :
                </div>
                <div class="w-8/12 items-center flex">
                    <input type="text" class="input" value="">
                </div>
            </div>

            <div class="flex rowtable">
                <div class="w-1/3 text-left text-sm pt-2 font-bold ">
                    Email
                </div>
                <div class="w-1/2 text-xs items-center flex">
                    <input type="text" class="input" value="">
                </div>
            </div>

        </div>

        <div class="container w-full mt-3 flex justify-center">
            <button onclick="window.location.href = '<?= root_path ?>/pages/bairline/chooseseat.php';" class="btn btn-rounded btn-success"><i class="fas fa-check"></i> SUBMIT</button>
        </div>

    </div>

    <?php
    include(base_path . "/component/footer.php");
    ?>

</body>

</html>