<html>
<?php
include(__DIR__ . "/../head.php");
?>


<body>

    <?php
    include(base_path . "/component/topnav.php");
    include(base_path . "/component/slider.php");
    ?>

    <div class="container w-11/12 mx-auto ">
        <?php include(base_path . '/component/vertical_tabs.php'); ?>
    </div>

    <div class="container w-11/12 mx-auto mt-5">
        <div class="flex">
            <div class="w-1/2">
                <div class="mx-auto w-1/2">
                    Adult1<br>
                    <input type="text" name="username" class="input input-sm" placeholder="SM"><br>
                    Adult2<br>
                    <input type="text" name="username" class="input input-sm" placeholder="SM"><br>
                    Children1 <br>
                    <input type="text" name="username" class="input input-sm" placeholder="SM"><br>
                    Infantri1<br>
                    <input type="text" name="username" class="input input-sm" placeholder="SM"><br>
                </div>
            </div>
            <div class="w-4/12 ">
                <div class="flex justify-center">
                    <span class="">Main Cabin</span>
                </div>
                <div class="flex justify-center">
                    <span class="mx-3">A</span>
                    <span class="mx-4">B</span>
                    <span class="mx-3">C</span>
                    <span class="mx-4">&nbsp</span>
                    <span class="mx-3">D</span>
                    <span class="mx-4">E</span>
                    <span class="mx-4">F</span>
                </div>
                <div class="flex justify-center">
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <span class=" p-3 items-center" disabled>1</span>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                </div>
                <div class="flex justify-center">
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <span class=" p-3 items-center" disabled>2</span>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                </div>
                <div class="flex justify-center">
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <span class=" p-3 items-center" disabled>3</span>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                </div>
                <div class="flex justify-center">
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <span class=" p-3 items-center" disabled>4</span>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                    <button class="btn btn-primary p-3">&nbsp</button>
                </div>
            </div>
        </div>
    </div>

    <?php
    include(base_path . "/component/footer.php");
    ?>

</body>

</html>