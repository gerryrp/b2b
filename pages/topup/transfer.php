<html>
<?php
include(__DIR__ . '/../head.php');
?>



<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');
    ?>


    <!-- isi content -->

    <div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">

        <div class="w-full boxrow">
            <div class="flex md-4">
                <span class="span-title">
                    <h2>Top Up List</h2>
                </span>
            </div>
        </div>

        <div class="flex md-4">
            <div class="w-1/3 bg-red">

            </div>

            <div class="w-1/2 items-center flex">

            </div>

            <div class="w-2/12 bg-blue items-center justify-end flex mt-1">
                <input type="checkbox" class="check" id="dropdownCheck2" data-label="remember me">
                <label class="span span-blue1" for="dropdownCheck2">
                    KlikPay Only
                </label>
            </div>

        </div>


        <div class="w-full mt-1 bg-gray-200">
            <div class="container">

                <div class='flex headtable'>
                    <div class='w-1/12'>No.</div>
                    <div class='w-2/12'>Transfer #</div>
                    <div class='w-2/12'>Customer</div>
                    <div class='w-2/12'>Status</div>
                    <div class='w-2/12'>Via</div>
                    <div class='w-2/12'>Incoming DP</div>
                    <div class='w-2/12'>Balance</div>
                    <div class='w-2/12'>Date</div>
                </div>

                <div class='flex rowtable'>
                    <div class='w-1/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = '<?= root_path ?>/pages/topup/transfer_detail.php';" class="span span span-bluelink">1903300004</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = '<?= root_path ?>/pages/log/depositlog_list.php';" class="span span span-bluelink">RODEX HOLIDAY (DP)</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="badge badge-fit">Received</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>BCA KlikPat</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>1,200,000</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>3,263,199 </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>30 Mar 2019 21:14</div>
                    </div>
                </div>



                <div class='flex rowtable'>
                    <div class='w-1/12'>
                        <div class=''>2</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="span span span-bluelink">1903300004</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="span span span-bluelink">RODEX HOLIDAY (DP)</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="badge badge-fit">Received</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>Manual Transfer</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>1,903,901</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>2,063,199</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>30 Mar 2019 15:55</div>
                    </div>
                </div>




            </div>
        </div>


    </div>



    <?php
    include(base_path . '/component/footer.php');
    ?>
</body>

</html>