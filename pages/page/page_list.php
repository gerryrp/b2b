<html>

<head>

    <?php
    include(__DIR__ . '/../head.php');
    ?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');

    ?>
    <br>
    <br>
    <br>
    <!-- isi content adasd -->

    <div class="container w-11/12 mx-auto">

        <div class="container max-w-full">

            <div class="w-full border-0 border-b border-solid border-black">
                <div class="flex md-4">
                    <span class="span-title">
                        <h2>Page List</h2>
                    </span>
                </div>
            </div>

        </div>

        <div class="container mt-5">
            <div class="flex md-4">

                <div class="container w-2/5">
                    &nbsp
                </div>

                <div class="container w-2/5 flex items-center">

                    <div class="text-small ">
                        Search : &nbsp
                    </div>

                    <div class="flex md-4">
                        <input type="text" class="input">
                        <button type="submit" class="fa fa-search ml-1" style="background-color: rgba(255, 255, 255, 0);border-color: rgba(255, 255, 255, 0)"></button>
                    </div>

                </div>

                <div class="flex w-1/5 items-center justify-end">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/menu/menu_add.php';" type="button" type="button" class="btn btn-sm btn-success btn-rounded mr-3  " value="Reset"><i class="fas fa-plus"></i> Add</button>
                    <button type="button" class="btn btn-sm btn-primary btn-rounded mr-3  " value="Reset"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>

            </div>
        </div>

        <div class="container  max-w-full bg-white mt-5">
            <div class="md-4">

                <div class="container w-full">
                    <div class="flex">

                        <div class="w-full bg-gray-400 ">
                            <div class="container">
                                <div class="headtable flex">
                                    <div class="w-4/12 text-left">No.</div>
                                    <div class="w-4/12 text-left">Name</div>
                                    <div class="w-4/12"></div>
                                </div>

                                <div class="rowtable flex">
                                    <div class="w-4/12">
                                        <div class=" text-left">1</div>
                                    </div>
                                    <div class="w-4/12">
                                        <div class=" text-left">
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/page/page_detail.php';" class="span span span-bluelink">admin-group</span>
                                        </div>
                                    </div>
                                    <div class="w-4/12">
                                        <div class=" text-right mr-3">
                                            <button> <i class="fas fa-pencil-alt"> </i> </button>
                                            <button> <i class="fas fa-times"> </i> </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="rowtable flex">
                                    <div class="w-4/12">
                                        <div class=" text-left">2</div>
                                    </div>
                                    <div class="w-4/12">
                                        <div class=" text-left">
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/page/page_detail.php';" class="span span span-bluelink">admin-reservation</span>
                                        </div>
                                    </div>
                                    <div class="w-4/12">
                                        <div class=" text-right mr-3">
                                            <button> <i class="fas fa-pencil-alt"> </i> </button>
                                            <button> <i class="fas fa-times"> </i> </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="rowtable flex">
                                    <div class="w-4/12">
                                        <div class=" text-left">3</div>
                                    </div>
                                    <div class="w-4/12">
                                        <div class=" text-left">
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/page/page_detail.php';" class="span span span-bluelink">admin-session</span>
                                        </div>
                                    </div>
                                    <div class="w-4/12">
                                        <div class=" text-right mr-3">
                                            <button> <i class="fas fa-pencil-alt"> </i> </button>
                                            <button> <i class="fas fa-times"> </i> </button>
                                        </div>
                                    </div>
                                </div>


                                <div class="rowtable flex">
                                    <div class="w-4/12">
                                        <div class=" text-left">4</div>
                                    </div>
                                    <div class="w-4/12">
                                        <div class=" text-left">
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/page/page_detail.php';" class="span span span-bluelink">agent</span>
                                        </div>
                                    </div>
                                    <div class="w-4/12">
                                        <div class=" text-right mr-3">
                                            <button> <i class="fas fa-pencil-alt"> </i> </button>
                                            <button> <i class="fas fa-times"> </i> </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="rowtable flex">
                                    <div class="w-4/12">
                                        <div class=" text-left">4</div>
                                    </div>
                                    <div class="w-4/12">
                                        <div class=" text-left">
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/page/page_detail.php';" class="span span span-bluelink">alinvoice</span>
                                        </div>
                                    </div>
                                    <div class="w-4/12">
                                        <div class=" text-right mr-3">
                                            <button> <i class="fas fa-pencil-alt"> </i> </button>
                                            <button> <i class="fas fa-times"> </i> </button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>

    </div>


    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>