<html>

<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');

    ?>

    <!-- isi content -->

    <div class="container bg-gray-200 mx-auto w-11/12 mt-10 p-5">


        <div class="container max-w-full">
            <div class="w-full boxrow">
                <div class="flex">
                    <span class="span-title">
                        <h2>Mark Up Period List</h2>
                    </span>
                </div>
            </div>
        </div>

        <div class="flex mb-4 mt-3">
            <div class="w-4/12 bg-red">

            </div>

            <div class="w-4/12 items-center justify-center flex">
                <div>
                    Search :
                    <input type="text" class="input">
                    <button><i class="fas fa-search"></i> </button>
                </div>
            </div>

            <div class="w-4/12 bg-blue items-center justify-end flex">
                <div class="mr-1">
                    <button class="btn btn-sm btn-rounded btn-success"><i class="fas fa-plus"></i> Add</button>
                </div>
                <div class="mr-1">
                    <button class="btn btn-sm btn-rounded btn-primary"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>
            </div>

        </div>


        <div class="container  max-w-full">
            <div class="md-4">

                <div class="container">
                    <div class="flex">

                        <div class="mx-auto w-full  ">
                            <div class="container">
                                <div class='flex headtable'>
                                    <div class='w-2/12'>No.</div>
                                    <div class='w-2/12'>Description</div>
                                    <div class='w-2/12'>Start Date</div>
                                    <div class='w-2/12'>End Date</div>
                                    <div class='w-2/12'>Type</div>
                                    <div class='w-2/12'>Amount</div>
                                    <div class='w-2/12'>Created</div>
                                </div>

                                <div class='flex rowtable'>
                                    <div class='w-2/12'>
                                        <div class=''>1</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/setting/imarksetting_detail.php';" class="span span span-bluelink">Dom + Inter</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>11 Jul 2018</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>31 Jan 2019</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>nominal</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>75000</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>11 Jul 2018 14:20</div>
                                    </div>
                                </div>

                                <div class='flex rowtable'>
                                    <div class='w-2/12'>
                                        <div class=''>2</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>
                                            <span onclick="window.location.href = 'hotel_detail.php';" class="span span span-bluelink">all season</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>20 Mar 2018</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>30 Apr 2018</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>nominal</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>50000</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>20 Mar 2018 12:08</div>
                                    </div>
                                </div>

                                <div class='flex rowtable'>
                                    <div class='w-2/12'>
                                        <div class=''>3</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>
                                            <span onclick="window.location.href = 'hotel_detail.php';" class="span span span-bluelink">Low season rate</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>21 Jun 2017</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>23 Dec 2017</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>nominal</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>100000</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>28 Feb 2017 10:56</div>
                                    </div>
                                </div>

                                <div class='flex rowtable'>
                                    <div class='w-2/12'>
                                        <div class=''>4</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>
                                            <span onclick="window.location.href = 'hotel_detail.php';" class="span span span-bluelink">Low season rate</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>1 Apr 2017</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>20 Jun 2017</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>nominal</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>100000</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>28 Feb 2017 10:42</div>
                                    </div>
                                </div>

                                <div class='flex rowtable'>
                                    <div class='w-2/12'>
                                        <div class=''>5</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/setting/imarkdetail_detail.php';" class="span span span-bluelink">EXTEND LOW SEASON</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>30 Mar 2017 </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>31 Mar 2017</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>nominal</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>100000</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>24 Feb 2017 11:30</div>
                                    </div>
                                </div>

                                <div class='flex rowtable'>
                                    <div class='w-2/12'>
                                        <div class=''>6</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>
                                            <span onclick="window.location.href = 'hotel_detail.php';" class="span span span-bluelink">Low season rate</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>18 Feb 2017</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>29 Mar 2017</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>nominal</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>100000</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>17 Feb 2017 09:39</div>
                                    </div>
                                </div>



                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>


    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>