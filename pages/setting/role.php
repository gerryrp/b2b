<html>

<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');

    ?>

    <!-- isi content -->

    <div class="container bg-gray-200 mx-auto w-11/12 mt-10 p-5">


        <div class="container max-w-full">
            <div class="w-full boxrow">
                <div class="flex md-4">
                    <span class="span-title">
                        <h2>User Role List</h2>
                    </span>
                </div>
            </div>
        </div>

        <div class="flex mb-4 mt-3">
            <div class="w-4/12 bg-red">

            </div>

            <div class="w-4/12 items-center justify-center flex">
                <div>
                    Search :
                    <input type="text" class="input">
                    <button><i class="fas fa-search"></i> </button>
                </div>
            </div>

            <div class="w-4/12 bg-blue items-center justify-end flex">
                <div class="mr-1">
                    <button class="btn btn-sm btn-rounded btn-success"><i class="fas fa-plus"></i> Add</button>
                </div>
                <div class="mr-1">
                    <button class="btn btn-sm btn-rounded btn-primary"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>
            </div>

        </div>


        <div class="container  max-w-full">
            <div class="md-4">

                <div class="container">
                    <div class="flex">

                        <div class="mx-auto w-full  ">
                            <div class="container">
                                <div class='headtable flex'>
                                    <div class='w-4/12'>No.</div>
                                    <div class='w-4/12'>Name</div>
                                    <div class='w-4/12'></div>

                                </div>

                                <div class='rowtable flex'>
                                    <div class='w-4/12'>
                                        <div class=' '>1</div>
                                    </div>
                                    <div class='w-4/12'>
                                        <div class=' text-left'>
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/setting/role_detail.php';" class="span span span-bluelink">API User</span>
                                        </div>
                                    </div>
                                    <div class='w-4/12'>
                                        <div class=' text-right'>
                                            <button type="button"><i class="fas fa-pencil-alt"></i></button>
                                            <button type="button"><i class="fas fa-times"></i></button>
                                        </div>
                                    </div>

                                </div>


                                <div class='rowtable flex'>
                                    <div class='w-4/12'>
                                        <div class=' '>2</div>
                                    </div>
                                    <div class='w-4/12'>
                                        <div class=' text-left'>
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/setting/imarksetting_detail.php';" class="span span span-bluelink">Auditor</span>
                                        </div>
                                    </div>
                                    <div class='w-4/12'>
                                        <div class=' text-right'>
                                            <button type="button"><i class="fas fa-pencil-alt"></i></button>
                                            <button type="button"><i class="fas fa-times"></i></button>
                                        </div>
                                    </div>

                                </div>


                                <div class='rowtable flex'>
                                    <div class='w-4/12'>
                                        <div class=' '>3</div>
                                    </div>
                                    <div class='w-4/12'>
                                        <div class=' text-left'>
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/setting/imarksetting_detail.php';" class="span span span-bluelink">Branch</span>
                                        </div>
                                    </div>
                                    <div class='w-4/12'>
                                        <div class=' text-right'>
                                            <button type="button"><i class="fas fa-pencil-alt"></i></button>
                                            <button type="button"><i class="fas fa-times"></i></button>
                                        </div>
                                    </div>

                                </div>



                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>


    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>