<html>

<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');

    ?>
    <br>
    <br>
    <br>
    <!-- isi content adasd -->


    <div class="container max-w-full mx-auto w-11/12 bg-gray-300 p-3">

        <div class="container max-w-full">
            <div class="w-full boxrow">
                <span style="font-size:20px; font-weight:bold;">
                    <H2>International Hotel Setting</H2>
                </span>
            </div>
        </div>


        <div class="container max-w-full mt-5 flex">
            <div class="w-11/12 mx-auto">

                <h4 class="border-b border-black text-base text-black text-left p-2">Basic Setting</h4>

                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-2/12 text-left text-sm pt-2 font-bold ">
                        Fit Password
                    </div>

                    <div class="w-8/12 items-center flex">
                        <input type="text" class="input" value="xsYMHGbF3LJN8U">
                    </div>

                </div>

                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-2/12 text-left text-sm pt-2 font-bold ">
                        Fitruums Username
                    </div>

                    <div class="w-8/12 items-center flex">
                        <input type="text" class="input" value="HARYONOXMLAPI">
                    </div>

                </div>

                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-2/12 text-left text-sm pt-2 font-bold ">
                        Default Markup Only
                    </div>

                    <div class="w-8/12 items-center flex">
                        <input type="text" class="input" value="false">
                    </div>

                </div>

                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-2/12 text-left text-sm pt-2 font-bold ">
                        Default Markup
                    </div>

                    <div class="w-8/12 items-center flex">
                        <input type="text" class="input" value="150000">
                    </div>

                </div>

                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-2/12 text-left text-sm pt-2 font-bold ">
                        Agent Discount
                    </div>

                    <div class="w-8/12 items-center flex">
                        <input type="text" class="input" value="20000">
                    </div>

                </div>

                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-2/12 text-left text-sm pt-2 font-bold ">
                        Fit Username
                    </div>

                    <div class="w-8/12 items-center flex">
                        <input type="text" class="input" value="HARYONOXMLAPI">
                    </div>

                </div>

                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-2/12 text-left text-sm pt-2 font-bold ">
                        Cache Path
                    </div>

                    <div class="w-8/12 items-center flex">
                        <input type="text" class="input" value="/var/www/b2b/fitcache/">
                    </div>

                </div>

                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-2/12 text-left text-sm pt-2 font-bold ">
                        Notif To
                    </div>

                    <div class="w-8/12 items-center flex">
                        <input type="text" class="input" value="hotel_intl@haryonotours.com">
                    </div>

                </div>

                <div class="flex flex-wrap rowtable rowtable-gray justify-center">

                    <button onclick="window.location.href = 'master_hotelRoomType.php';" class="btn btn-sm btn-rounded btn-light"> Update</button>

                </div>

            </div>
        </div>

        <div class="container max-w-full mt-5 flex">

            <div class="w-11/12 mx-auto">

                <h4 class="border-b border-black text-base text-black text-left p-2">Synchronization Setting</h4>

                <div class="flex flex-wrap rowtable rowtable-gray py-1">
                    <div class="w-2/12 text-left text-sm font-bold ">
                        Insert Countries
                    </div>

                    <div class="w-10/12 items-center flex">
                        <input type="file" class="border border-black">
                    </div>
                </div>

                <div class="flex flex-wrap rowtable rowtable-gray">
                    <div class="w-2/12 text-left text-sm font-bold pt-2 ">
                        Citilink
                    </div>

                    <div class="w-10/12 items-center flex">
                        <input type="text" class="input" value="0,00">
                    </div>
                </div>

                <div class="flex flex-wrap rowtable rowtable-gray">
                    <div class="w-2/12 text-left text-sm font-bold pt-2 ">
                        Garuda
                    </div>

                    <div class="w-10/12 items-center flex">
                        <input type="text" class="input" value="0,00">
                    </div>
                </div>

                <div class="flex flex-wrap rowtable rowtable-gray justify-center">

                    <button onclick="window.location.href = 'master_hotelRoomType.php';" class="btn btn-sm btn-rounded btn-light"><i class="fas fa-save"></i> Save</button>

                </div>

            </div>

        </div>


    </div>


    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>