<html>

<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');

    ?>

    <!-- isi content -->

    <div class="container bg-gray-200 mx-auto w-11/12 mt-10 p-5">


        <div class="container max-w-full">
            <div class="w-full border-0 border-b border-solid border-black">
                <div class="flex md-4">
                    <span class="span-title">
                        <h2>News List</h2>
                    </span>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="flex md-4">

                <div class="w-1/3 bg-red">
                    <?php
                    include(base_path . '/component/padding.php');
                    ?>
                </div>

                <div class="mx-auto flex md-4 items-center">

                    <div class="container">
                        <div class="text-small text-center">
                            Hotel
                        </div>

                        <div class="flex md-4 justify-center items-center">
                            <input type="text" class="input" placeholder="Hotel">
                            <button type="submit" class="fa fa-search ml-1" style="background-color: rgba(255, 255, 255, 0);border-color: rgba(255, 255, 255, 0)"></button>
                        </div>
                    </div>

                </div>

                <div class="flex md-4 w-1/4 items-center justify-end ">

                    <button onclick="window.location.href = '<?= root_path ?>/pages/setting/news_add.php';" type="button" class="btn btn-sm btn-rounded btn-success mr-3 " value="Add"><i class="fas fa-plus"></i> Add</button>

                    <button type="button" class="btn btn-sm btn-rounded btn-danger mr-3 " value="Sign Up"><i class="fas fa-sync-alt"></i> Reset</button>

                </div>

            </div>
        </div>


        <div class="container  max-w-full border-0 border-b border-solid border-gray-300">
            <div class="md-4">

                <div class="container">
                    <div class="flex">

                        <div class="mx-auto w-full  ">
                            <div class="container text-left" id="results">
                                <div class='headtable flex'>
                                    <div class='w-2/12'>No.</div>
                                    <div class='w-2/12'>Title</div>
                                    <div class='w-2/12'>Date Start</div>
                                    <div class='w-2/12'>Date End</div>
                                    <div class='w-2/12'>Content</div>
                                    <div class='w-2/12'></div>
                                </div>

                                <div class='rowtable flex'>
                                    <div class='w-2/12'>
                                        <div class=' '>1</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=' text-left'>
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/setting/news_detail.php';" class="span span span-bluelink">PROMO MULIA BALI</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=' text-left'>19-03-2019</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=' '>03-04-2019</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=' '>
                                            <span class="span span-success font-bold text-lg">MULIA RESORT NUSA DUA…</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=' text-right'>
                                            <button type="button"><i class="fas fa-pencil-alt"></i></button>

                                        </div>
                                    </div>

                                </div>

                                <div class='rowtable flex'>
                                    <div class='w-2/12'>
                                        <div class=' '>2</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=' text-left'>
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/setting/role_detail.php';" class="span span span-bluelink">PROMO ARYADUTA MEDAN</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=' text-left'>06-03-2019</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=' '>30-04-2019</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=' '>
                                            <span class="span span-danger font-bold text-lg">ARYADUTA MEDAN [*5]…</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=' text-right'>
                                            <button type="button"><i class="fas fa-pencil-alt"></i></button>

                                        </div>
                                    </div>

                                </div>

                                <div class='rowtable flex'>
                                    <div class='w-2/12'>
                                        <div class=' '>3</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=' text-left'>
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/setting/role_detail.php';" class="span span span-bluelink">PROMO FOUR POINTS BY SHERATON BALI SEMINYAK</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=' text-left'>18-02-2019</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=' '>30-03-2020</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=' '>
                                            <span class="span span-pink font-bold text-lg">FOUR POINTS BY SHERATON…</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=' text-right'>
                                            <button type="button"><i class="fas fa-pencil-alt"></i></button>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <div class="container max-w-full mt-3">
            <form>
                <select class="inputselect">
                    <option value="0">0</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
            </form>
        </div>


    </div>


    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>