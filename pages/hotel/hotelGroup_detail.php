<html>

<head>
    <title>B2B Haryono</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php
    include(__DIR__ . '/../head.php');
    ?>

</head>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');

    ?>
    <br>
    <br>
    <br>
    <!-- isi content -->

    <div class="container w-11/12 bg-gray-200 mx-auto p-5">

        <div class="container w-full">

            <div class="w-full boxrow">
                <div class="flex md-4">
                    <span class="span-title">
                        <h2>Hotel Group Detail</h2>
                    </span>
                </div>
            </div>

        </div>

        <div class="container">

            <div class="flex justify-end ">
                <div class="mr-1">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/hotel/hotelGroup_edit.php';" class="btn btn-sm btn-rounded btn-warning"><i class="fas fa-pencil-alt"></i> Edit</button>
                </div>

                <div class="mr-1">
                    <button class="btn btn-sm btn-rounded btn-danger"><i class="fas fa-times"></i> Delete</button>
                </div>

                <div class="mr-1">
                    <button onclick="window.location.href = 'hotelGroup_add.php';" class="btn btn-sm btn-rounded btn-success"><i class="fas fa-plus"></i> Add</button>
                </div>

                <div class="mr-1">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/hotel/hotelGroup.php';" class="btn btn-sm btn-rounded btn-light"><i class="fas fa-list"></i> List</button>
                </div>
            </div>

        </div>

        <div class="container w-full mt-5 ">

            <h4 class="headtable headtable-navy">Hotel Group Information</h4>
            <div class="flex flex-wrap rowtable ">

                <div class="w-1/5 text-left text-xs font-bold pt-2 ">
                    Name
                </div>

                <div class="w-1/2 items-center text-xs flex">
                    TAUZIA HOTELS
                </div>

            </div>

            <div class="flex flex-wrap rowtable ">

                <div class="w-1/5 text-left text-xs font-bold pt-2 ">
                    Description
                </div>

                <div class="w-1/2 items-center text-xs flex">

                </div>

            </div>

        </div>



        <div class="container w-full mt-8 ">
            <div class="w-full mb-2">
                <div class="flex md-4">
                    <span class="span-title text-sm">
                        <h2>Hotel List (7)</h2>
                    </span>
                </div>
            </div>


            <div class="container w-full">
                <div class="flex">

                    <div class="w-full bg-gray-400 ">
                        <div class="container">
                            <div class='headtable flex'>
                                <div class='w-1/12'>#</div>
                                <div class='w-4/12'>Name</div>
                                <div class='w-3/12'>City</div>
                                <div class='w-4/12'>Address</div>

                            </div>

                            <div class='rowtable flex'>
                                <div class='w-1/12'>
                                    <div class=''>1</div>
                                </div>
                                <div class='w-4/12'>
                                    <div class=''>
                                        <span onclick="window.location.href = 'hotelGroup_detail.php';" class="span span span-bluelink">HARRIS BATAM CENTER</span>
                                    </div>
                                </div>
                                <div class='w-3/12'>
                                    <div class=''>BATAM</div>
                                </div>
                                <div class='w-4/12'>
                                    <div class=''>JL. ENGKU PUTRI, BATAM 29641</div>
                                </div>
                            </div>

                            <div class='rowtable flex'>
                                <div class='w-1/12'>
                                    <div class=''>2</div>
                                </div>
                                <div class='w-4/12'>
                                    <div class=''>
                                        <span class="span span span-bluelink">PESONA ALAM RESORT & SPA ( PREV. HARRIS PUNCAK)</span>
                                    </div>
                                </div>
                                <div class='w-3/12'>
                                    <div class=''>PUNCAK</div>
                                </div>
                                <div class='w-4/12'>
                                    <div class=''>Jl. Taman Safari no.101, Kp. Baru Tegal RT 003</div>
                                </div>
                            </div>

                            <div class='rowtable flex'>
                                <div class='w-1/12'>
                                    <div class=''>3</div>
                                </div>
                                <div class='w-4/12'>
                                    <div class=''>
                                        <span class="span span span-bluelink">THE WUJIL RESORT & CONVENTIONS UNGARAN</span>
                                    </div>
                                </div>
                                <div class='w-3/12'>
                                    <div class=''>SEMARANG</div>
                                </div>
                                <div class='w-4/12'>
                                    <div class=''>JL.SOEKARNO-HATTA KM 25,5</div>
                                </div>
                            </div>

                            <div class='rowtable flex'>
                                <div class='w-1/12'>
                                    <div class=''>4</div>
                                </div>
                                <div class='w-4/12'>
                                    <div class=''>
                                        <span class="span span span-bluelink">HARRIS MALANG</span>
                                    </div>
                                </div>
                                <div class='w-3/12'>
                                    <div class=''>MALANG</div>
                                </div>
                                <div class='w-4/12'>
                                    <div class=''>Jl. A YANI UTARA RIVERSIDE BlOCK C-1</div>
                                </div>
                            </div>

                            <div class='rowtable flex'>
                                <div class='w-1/12'>
                                    <div class=''>5</div>
                                </div>
                                <div class='w-4/12'>
                                    <div class=''>
                                        <span class="span span span-bluelink">POP! HOTEL TIMOHO</span>
                                    </div>
                                </div>
                                <div class='w-3/12'>
                                    <div class=''>YOGYAKARTA</div>
                                </div>
                                <div class='w-4/12'>
                                    <div class=''>JL.IPDA TUT HARSONO NO.11</div>
                                </div>
                            </div>

                            <div class='rowtable flex'>
                                <div class='w-1/12'>
                                    <div class=''>6</div>
                                </div>
                                <div class='w-4/12'>
                                    <div class=''>
                                        <span class="span span span-bluelink"> POP! HOTEL BANJARMASIN</span>
                                    </div>
                                </div>
                                <div class='w-3/12'>
                                    <div class=''>BANJARMASIN</div>
                                </div>
                                <div class='w-4/12'>
                                    <div class=''>JL.H.DJOK MENTAYA NO.50 KERTAK BARU ILIR</div>
                                </div>
                            </div>

                            <div class='rowtable flex'>
                                <div class='w-1/12'>
                                    <div class=''>7</div>
                                </div>
                                <div class='w-4/12'>
                                    <div class=''>
                                        <span class="span span span-bluelink">HARRIS SEMINYAK</span>
                                    </div>
                                </div>
                                <div class='w-3/12'>
                                    <div class=''>BALI</div>
                                </div>
                                <div class='w-4/12'>
                                    <div class=''>JL. DRUPADI 99, SEMINYAK</div>
                                </div>
                            </div>

                        </div>
                    </div>


                </div>
            </div>

        </div>



    </div>

    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>