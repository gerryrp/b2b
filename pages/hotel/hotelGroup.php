<html>

<head>

    <?php
    include(__DIR__ . '/../head.php');
    ?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');

    ?>
    <br>
    <br>
    <br>
    <!-- isi content adasd -->

    <div class="container w-11/12 mx-auto">

        <div class="container max-w-full">

            <div class="w-full boxrow">
                <div class="flex md-4">
                    <span class="span-title">
                        <h2>Master Hotel Group</h2>
                    </span>
                </div>
            </div>

        </div>

        <div class="container mt-5">
            <div class="flex md-4">

                <div class="container w-2/5">
                    &nbsp
                </div>

                <div class="container w-2/5 flex items-center">

                    <div class="text-small ">
                        Search : &nbsp
                    </div>

                    <div class="flex md-4">
                        <input type="text" class="input ">
                        <button type="submit" class="fa fa-search ml-1" style="background-color: rgba(255, 255, 255, 0);border-color: rgba(255, 255, 255, 0)"></button>
                    </div>

                </div>

                <div class="flex w-1/5 items-center justify-end">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/hotel/hotelGroup_add.php';" type="button" type="button" class="btn btn-sm btn-success btn-rounded mr-3  " value="Reset"><i class="fas fa-plus"></i> Add</button>
                    <button type="button" class="btn btn-sm btn-danger btn-rounded mr-3  " value="Reset"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>

            </div>
        </div>

        <div class="container  max-w-full bg-white mt-5">

            <div class="flex">

                <div class="container w-full bg-gray-400 ">
                    
                    <div class='flex headtable'>
                        <div class='w-2/12'>No.</div>
                        <div class='w-2/12'>Name</div>
                        <div class='w-2/12'>Description</div>
                        <div class='w-2/12'>#Hotel in Group</div>

                    </div>

                    <div class='flex rowtable'>
                        <div class='w-2/12'>
                            <div class='table_cell'>1</div>
                        </div>
                        <div class='w-2/12'>
                            <div class='table_cell'>
                                <span onclick="window.location.href = '<?= root_path ?>/pages/hotel/hotelGroup_detail.php';" class="span span span-bluelink">TAUZIA</span>
                            </div>
                        </div>
                        <div class='w-2/12'>
                            <div class='table_cell'></div>
                        </div>
                        <div class='w-2/12'>
                            <div class='table_cell'>7</div>
                        </div>

                    </div>

                    <div class='flex rowtable'>
                        <div class='w-2/12'>
                            <div class='table_cell'>2</div>
                        </div>
                        <div class='w-2/12'>
                            <div class='table_cell'>
                                <span class="span span span-bluelink">ACCOR</span>
                            </div>
                        </div>
                        <div class='w-2/12'>
                            <div class='table_cell'></div>
                        </div>
                        <div class='w-2/12'>
                            <div class='table_cell'>18</div>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>


    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>