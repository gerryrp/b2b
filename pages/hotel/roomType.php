<html>

<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');

    ?>
    <br>
    <br>
    <br>
    <!-- isi content -->

    <div class="container w-11/12 mx-auto">

        <div class="container max-w-full">

            <div class="w-full boxrow">
                <div class="flex md-4">
                    <span class="span-title">
                        <h2>Room Type List</h2>
                    </span>
                </div>
            </div>

        </div>

        <div class="container">
            <div class="flex md-4">

                <div class="container w-2/5">
                    <?php
                    include(base_path . '/component/padding.php');
                    ?>
                </div>

                <div class="container w-2/5 flex items-center">

                    <div class="text-small ">
                        Hotel : &nbsp
                    </div>

                    <div class="flex md-4">
                        <input type="text" class="input" placeholder="Hotel">
                        <button type="submit" class="fa fa-search ml-1" style="background-color: rgba(255, 255, 255, 0);border-color: rgba(255, 255, 255, 0)"></button>
                    </div>

                </div>

                <div class="flex w-1/5 items-center justify-end">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/hotel/roomType_add.php';" type="button" class="btn btn-sm btn-rounded btn-success mr-3  " value="Sign Up"><i class="fas fa-plus"></i> Add</button>
                    <button type="button" class="btn btn-sm btn-primary btn-rounded mr-3  " value="Sign Up"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>

            </div>
        </div>

        <div class="container max-w-full bg-white">
            <div class="flex md-4">

                <div class="container w-full">
                    <div class="flex ">

                        <div class="bg-gray-400 w-full ">
                            <div class="container" id="results">
                                <div class='flex headtable'>
                                    <div class='w-1/12'>No.</div>
                                    <div class='w-2/12'>Room Type</div>
                                    <div class='w-2/12'>Additional</div>
                                    <div class='w-2/12'>Description</div>
                                    <div class='w-2/12'>Group Type</div>
                                    <div class='w-2/12'></div>
                                </div>

                                <div class='flex rowtable '>
                                    <div class='w-1/12'>
                                        <div class=''>1</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''></div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''></div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''></div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''></div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>
                                            <i class="fas fa-pencil-alt"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class='flex rowtable '>
                                    <div class='w-1/12'>
                                        <div class=''>2</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/hotel/roomType_detail.php';" class="span span span-bluelink"> 2 BED MANGGAR VILLAS</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''></div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>2 BED MANGGAR VILLAS</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>2 BED MANGGAR VILLAS</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>
                                            <i class="fas fa-pencil-alt"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class='flex rowtable '>
                                    <div class='w-1/12'>
                                        <div class=''>3</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>
                                            <span class="span span span-bluelink">
                                                1 BED ROOM VILLA
                                            </span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''></div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>1 BED ROOM VILLA</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>1 BED ROOM VILLA</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>
                                            <i class="fas fa-pencil-alt"></i>
                                        </div>
                                    </div>
                                </div>

                                <div class='flex rowtable '>
                                    <div class='w-1/12'>
                                        <div class=''>4</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>
                                            <span class="span span span-bluelink"> 1 BED</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''></div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''> 1 BED</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''> 1 BED</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class=''>
                                            <i class="fas fa-pencil-alt"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>

    </div>

    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>