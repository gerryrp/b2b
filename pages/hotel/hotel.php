<html>

<?php
include(__DIR__ . "/../head.php");
?>



<body>

    <?php
    include(base_path . "/component/topnav.php");
    include(base_path . "/component/slider.php");

    ?>

    <!-- isi content adasd -->

    <div class="container bg-gray-200 mx-auto w-11/12 mt-10 p-5">

        <div class="container max-w-full">

            <div class="container max-w-full">
                <div class="w-full boxrow">
                    <div class="flex md-4">
                        <span class="span-title">
                            <h2>Master Hotel</h2>
                        </span>
                    </div>
                </div>
            </div>

            <div class="container max-w-full mt-2 ml-5">
                <div class="flex md-4">

                    <div class="mx-auto flex md-4 items-center">

                        <div class="mr-10 flex mt-3">
                            <form>
                                <select class="inputselect">
                                    <option value="All Country">All Country</option>
                                    <option value="Domestic">Domestic</option>
                                    <option value="Foreign">Foreign</option>
                                </select>
                            </form>
                        </div>

                        <div class="container">
                            <div class="text-small text-center">
                                Hotel
                            </div>

                            <div class="flex md-4 justify-center items-center">
                                <input type="text" class="input border border-gray-400" placeholder="Hotel">
                                <button type="submit" class="fa fa-search ml-1" style="background-color: rgba(255, 255, 255, 0);border-color: rgba(255, 255, 255, 0)"></button>
                            </div>
                        </div>

                        <div class="ml-10 mt-3">
                            <form>
                                <select class="inputselect">
                                    <option value=""></option>
                                    <option value="Instant Confirm">Instant Confirm</option>
                                    <option value="XML">XML</option>
                                    <option value="ERA">ERA</option>
                                    <option value="On Request">On Request</option>
                                </select>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>


        <div class="container ">

            <div class="flex md-4">

                <div class="flex md-4 w-10/12">
                    <?php
                    include('../../component/padding.php');
                    ?>
                </div>

                <div class="flex md-4 w-1/4 items-center justify-end ">
                    <button type="button" class="btn btn-sm btn-rounded-full btn-secondary mr-3 text-xs " value="Sign Up">Incentive Report</button>
                    <button onclick="window.location.href = '<?= root_path ?>/pages/hotel/hotel_add.php';" type="button" class="btn btn-sm btn-rounded btn-success mr-3 " value=""><i class="fas fa-plus"></i> Add</button>
                    <button type="button" class="btn btn-sm btn-rounded btn-primary mr-3 " value="Sign Up"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>

            </div>

        </div>


        <div class="container  max-w-full relative ">

            <div class="">

                <div class="container">
                    <div class="flex">

                        <div class="w-full mx-auto bg-gray-400   ">
                            <div class="container">
                                <div class="flex headtable">
                                    <div class="w-1/12">No.</div>
                                    <div class="w-3/12">Name</div>
                                    <div class="w-1/12">Allotment Type</div>
                                    <div class="w-3/12">Address</div>
                                    <div class="w-1/12">City</div>
                                    <div class="w-2/12">Email</div>
                                    <div class="w-1/12">Star</div>
                                    <div class="w-1/12">&nbsp</div>
                                </div>

                                <div class="flex rowtable">

                                    <div class="w-1/12">
                                        <div class="">1</div>
                                    </div>

                                    <div class="w-3/12">
                                        <div class="">
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/hotel/hotel_detail.php';" class="span span-bluelink">FAVE HOTEL LOSARI MAKASSAR (FORMERLY FAVE DAENG TOMPO)</span>
                                        </div>
                                    </div>

                                    <div class="w-1/12">
                                        <div class="">IC</div>
                                    </div>

                                    <div class="w-3/12">
                                        <div class="">JL. DAENG TOMPO 28-36 MAKASSAR</div>
                                    </div>

                                    <div class="w-1/12">
                                        <div class="">Makassar</div>
                                    </div>

                                    <div class="2/22">
                                        <div class=""> engineer@haryono.co.id
                                            <span class="badge badge-primary">email</span>
                                        </div>
                                    </div>

                                    <div class="w-1/12">
                                        <div class="">
                                            <i class="fas fa-star"></i>
                                            <i class="fas fa-star"></i>
                                        </div>
                                    </div>

                                    <div class="w-1/12">
                                        <div class="flex">
                                            <button onclick="window.location.href = 'hotelrate_detail.php';" class="btn btn-sm btn-light">Rate</button>
                                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-pencil-alt"></i></button>
                                        </div>
                                    </div>

                                </div>

                                <div class="flex rowtable">
                                    <div class="w-1/12">
                                        <div class="">2</div>
                                    </div>
                                    <div class="w-3/12">
                                        <div class="">
                                            <span class="span span span-bluelink">Grand Clarion Hotel & Convention Kendari</span>
                                        </div>
                                    </div>
                                    <div class="w-1/12">
                                        <div class="">OR</div>
                                    </div>
                                    <div class="w-3/12">
                                        <div class="">Jalan Edy Sabara No.89, Kendari Barat, Lahundape, Kendari Bar., Kota Kendari, Sulawesi Tenggara 93121</div>
                                    </div>
                                    <div class="w-1/12">
                                        <div class="">Kendari</div>
                                    </div>
                                    <div class="2/22">
                                        <div class="">engineer@haryono.co.id
                                            <span class="badge badge-primary">email</span>
                                        </div>
                                    </div>
                                    <div class="w-1/12">
                                        <div class=" flex justify-center">

                                        </div>
                                    </div>
                                    <div class="w-1/12">
                                        <div class="">
                                            <button class="btn btn-sm btn-light">Rate</button>
                                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-pencil-alt"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <!-- <div class="overlay overlay-relative justify-center">
                <div class="loader mt-20">

                </div>
                <div class="text-2xl text-white font-bold ml-5">
                    Searching...
                </div>
            </div> -->

        </div>

    </div>


    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>