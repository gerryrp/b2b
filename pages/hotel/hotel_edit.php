<html>

<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');

    ?>
    <br>
    <br>
    <br>
    <!-- isi content adasd -->


    <div class="container max-w-full mx-auto w-11/12">

        <div class="container max-w-full">
            <div class="w-full boxrow">
                <span style="font-size:20px; font-weight:bold;">
                    <H2>Edit Hotel</H2>
                </span>
            </div>
        </div>

        <div class="container max-w-full mt-8">
            <div class="flex">

                <div class="w-8/12 items-center flex">
                    <span class="bg-orange-400 font-bold p-2">Please input valid Reservation Email address and Allow Email for Instant Confirmation</span>
                </div>

                <div class="flex w-4/12 justify-end ">

                    <div class="mr-1">
                        <button onclick="window.location.href = '<?= root_path ?>/pages/hotel/hotel.php';" class="btn btn-sm btn-rounded btn-light"><i class="fas fa-list"></i> List</button>
                    </div>

                    <div class="mr-1">
                        <button onclick="window.location.href = '<?= root_path ?>/pages/users/user_add.php';" class="btn btn-sm btn-rounded btn-success"><i class="fas fa-plus"></i> Add User</button>
                    </div>

                </div>

            </div>
        </div>

        <div class="container max-w-full mt-5 flex">
            <div class="w-7/12">

                <h4 class="headtable headtable">Hotel Information</h4>

                <div class="w-full flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-4/12 text-left text-sm flex items-center font-bold ">
                        Name
                    </div>

                    <div class="w-8/12 text-xs items-center flex">
                        <input type="text" class="input text-xs" style="width:300" value="FAVE HOTEL LOSARI MAKASSAR (FORMERLY FAVE DAENG TOMPO)">
                    </div>

                </div>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        Address
                    </div>

                    <div class="w-8/12 items-center">
                        <input type="text" class="input text-xs" style="width:450" value="JL. DAENG TOMPO 28-36 MAKASSAR">
                        <input type="text" class="input text-xs mt-1" style="width:450" value="NULL">
                    </div>
                    
                </div>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        City
                    </div>

                    <div class="w-8/12 items-center flex">
                        <input type="text" class="input text-xs" style="width:300" value="MAKASSAR, SULAWESI-SOUTH">
                    </div>

                </div>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        Phone
                    </div>

                    <div class="w-8/12 items-center flex">
                        <input type="text" class="input text-xs " style="width:200" value="0411-363 9777">
                    </div>

                </div>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        Fax
                    </div>

                    <div class="w-8/12 items-center flex">
                        <input type="text" class="input  text-xs" style="width:200" value="0411-363 8008">
                    </div>

                </div>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        Hotel Group
                    </div>

                    <div class="w-8/12 items-center flex">

                        <form>
                            <select class="border border-gray-600 border-solid rounded mt-3Fh">
                                <option value=""></option>
                                <option value="">TAUZIA ACCOR</option>
                                <option value="">ACCOR</option>
                            </select>
                        </form>

                    </div>

                </div>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        Website
                    </div>

                    <div class="w-8/12 items-center flex">
                        <input type="text" class="input text-xs" style="width:300" value="www.favehotels.com, daengtompo.favehotels.com">
                    </div>

                </div>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        Star
                    </div>

                    <div class="w-8/12 items-center flex">
                        <form>
                            <select class="inputselect rounded">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </form>
                    </div>

                </div>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        Reservation Email
                    </div>

                    <div class="w-8/12 items-center flex">
                        <input type="text" class="input text-xs" style="width:450" value="engineer@haryono.co.id">
                    </div>

                </div>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        Sales Email
                    </div>

                    <div class="w-8/12 items-center flex">
                        <input type="text" class="input text-xs" style="width:450" value="daengtomposmm@favehotels.com">
                    </div>

                </div>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        Remarks
                    </div>

                    <div class="w-8/12 items-center flex">
                        <textarea rows="6" cols="29" class="border border-gray-400 rounded-lg text-xs ">Cancellation: Low = 7 days, High/Peak = 17 days prior to arrival, EXTRA BED = N/A     
                            </textarea>
                    </div>

                </div>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        Admin Notes
                    </div>

                    <div class="w-8/12 items-center flex">
                        <textarea rows="6" cols="29" class="border border-gray-400 rounded-lg text-xs ">daengtompoinfo@favehotels.com, daengtomposmm@favehotels.com    
                            </textarea>
                    </div>

                </div>
            </div>

            <div class="w-1/3 ml-10">
                <h4 class="headtable headtable ">&nbsp</h4>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        Has Contract
                    </div>

                    <div class="w-1/2 items-center flex">
                        <span class="span span-success font-bold">YES</span>
                    </div>

                    <div>
                        <button onclick="window.location.href = '<?= root_path ?>/pages/rate/hotelRate_detail.php';" class="btn btn-sm btn-rounded btn-light">Rate</button>
                    </div>

                </div>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        Allotment Type
                    </div>

                    <div class="w-1/3 items-center flex">
                        <form>
                            <select class="border border-gray-600 border-solid rounded mt-3 text-xs ">
                                <option value="Instant Confirm">Instant Confirm</option>
                                <option value="XML">XML</option>
                                <option value="ERA">ERA</option>
                                <option value="On Request">On Request</option>
                            </select>
                        </form>
                    </div>



                </div>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        Company ID
                    </div>

                    <div class="w-1/2 items-center flex">
                        <input type="text" class="input text-xs" style="width:300" value="favedaengtompo">
                    </div>

                </div>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        Allow Email
                    </div>

                    <div class="w-1/2 items-center flex">
                        <span>
                            <input type="checkbox" class="check" id="dropdownCheck1" data-label="remember me">
                            <label class="" for="dropdownCheck1">
                                Allow Email
                            </label>
                        </span>
                    </div>

                </div>
                <div class="flex flex-wrap rowtable rowtable-gray ">

                    <div class="w-1/3 text-left text-sm flex items-center font-bold ">
                        Reservation
                    </div>

                    <div class="w-1/2 items-center flex">
                        <span>
                            <input type="checkbox" class="check" id="dropdownCheck2" data-label="remember me">
                            <label class="" for="dropdownCheck2">
                                Disabled
                            </label>
                        </span>
                    </div>

                </div>

                <div class="text-sm mt-3">
                    <p class="font-bold">Public Announcement</p>

                    <textarea rows="6" cols="29" class="border border-solid border-gray-600 rounded-lg "></textarea>
                </div>

            </div>
        </div>

        <div class="container w-full mt-3 flex justify-center">
            <button onclick="window.location.href = 'master_hotelRoomType.php';" class="btn btn-sm btn-rounded btn-light"><i class="fas fa-save"></i> Save</button>
            <button onclick="window.location.href = 'master_hotelRoomType.php';" class="btn btn-sm btn-rounded btn-light"><i class="fas fa-undo"></i> Cancel</button>

        </div>

        <div class="container max-w-full mt-5 flex">
            <div class="w-5/12">
                <h4 class="text-base font-bold">Hotel Rooms</h4>

                <div class="flex flex-wrap overflow-hidden bg-gray-300 mb-1 p-1">

                    <div class="w-2/12 text-left text-sm flex items-center items-center flex ">
                        Room Type :
                    </div>

                    <div class="w-8/12 text-left text-sm flex items-center items-center flex">
                        <input type="text" class="input input-sm " value="SUPERIOR ( 21.7m2 )">
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light">disable</button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-down"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-up"></i></button>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <input type="text" class="input input-sm " value="SUPERIOR ( 21.7m2 )">
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        </div>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <form>
                                <select class=" border-solid rounded">
                                    <option value="NO WINDOW"> NO WINDOW</option>
                                    <option value="100 sqm"> 100 sqm</option>
                                    <option value="2A + 2C UNDER 12 YO"> 2A + 2C UNDER 12 YO</option>
                                    <option value="65 SQM"> 65 SQM</option>
                                    <option value="130 SQM"> 130 SQM</option>
                                </select>
                            </form>

                            <button class="btn btn-sm btn-rounded btn-light">Add</button>
                        </div>
                    </div>

                </div>

                <div class="flex flex-wrap overflow-hidden bg-gray-300 mb-1 p-1">

                    <div class="w-2/12 text-left text-sm flex items-center items-center flex ">
                        Room Type :
                    </div>

                    <div class="w-8/12 text-left text-sm flex items-center items-center flex">
                        <input type="text" class="input input-sm " value="SUPERIOR ( 21.7m2 )">
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light">disable</button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-down"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-up"></i></button>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <input type="text" class="input input-sm " value="Breakfast for 2">
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        </div>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <form>
                                <select class=" border-solid rounded">
                                    <option value="NO WINDOW"> NO WINDOW</option>
                                    <option value="100 sqm"> 100 sqm</option>
                                    <option value="2A + 2C UNDER 12 YO"> 2A + 2C UNDER 12 YO</option>
                                    <option value="65 SQM"> 65 SQM</option>
                                    <option value="130 SQM"> 130 SQM</option>
                                </select>
                            </form>

                            <button class="btn btn-sm btn-rounded btn-light">Add</button>
                        </div>
                    </div>

                </div>

                <div class="flex flex-wrap overflow-hidden bg-gray-300 mb-1 p-1">

                    <div class="w-2/12 text-left text-sm flex items-center items-center flex ">
                        Room Type :
                    </div>

                    <div class="w-8/12 text-left text-sm flex items-center items-center flex">
                        <input type="text" class="input input-sm " value="FAMILY">
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light">disable</button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-down"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-up"></i></button>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <input type="text" class="input input-sm " value="Breakfast for 3">
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        </div>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <form>
                                <select class=" border-solid rounded">
                                    <option value="NO WINDOW"> NO WINDOW</option>
                                    <option value="100 sqm"> 100 sqm</option>
                                    <option value="2A + 2C UNDER 12 YO"> 2A + 2C UNDER 12 YO</option>
                                    <option value="65 SQM"> 65 SQM</option>
                                    <option value="130 SQM"> 130 SQM</option>
                                </select>
                            </form>

                            <button class="btn btn-sm btn-rounded btn-light">Add</button>
                        </div>
                    </div>

                </div>

                <div class="flex flex-wrap overflow-hidden bg-gray-300 mb-1 p-1">

                    <div class="w-2/12 text-left text-sm flex items-center items-center flex ">
                        Room Type :
                    </div>

                    <div class="w-8/12 text-left text-sm flex items-center items-center flex">
                        <input type="text" class="input input-sm " value="TRIPLE">
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light">disable</button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-down"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-up"></i></button>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <input type="text" class="input input-sm " value="Room Only">
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        </div>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <form>
                                <select class=" border-solid rounded ">
                                    <option value="NO WINDOW"> NO WINDOW</option>
                                    <option value="100 sqm"> 100 sqm</option>
                                    <option value="2A + 2C UNDER 12 YO"> 2A + 2C UNDER 12 YO</option>
                                    <option value="65 SQM"> 65 SQM</option>
                                    <option value="130 SQM"> 130 SQM</option>
                                </select>
                            </form>

                            <button class="btn btn-sm btn-rounded btn-light">Add</button>
                        </div>
                    </div>

                </div>

                <div class="flex flex-wrap overflow-hidden bg-gray-300 mb-1 p-1">

                    <div class="w-2/12 text-left text-sm flex items-center items-center flex ">
                        Room Type :
                    </div>

                    <div class="w-8/12 text-left text-sm flex items-center items-center flex">
                        <input type="text" class="input input-sm " value="TRIPLE">
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light">disable</button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-down"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-up"></i></button>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <input type="text" class="input input-sm " value="Breakfast for 3">
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        </div>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <form>
                                <select class=" border-solid rounded">
                                    <option value="NO WINDOW"> NO WINDOW</option>
                                    <option value="100 sqm"> 100 sqm</option>
                                    <option value="2A + 2C UNDER 12 YO"> 2A + 2C UNDER 12 YO</option>
                                    <option value="65 SQM"> 65 SQM</option>
                                    <option value="130 SQM"> 130 SQM</option>
                                </select>
                            </form>

                            <button class="btn btn-sm btn-rounded btn-light">Add</button>
                        </div>
                    </div>

                </div>

                <div class="flex flex-wrap overflow-hidden bg-gray-300 mb-1 p-1">

                    <div class="w-2/12 text-left text-sm flex items-center items-center flex ">
                        Room Type :
                    </div>

                    <div class="w-8/12 text-left text-sm flex items-center items-center flex">
                        <input type="text" class="input input-sm " value="FAMILY">
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light">disable</button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-down"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-up"></i></button>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <input type="text" class="input input-sm " value="Room Only">
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        </div>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <form>
                                <select class=" border-solid rounded">
                                    <option value="NO WINDOW"> NO WINDOW</option>
                                    <option value="100 sqm"> 100 sqm</option>
                                    <option value="2A + 2C UNDER 12 YO"> 2A + 2C UNDER 12 YO</option>
                                    <option value="65 SQM"> 65 SQM</option>
                                    <option value="130 SQM"> 130 SQM</option>
                                </select>
                            </form>

                            <button class="btn btn-sm btn-rounded btn-light">Add</button>
                        </div>
                    </div>

                </div>

                <div class="flex flex-wrap overflow-hidden bg-gray-300 mb-1 p-1">

                    <div class="w-2/12 text-left text-sm flex items-center items-center flex ">
                        Room Type :
                    </div>

                    <div class="w-8/12 text-left text-sm flex items-center items-center flex">
                        <input type="text" class="input input-sm " value="SUITE">
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light">disable</button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-down"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-up"></i></button>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <input type="text" class="input input-sm " value="Room Only">
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        </div>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <form>
                                <select class=" border-solid rounded">
                                    <option value="NO WINDOW"> NO WINDOW</option>
                                    <option value="100 sqm"> 100 sqm</option>
                                    <option value="2A + 2C UNDER 12 YO"> 2A + 2C UNDER 12 YO</option>
                                    <option value="65 SQM"> 65 SQM</option>
                                    <option value="130 SQM"> 130 SQM</option>
                                </select>
                            </form>

                            <button class="btn btn-sm btn-rounded btn-light">Add</button>
                        </div>
                    </div>

                </div>

                <div class="flex flex-wrap overflow-hidden bg-gray-300 mb-1 p-1">

                    <div class="w-2/12 text-left text-sm flex items-center items-center flex ">
                        Room Type :
                    </div>

                    <div class="w-8/12 text-left text-sm flex items-center items-center flex">
                        <input type="text" class="input input-sm " value="SUITE ( 40.8 m2 )">
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light">disable</button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-down"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-up"></i></button>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <input type="text" class="input input-sm " value="Breakfast for 2">
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        </div>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <form>
                                <select class=" border-solid rounded">
                                    <option value="NO WINDOW"> NO WINDOW</option>
                                    <option value="100 sqm"> 100 sqm</option>
                                    <option value="2A + 2C UNDER 12 YO"> 2A + 2C UNDER 12 YO</option>
                                    <option value="65 SQM"> 65 SQM</option>
                                    <option value="130 SQM"> 130 SQM</option>
                                </select>
                            </form>

                            <button class="btn btn-sm btn-rounded btn-light">Add</button>
                        </div>
                    </div>

                </div>

                <div class="flex flex-wrap overflow-hidden bg-gray-300 mb-1 p-1">

                    <div class="w-2/12 text-left text-sm flex items-center items-center flex ">
                        Room Type :
                    </div>

                    <div class="w-8/12 text-left text-sm flex items-center items-center flex">
                        <input type="text" class="input input-sm " value="EXECUTIVE SUITE">
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light">disable</button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-down"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-up"></i></button>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <input type="text" class="input input-sm " value="Room Only">
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        </div>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <form>
                                <select class=" border-solid rounded">
                                    <option value="NO WINDOW"> NO WINDOW</option>
                                    <option value="100 sqm"> 100 sqm</option>
                                    <option value="2A + 2C UNDER 12 YO"> 2A + 2C UNDER 12 YO</option>
                                    <option value="65 SQM"> 65 SQM</option>
                                    <option value="130 SQM"> 130 SQM</option>
                                </select>
                            </form>

                            <button class="btn btn-sm btn-rounded btn-light">Add</button>
                        </div>
                    </div>

                </div>

                <div class="flex flex-wrap overflow-hidden bg-gray-300 mb-1 p-1">

                    <div class="w-2/12 text-left text-sm flex items-center items-center flex ">
                        Room Type :
                    </div>

                    <div class="w-8/12 text-left text-sm flex items-center items-center flex">
                        <input type="text" class="input input-sm " value="EXECUTIVE SUITE">
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light">disable</button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-down"></i></button>
                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sort-up"></i></button>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <input type="text" class="input input-sm " value="Breakfast for 2">
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                        </div>
                    </div>

                    <div class="w-full items-center flex ml-32">
                        <div class="w-2/12 text-left text-sm flex items-center  items-center flex mr-3 ">
                            Facility :
                        </div>

                        <div class="w-8/12 text-left text-sm flex items-center  items-center flex">
                            <form>
                                <select class=" border-solid rounded">
                                    <option value="NO WINDOW"> NO WINDOW</option>
                                    <option value="100 sqm"> 100 sqm</option>
                                    <option value="2A + 2C UNDER 12 YO"> 2A + 2C UNDER 12 YO</option>
                                    <option value="65 SQM"> 65 SQM</option>
                                    <option value="130 SQM"> 130 SQM</option>
                                </select>
                            </form>

                            <button class="btn btn-sm btn-rounded btn-light">Add</button>
                        </div>
                    </div>

                </div>

                <div class="boxrow  flex flex-wrap overflow-hidden bg-gray-300 mb-1 p-1">
                    <div class="p-1 ">
                        Room Type <input type="text" class="input text-xs" style="width:300" value="">
                        <button class="btn btn-sm btn-rounded btn-light"> Add</button>

                    </div>
                    <div class="p-1">
                        <span class="text-xs">Note: Room must be from selected drop down</span>
                    </div>
                </div>

            </div>

            <div class="w-7/12">

                <div class="w-full ml-10">
                    <h4 class="text-base font-bold">Hotel Facilities</h4>

                    <div class="w-full bg-gray-300 p-1 flex items-center">
                        <div class="w-11/12 items-center flex">
                            <input type="text" class="input text-xs w-8/12"  value="Free Wi-fi in all area">
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-save"></i></button>
                        </div>
                    </div>

                    <div class="w-full bg-gray-300 p-1 flex items-center">
                        <div class="w-11/12 items-center flex">
                            <input type="text" class="input text-xs w-8/12"  value="Mini Gym">
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-save"></i></button>
                        </div>
                    </div>

                    <div class="w-full bg-gray-300 p-1 flex items-center">
                        <div class="w-11/12 items-center flex">
                            <input type="text" class="input text-xs w-8/12"  value="Bussiness Centre">
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-save"></i></button>
                        </div>
                    </div>

                    <div class="w-full bg-gray-300 p-1 flex items-center">
                        <div class="w-11/12 items-center flex">
                            <input type="text" class="input text-xs w-8/12"  value="sTREATs Bar">
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-save"></i></button>
                        </div>
                    </div>

                    <div class="w-full bg-gray-300 p-1 flex items-center">
                        <div class="w-11/12 items-center flex">
                            <input type="text" class="input text-xs w-8/12"  value="Skyloft Restaurant & Lounge">
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-window-close"></i></button>
                            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-save"></i></button>
                        </div>
                    </div>

                    <div class="w-full bg-gray-300 p-1 flex items-center mt-3">
                        <div class="w-2/12 text-left text-sm flex items-center font-bold pt-2">
                            Facility
                        </div>

                        <div class="w-11/12 items-center flex">
                            <input type="text" class="input text-xs" style="width:300" value="">
                            <button class="btn btn-sm btn-rounded btn-light">Add Hotel Facility</button>
                        </div>
                    </div>
                </div>

                <div class="w-full ml-10 mt-5">
                    <h4 class="text-base font-bold">Nearby Places</h4>

                    <div class="w-full bg-gray-300  flex items-center">
                        <div class="w-1/12 text-left text-sm flex items-center font-bold ">
                            Place
                        </div>

                        <div class="w-11/12 items-center flex">
                            <input type="text" class="input  mx-1" style="width:30px" value="">
                            <form>
                                <select class=" border rounded border-solid border-gray-600 mx-1 mt-3">
                                    <option value="Minute(s)">Minute(s)</option>
                                    <option value="Kilometer(s)">Kilometer(s)</option>
                                </select>
                            </form>
                            <span class="mx-1">to</span>
                            <input type="text" class="input" style="width:300px" value="">
                            <button class="btn btn-sm btn-rounded btn-light">Add</button>
                        </div>
                    </div>
                </div>

            </div>


        </div>

        <div class="container max-w-full mt-5 flex border-dotted border-0 border-b-1 border-solid border-gray-400">
            <div class="w-6/12">
                <h4 class="text-base font-bold">Inactive Rooms</h4>

                <div class="mt-2 flex items-center">
                    <div class="w-6/12">
                        <p>STANDARD (21.7 m2) </p>
                        <p>Room Only</p>
                    </div>
                    <div class="w-6/12">
                        <button class="btn btn-sm btn-rounded btn-light">Enable</button>
                    </div>
                </div>

                <div class="mt-2 flex items-center">
                    <div class="w-6/12">
                        <p>STANDARD (21.7 m2) </p>
                        <p>Breakfast for 2</p>
                    </div>
                    <div class="w-6/12">
                        <button class="btn btn-sm btn-rounded btn-light">Enable</button>
                    </div>
                </div>

                <div class="mt-2 flex items-center">
                    <div class="w-6/12">
                        <p>FAMILY (25.5 m2)</p>
                        <p>Breakfast for 3</p>
                    </div>
                    <div class="w-6/12">
                        <button class="btn btn-sm btn-rounded btn-light">Enable</button>
                    </div>
                </div>

                <div class="mt-2 flex items-center">
                    <div class="w-6/12">
                        <p>EXECUTIVE</p>
                        <p>Room Only</p>
                    </div>
                    <div class="w-6/12">
                        <button class="btn btn-sm btn-rounded btn-light">Enable</button>
                    </div>
                </div>

                <div class="mt-2 flex items-center">
                    <div class="w-6/12">
                        <p>EXECUTIVE (25.5 m2)</p>
                        <p>Breakfast for 2</p>
                    </div>
                    <div class="w-6/12">
                        <button class="btn btn-sm btn-rounded btn-light">Enable</button>
                    </div>
                </div>


                <div class="mt-2 flex items-center">
                    <div class="w-6/12">
                        <p>SUITE</p>
                        <p>Breakfast for 2</p>
                    </div>
                    <div class="w-6/12">
                        <button class="btn btn-sm btn-rounded btn-light">Enable</button>
                    </div>
                </div>


                <div class="mt-2 flex items-center">
                    <div class="w-6/12">
                        <p>STANDARD</p>
                        <p>Breakfast for 2</p>
                    </div>
                    <div class="w-6/12">
                        <button class="btn btn-sm btn-rounded btn-light">Enable</button>
                    </div>
                </div>

                <div class="mt-2 flex items-center">
                    <div class="w-6/12">
                        <p>EXECUTIVE</p>
                        <p>Breakfast for 2</p>
                    </div>
                    <div class="w-6/12">
                        <button class="btn btn-sm btn-rounded btn-light">Enable</button>
                    </div>
                </div>

                <div class="mt-2 flex items-center">
                    <div class="w-6/12">
                        <p>TRIPLE (25.5 m2)</p>
                        <p>Breakfast for 3</p>
                    </div>
                    <div class="w-6/12">
                        <button class="btn btn-sm btn-rounded btn-light">Enable</button>
                    </div>
                </div>

                <div class="mt-2 flex items-center">
                    <div class="w-6/12">
                        <p>SUPERIOR</p>
                        <p>Breakfast for 2</p>
                    </div>
                    <div class="w-6/12">
                        <button class="btn btn-sm btn-rounded btn-light">Enable</button>
                    </div>
                </div>

                <div class="mt-2 flex items-center">
                    <div class="w-6/12">
                        <p>TRIPLE</p>
                        <p>Breakfast for 3</p>
                    </div>
                    <div class="w-6/12">
                        <button class="btn btn-sm btn-rounded btn-light">Enable</button>
                    </div>
                </div>

                <div class="mt-2 flex items-center">
                    <div class="w-6/12">
                        <p>Suite</p>
                        <p></p>
                    </div>
                    <div class="w-6/12">
                        <button class="btn btn-sm btn-rounded btn-light">Enable</button>
                    </div>
                </div>


                <div class="mt-2 flex items-center">
                    <div class="w-6/12">
                        <p>Triple/Family (25.5 m2)</p>
                        <p>Breakfast for 2</p>
                    </div>
                    <div class="w-6/12">
                        <button class="btn btn-sm btn-rounded btn-light">Enable</button>
                    </div>
                </div>


                <div class="mt-2 flex items-center">
                    <div class="w-6/12">
                        <p>Extra Bed</p>
                        <p>Breakfast for 1</p>
                    </div>
                    <div class="w-6/12">
                        <button class="btn btn-sm btn-rounded btn-light">Enable</button>
                    </div>
                </div>

            </div>
        </div>


        <div class="containter w-full p-2 mt-10">
            <div>
                <span class="text-base font-bold">
                    <h2>User List</h2><span>
            </div>

            <div class="flex md-4">
                <div class="w-full mt-1 bg-gray-200">
                    <div class="container">

                        <div class='headtable flex'>
                            <div class='w-4/12'>User ID</div>
                            <div class='w-4/12'>Name</div>
                            <div class='w-4/12'>Email</div>
                            <div class='w-4/12'>Role</div>

                        </div>

                        <div class='rowtable flex'>
                            <div class='w-4/12'>
                                <div class=''>
                                    <span class="span span span-bluelink">daengtomposmm@favehotels.com</span>
                                </div>
                            </div>
                            <div class='w-4/12'>
                                <div class=''>Fave Daeng Tompo Makassar</div>
                            </div>
                            <div class='w-4/12'>
                                <div class=''>daengtomposmm@favehotels.com</div>
                            </div>
                            <div class='w-4/12'>
                                <div class=''>Hotel Sales</div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="container w-full mt-10">
            <div>
                <span class="text-base font-bold">
                    <h2>Log</h2><span>
            </div>

            <div class="w-full mt-1 bg-gray-200">
                <div class="container">

                    <div class='rowtable flex'>
                        <div class='w-4/12'>
                            <div class=''>10 Feb 2019 15:39</div>
                        </div>
                        <div class='w-4/12'>
                            <div class=''>10600</div>
                        </div>
                        <div class='w-4/12'>
                            <div class=''>PRT : 115.178.197.134</div>
                        </div>
                        <div class='w-4/12'>
                            <div class=''>Update Hotel Detail</div>
                        </div>
                        <div class='w-4/12'>
                            <div class=''>
                                <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-info-circle"></i></button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>







    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>