<html>
<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');
    ?>


    <!-- isi content -->

    <div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">

        <div class="w-full">
            <div class="flex md-4">


                <div class="my-2 px-1">
                    <input type="checkbox" class="check" id="Check11" data-label="to">
                    <label class="span span-blue1" for="Check11">
                        Only Me
                    </label>
                </div>

                <div class="my-2 px-1">
                    <input type="checkbox" class="check" id="Check13" data-label="to" checked>
                    <label class="span span-blue1" for="Check13">
                        Branch
                    </label>
                </div>

                <div class="my-2 px-1">
                    <input type="checkbox" class="check" id="Check14" data-label="to" checked>
                    <label class="span span-blue1" for="Check14">
                        Agent
                    </label>
                </div>

                <div class="my-2 px-1">
                    <input type="checkbox" class="check" id="Check15" data-label="to" checked>
                    <label class="span span-blue1" for="Check15">
                        Corporate
                    </label>
                </div>

                <div class="my-2 px-1">
                    <input type="checkbox" class="check" id="Check16" data-label="to" checked>
                    <label class="span span-blue1" for="Check16">
                        FIT
                    </label>
                </div>

                <div class="my-2 px-1">
                    <input type="checkbox" class="check" id="Check17" data-label="to" checked>
                    <label class="span span-blue1" for="Check17">
                        Spesial Customer
                    </label>
                </div>



            </div>
        </div>

        <div class="flex md-4">
            <div class="w-11/12 bg-red">
                <?php
                include(base_path . '/component/padding.php');
                ?>
            </div>

            <div class="w-1/12 bg-blue items-center flex justify-end">

                <button class="btn btn-sm btn-rounded btn-primary"><i class="fas fa-sync-alt"></i> Reset</button>

            </div>

        </div>

        <div class="w-full mt-1">
            <div class="container">

                <div class='flex headtable'>
                    <div class='w-2/12'>Booking Date.</div>
                    <div class='w-2/12'>Resv.#</div>
                    <div class='w-2/12'>Booking Code</div>
                    <div class='w-2/12'>Airline</div>
                    <div class='w-2/12'>Dep</div>
                    <div class='w-2/12'>Arv</div>
                    <div class='w-2/12'>Std</div>
                    <div class='w-2/12'>Time Limit</div>
                    <div class='w-2/12'>Status</div>
                    <div class='w-2/12'>Pax Count</div>
                    <div class='w-2/12'>Pax Name</div>
                    <div class='w-2/12'>Booked by</div>
                    <div class='w-1/12'></div>
                </div>

                <div class='flex rowtable'>
                    <div class='w-2/12'>
                        <div class=''>31 Mar 2019 14:16</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = '<?= root_path ?>/pages/alreservation/alreservation_detail.php';" class="span span span-bluelink"> 19088398</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>UEJDXD</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>Citilink</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>HLP</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>MLG</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>1 Apr 2019 12:25</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>31 Mar 2019 16:16</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="badge badge-primary">Issued</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>TIKA NUR WIDYANI,Ms</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>LEN
                            <span class="badge badge-primary">HTT</span>
                        </div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <span class="badge badge-danger">B</span>
                        </div>
                    </div>
                </div>

                <div class='flex rowtable'>
                    <div class='w-2/12'>
                        <div class=''>31 Mar 2019 14:16</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>19088398</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>UEJDXD</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>Citilink</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>HLP</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>MLG</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>1 Apr 2019 12:25</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>31 Mar 2019 16:16</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="badge badge-primary">Issued</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>TIKA NUR WIDYANI,Ms</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>LEN
                            <span class="badge badge-primary">HTT</span>
                        </div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <span class="badge badge-danger">B</span>
                        </div>
                    </div>
                </div>

                <div class='flex rowtable'>
                    <div class='w-2/12'>
                        <div class=''>31 Mar 2019 14:16</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>19088398</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>UEJDXD</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>Citilink</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>HLP</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>MLG</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>1 Apr 2019 12:25</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>31 Mar 2019 16:16</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="badge badge-primary">Issued</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>TIKA NUR WIDYANI,Ms</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>LEN
                            <span class="badge badge-primary">HTT</span>
                        </div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <span class="badge badge-danger">B</span>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>



    <?php
    include(base_path . '/component/footer.php');
    ?>
</body>

</html>