<html>
<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');

    ?>
    <br>
    <br>
    <br>
    <!-- isi content -->

    <div class="container w-11/12 bg-gray-200 mx-auto p-5">

        <div class="container w-full">

            <div class="w-full border-0 border-b border-solid border-black">
                <div class="flex md-4">
                    <span class="span-title">
                        <h2>Sign Up Details</h2>
                    </span>
                </div>
            </div>

        </div>

        <div class="container mt-2 flex">

            <div class="w-full flex justify-end ">
                <div class="mr-1">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/users/user.php';" class="btn btn-rounded btn-light"><i class="fas fa-list"></i> List</button>
                </div>
            </div>

        </div>

        <div class="container w-full mt-3 ">

            <div class="flex flex-wrap overflow-hidden bg-gray-100 border-0 border-b border-solid border-black p-2 ">

                <div class="w-1/6 text-left text-xs font-bold flex items-center  ">
                    Company Name
                </div>

                <div class="w-2/12 items-center text-xs flex">
                    Klub Bunga Butik Resort`
                </div>

            </div>

            <div class="flex flex-wrap overflow-hidden bg-gray-100 border-0 border-b border-solid border-black p-2 ">

                <div class="w-1/6 text-left text-xs font-bold flex items-center  ">
                    User ID
                </div>

                <div class="w-1/2 items-center text-xs flex">
                    <input type="text" class="input " style="width:400px" value="ian_klubbunga@yahoo.com">
                </div>

            </div>

            <div class="flex flex-wrap overflow-hidden bg-gray-100 border-0 border-b border-solid border-black p-2 ">

                <div class="w-1/6 text-left text-xs font-bold flex items-center  ">
                    Full Name
                </div>

                <div class="w-1/2 items-center text-xs flex">
                    <input type="text" class="input " style="width:400px" value="Mrs. Adhrieyana">
                </div>

            </div>

            <div class="flex flex-wrap overflow-hidden bg-gray-100 border-0 border-b border-solid border-black p-2 ">

                <div class="w-1/6 text-left text-xs font-bold flex items-center  ">
                    Email
                </div>

                <div class="w-1/2 items-center text-xs flex">
                    <input type="text" class="input " style="width:400px" value="ian_klubbunga@yahoo.com">
                </div>

            </div>

            <div class="flex flex-wrap overflow-hidden bg-gray-100 border-0 border-b border-solid border-black p-2 ">

                <div class="w-1/6 text-left text-xs font-bold flex items-center  ">
                    Address
                </div>

                <div class="w-1/2 items-center text-xs flex">
                    <textarea rows="6" cols="29" class="border border-gray-400 rounded-lg"> Jl. Kartika No. 1</textarea>
                </div>

            </div>

            <div class="flex flex-wrap overflow-hidden bg-gray-100 border-0 border-b border-solid border-black p-2 ">

                <div class="w-1/6 text-left text-xs font-bold flex items-center  ">
                    City
                </div>

                <div class="w-1/2 items-center text-xs flex">
                    <input type="text" class="input " style="width:400px" value="Kota Batu">
                </div>

            </div>

            <div class="flex flex-wrap overflow-hidden bg-gray-100 border-0 border-b border-solid border-black p-2 ">

                <div class="w-1/6 text-left text-xs font-bold flex items-center  ">
                    Phone
                </div>

                <div class="w-1/2  text-xs ">
                    <input type="text" class="input " style="width:400px" value="0341-594777">
                </div>

            </div>

            <div class="flex flex-wrap overflow-hidden bg-gray-100 border-0 border-b border-solid border-black p-2 ">

                <div class="w-1/6 text-left text-xs font-bold flex items-center  ">
                    Fax
                </div>

                <div class="w-1/2  text-xs ">
                    <input type="text" class="input " style="width:400px" value="0341-594770">
                </div>

            </div>

            <div class="flex flex-wrap overflow-hidden bg-gray-100 border-0 border-b border-solid border-black p-2 ">

                <div class="w-1/6 text-left text-xs font-bold flex items-center  ">
                    Instant Messenger
                </div>

                <div class="w-1/2  text-xs ">
                    <input type="text" class="input " style="width:400px" value="ian_klubbunga@yahoo.com">
                </div>

            </div>

            <div class="flex flex-wrap overflow-hidden bg-gray-100 border-0 border-b border-solid border-black p-2 ">

                <div class="w-1/6 text-left text-xs font-bold flex items-center  ">
                    Password
                </div>

                <div class="w-1/2  text-xs ">
                    <input type="text" class="input" value="">
                </div>

            </div>

            <div class="flex flex-wrap overflow-hidden bg-gray-100 border-0 border-b border-solid border-black p-2 ">

                <div class="w-1/6 text-left text-xs font-bold flex items-center  ">
                    Retype Password
                </div>

                <div class="w-1/2  text-xs ">
                    <input type="text" class="input" value="">
                </div>

            </div>

            <div class="flex flex-wrap overflow-hidden bg-gray-100 border-0 border-b border-solid border-black p-2 ">

                <div class="w-1/6 text-left text-xs font-bold flex items-center  ">
                    Status
                </div>

                <div class="w-1/2  text-xs ">
                    <input type="checkbox" class="check" id="dropdownCheck2" data-label="remember me">
                    <label class="span span span-bluelink" for="dropdownCheck2">
                        Active
                    </label>
                </div>

            </div>

            <div class="flex flex-wrap overflow-hidden bg-gray-100 border-0 border-b border-solid border-black p-2 ">

                <div class="w-1/6 text-left text-xs font-bold flex items-center  ">
                    Role
                </div>

                <div class="w-1/2  text-xs ">
                    <div>
                        <form>
                            <select class=" border-solid border-gray-600 border rounded mt-3">
                                <option value="">Checkbox 1</option>
                                <option value="">Checkbox 2</option>
                                <option value="">Checkbox 3</option>
                                <option value="">Checkbox 4</option>
                                <option value="">Checkbox 5</option>
                            </select>
                        </form>
                    </div>
                </div>

            </div>

            <div class="flex flex-wrap overflow-hidden bg-gray-100 border-0 border-b border-solid border-black p-2 ">

                <div class="w-1/6 text-left text-xs font-bold flex items-center flex items-center  ">
                    Company
                </div>

                <div class="w-1/2  text-xs ">
                    <div class="flex">
                        <div class="mr-2">
                            <input type="checkbox" class="check" id="dropdownCheck1" data-label="remember me">
                            <label class="span span span-bluelink" for="dropdownCheck1">
                                Customer
                            </label>
                        </div>

                        <div class="mr-2">
                            <input type="checkbox" class="check" id="dropdownCheck2" data-label="remember me">
                            <label class="span span span-bluelink" for="dropdownCheck2">
                                Hotel
                            </label>
                        </div>

                        <div class="mr-2">
                            <input type="checkbox" class="check" id="dropdownCheck3" data-label="remember me">
                            <label class="span span span-bluelink" for="dropdownCheck3">
                                Branch
                            </label>
                        </div>

                    </div>
                    <div class="mt-2">
                        <input type="text" class="input " style="width:400px" value="">
                    </div>
                    <div class="mt-2">
                        <span class="text-xs">
                            Note: please make sure Company ID for this customer/hotel is already exist. If Company ID does not exist it will be automatically filled.
                        </span>
                    </div>
                </div>

            </div>

            <div class="flex flex-wrap overflow-hidden bg-gray-100 border-0 border-b border-solid border-black p-2 ">

                <div class="w-1/6 text-left text-xs font-bold flex items-center  ">
                    Company ID
                </div>

                <div class="w-1/2  text-xs ">
                    <input type="text" class="input " style="width:400px" value="">
                </div>

            </div>

        </div>

        <div class="container w-full mt-3 flex justify-center">

            <button onclick="window.location.href = 'master_hotelRoomType.php';" class="btn btn-rounded btn-light"><i class="fas fa-save"></i> Save</button>
            <button onclick="window.location.href = 'master_hotelRoomType.php';" class="btn btn-rounded btn-light"><i class="fas fa-undo"></i> Cancel</button>

        </div>

    </div>

    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>