<html>
<?php
include(__DIR__ . '/../head.php');
?>

<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');
    ?>


    <!-- isi content -->

    <div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">

        <div class="w-full border-0 border-b border-solid border-black">
            <div class="flex md-4">
                <span class="span-title">
                    <h2>Sign Up List</h2>
                </span>
            </div>
        </div>

        <div class="w-full  justify-center items-center flex mt-1 ">
            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-save"></i> Save Excel</button>
            <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-sync-alt"></i> Reset</button>
        </div>

        <div class="flex md-4">
            <div class="w-1/3 bg-red">
                <?php
                include(base_path . '/component/padding.php');
                ?>
            </div>

            <div class="w-1/2 items-center flex">

            </div>

            <div class="w-2/12 bg-blue items-center flex mt-1">

            </div>

        </div>


        <div class="w-full mt-1 bg-gray-200">
            <div class="container">

                <div class='flex headtable'>
                    <div class='w-1/12'>No.</div>
                    <div class='w-2/12'>Company</div>
                    <div class='w-2/12'>Address</div>
                    <div class='w-2/12'>City</div>
                    <div class='w-2/12'>Contact</div>
                    <div class='w-2/12'>User Type</div>
                    <div class='w-2/12'>Status</div>
                    <div class='w-2/12'>Date</div>
                </div>

                <div class='flex rowtable'>
                    <div class='w-1/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = '<?= root_path ?>/pages/signup/signup_detail.php';" class="span span span-bluelink">Klub Bunga Butik Resort</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>Jl. Kartika No.1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>Kota Batu</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>Mrs. Adhrieyana</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="badge badge-success">Hotel</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="badge badge-success">New</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>29 Mar 2019 10:12</div>
                    </div>
                </div>






            </div>
        </div>

    </div>



    <?php
    include(base_path . '/component/footer.php');
    ?>
</body>

</html>