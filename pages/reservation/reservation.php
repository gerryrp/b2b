<html>
<?php 
    include(__DIR__.'/../head.php');
?>


<body>

<?php 
include(base_path.'/component/topnav.php');
include(base_path.'/component/slider.php');
?>


<!-- isi content -->

<div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">
    
    <div class="container bg-gray-300">
        <div class=" flex md-4 ">

            <div id="myDiv" class="w-11/12  flex md-4">
                <div class="mr-1">
                    <button class="btn btn-light activebtn">New</button>
                </div>
                <div class="mr-1">
                    <button class="btn btn-light"> On Progress</button>
                </div>
                <div class="mr-1">
                    <button class="btn btn-light">Confirmed</button>    
                </div>
                <div class="mr-1">
                    <button class="btn btn-light"> Not Confirmed</button>
                </div>
                <div class="mr-1">
                    <button class="btn btn-light">Time Limit</button>
                </div>
                <div class="mr-1">
                    <button class="btn btn-light">Canceled by OP</button>    
                </div>
                <div class="mr-1">
                    <button class="btn btn-light">Canceled by User</button>
                </div>
                <div class="mr-1">
                    <button class="btn btn-light">Printed</button>
                </div>
                <div class="mr-1">
                    <button class="btn btn-light">Issued</button>    
                </div>
                <div class="mr-1">
                    <button class="btn btn-light">Void</button>    
                </div>
            </div>

            <div class="w-1/12  items-center flex ">
                <div class="my-2 px-1  ">
                        <input type="checkbox" class="check" id="Check1"  data-label="to">
                        <label class="span span-blue1" for="Check1">
                            All Record
                        </label>          
                </div>
            </div>
            
        </div>
    </div>

    <div class="w-full">
        <div class="flex md-4">
            

                <div class="my-2 px-1">
                        <input type="checkbox" class="check" id="Check11"  data-label="to">
                        <label class="span span-blue1" for="Check11">
                            Only Me
                        </label>          
                </div>

                <div class="my-2 px-1">
                        <input type="checkbox" class="check" id="Check12"  data-label="to">
                        <label class="span span-blue1" for="Check12">
                            Hotel Conf.
                        </label>                              
                </div>

                <div class="my-2 px-1">
                        <input type="checkbox" class="check" id="Check13"  data-label="to" checked>
                        <label class="span span-blue1" for="Check13">
                            Branch
                        </label>                              
                </div>

                <div class="my-2 px-1">
                        <input type="checkbox" class="check" id="Check14"  data-label="to" checked>
                        <label class="span span-blue1" for="Check14">
                            Agent
                        </label>                              
                </div>

                <div class="my-2 px-1">
                        <input type="checkbox" class="check" id="Check15"  data-label="to" checked>
                        <label class="span span-blue1" for="Check15">
                            Corporate
                        </label>                              
                </div>

                <div class="my-2 px-1">
                        <input type="checkbox" class="check" id="Check16"  data-label="to" checked>
                        <label class="span span-blue1" for="Check16">
                            FIT
                        </label>                              
                </div>

                <div class="my-2 px-1">
                        <input type="checkbox" class="check" id="Check17"  data-label="to" checked>
                        <label class="span span-blue1" for="Check17">
                            Spesial Customer
                        </label>                              
                </div>

           

        </div>
    </div>

    <div class="flex md-4">
        <div class="w-11/12 bg-red">
            <?php 
            include(base_path.'/component/padding.php');
            ?>
        </div>
        
        <div class="w-1/12 bg-blue  flex justify-end items-center">
           
            <button class="btn btn-sm btn-rounded btn-primary"><i class="fas fa-sync-alt"></i> Reset</button>

        </div>

    </div>


    <div class="w-full mt-1 bg-gray-200 p-2">
        <div class="container">

            <div class='flex headtable'>
                <div class='w-2/12'>Booking Date.</div>
                <div class='w-2/12'>Resv.#</div>
                <div class='w-2/12'>Hotel & City</div>
                <div class='w-2/12'>Cut-off Date </div>
                <div class='w-2/12'>Guest Name</div>
                <div class='w-2/12'>Check-in</div>
                <div class='w-2/12'>Status</div>
                <div class='w-2/12'>Booked by</div>
                <div class='w-1/12'></div>
            </div>

            <div class='flex rowtable'>
                <div class='w-2/12'>
                    31 Mar 2019 19:36
                </div>
                <div class='w-2/12'>
                    <span onclick="window.location.href = '<?=root_path?>/pages/reservation/reservation_detail.php';" class="span span span-bluelink">19014960</span> 
                </div>
                <div class='w-2/12'>
                    POP! HOTEL AIRPORT JAKARTA, TANGERANG
                </div>
                <div class='w-2/12'>
                    -
                </div>
                <div class='w-2/12'>
                    LUKAS DJUNANTO
                </div>
                <div class='w-2/12'>
                    2 Apr 2019
                </div>
                <div class='w-2/12'>
                    <span class="badge badge-primary">Issued</span>
                </div>
                <div class='w-2/12'>
                    SAKURA MITRA WISATA (DP)
                </div>
                <div class='w-1/12'>
                    <div class="badge  badge-primary tooltip tooltipbot">
                        A<span class="textbot">A sub agent</span>
                    </div>
                </div>
            </div>  

            <div class='flex rowtable '>
                <div class='w-2/12'>
                    31 Mar 2019 12:47
                </div>
                <div class='w-2/12'>
                    19014959
                </div>
                <div class='w-2/12'>
                    GOLDEN TULIP HOLLAND RESORT BATU, BATU
                </div>
                <div class='w-2/12'>
                    18 Apr 2019
                </div>
                <div class='w-2/12'>
                    ROBBY LINDARTANTO
                </div>
                <div class='w-2/12'>
                    21 Apr 2019
                </div>
                <div class='w-2/12'>
                    <span class="badge badge-primary">Issued</span> 
                </div>
                <div class='w-2/12'>
                    <div class=''>ULI
                        <span class="badge badge-primary">HTT</span> 
                    </div>
                </div>
                <div class='w-1/12'>
                    <div class=''>
                        <div class="badge badge-danger tooltip tooltipbot">B
                            <span class="textbot">Branch</span>
                        </div> 
                    </div>
                </div>
            </div>

            <div class='flex rowtable '>
                <div class='w-2/12'>
                    31 Mar 2019 11:41
                </div>
                <div class='w-2/12'>
                    19014958
                </div>
                <div class='w-2/12'>
                    NOVOTEL BOGOR, BOGOR
                </div>
                <div class='w-2/12'>
                    14 Apr 2019 12:00
                </div>
                <div class='w-2/12'>
                    CHRIS VAN ARR
                </div>
                <div class='w-2/12'>
                    21 Apr 2019
                </div>
                <div class='w-2/12'>
                    <span class="badge badge-sm badge-success">On Progress</span> 
                </div>
                <div class='w-2/12'>
                    <div class=''>MAY
                        <span class="badge badge-primary">KBR</span> 
                    </div>
                </div>
                <div class='w-1/12'>
                    <div class=''> 
                        <div class="badge badge-danger tooltip tooltipbot">
                                B  <span class="textbot">Branch</span> 
                        </div>
                    </div>
                </div>
            </div>
        
        </div>
    </div>


</div>



<?php 
include(base_path.'/component/footer.php');
?>

<script>
var header = document.getElementById("myDiv");
var btns = header.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("activebtn");
  current[0].className = current[0].className.replace(" activebtn", "");
  this.className += " activebtn";
  });
}
</script>
</body>
</html>