<html>

<?php
include(__DIR__ . "/../head.php");
?>



<body>

    <?php
    include(base_path . "/component/topnav.php");
    include(base_path . "/component/slider.php");

    ?>

    <!-- isi content adasd -->

    <div class="container bg-gray-200 mx-auto w-11/12 mt-10 p-5">
        <div class="container max-w-full">
            <div class="w-full boxrow">
                <div class="flex md-4">
                    <span class="span-title">
                        <h2>Price List</h2>
                    </span>
                </div>
            </div>
        </div>

        <div class="container max-w-full mt-2 flex items-center ">

            <div class="w-4/12 flex">
                <div class="items-center flex">
                    <span>City</span>
                </div>

                <div class="ml-20 mt-3">
                    <div>
                        <form>
                            <select class="inputselect rounded ">
                                <option value="SURABAYA"> SURABAYA</option>
                                <option value="JAKARTA"> JAKARTA</option>
                                <option value="YOGYAKARTA"> YOGYAKARTA</option>
                                <option value="BANDUNG"> BANDUNG</option>
                                <option value="LABUAN BAJO - MUTIARA AIRPORT"> LABUAN BAJO - MUTIARA AIRPORT</option>
                            </select>
                        </form>
                    </div>
                </div>
            </div>

            <button class="btn btn-sm btn-rounded btn-light"> <i class="fas fa-file-pdf"></i> PDF</button>

            <button class="btn btn-sm btn-rounded btn-light"> <i class="fas fa-file-excel"></i> Excel</button>

        </div>

        <div class="container max-w-full mt-5">
            <div class="w-full boxrow">
                <div class="flex justify-center">
                    <span class="font-bold text-xl italic">
                        <h2>SURABAYA</h2>
                    </span>
                </div>
            </div>
        </div>

        <div class="container  max-w-full relative ">
            <div class=" w-full bg-gray-400   ">
                <div class="flex headtable">
                    <div class="w-2/12">Hotel Address</div>
                    <div class="w-2/12">Rate Remarks</div>
                    <div class="w-8/12 flex">
                        <div class="w-2/12">Type</div>
                        <div class="w-2/12">Facility</div>
                        <div class="w-1/12">Price</div>
                        <div class="w-2/12">Type</div>
                        <div class="w-2/12">Facility</div>
                        <div class="w-1/12">Price</div>
                    </div>
                </div>

                <div class="flex rowtable">

                    <div class="w-2/12">
                        <div class="table_cell ">

                            <div class="flex justify-center">
                                <span class="font-bold">AMARIS EMBONG MALANG SURABAYA [*2]</span>
                            </div>

                            <div class="flex justify-center">
                                <span>Jl. Kedungdoro No 1-3 Surabaya, SURABAYA</span>
                            </div>

                            <div class="flex justify-center">
                                <span>P: 031 548 2299 </span>
                            </div>

                            <div class="flex justify-center">
                                <span>F: 031 549 2299</span>
                            </div>

                            <div class="flex justify-center">
                                <span>engineer@haryono.co.id</span>
                            </div>

                            <div class="flex justify-center mt-2">
                                <span>CANCELLATION :</span>
                            </div>

                            <div class="flex justify-center">
                                <span>LOW SEASON 5 DAYS PRIOR TO ARIVAL</span>
                            </div>

                            <div class="flex justify-center">
                                <span>HIGH SEASON 16 DAYS PRIOR TO ARIVAL</span>
                            </div>

                        </div>
                    </div>

                    <div class="w-2/12">
                        <div class="rowtable pb-5">
                            <span>EFF : 1 APR 19 - 31 MAR 20 [BF] [ALL MARKET] "CONTRACT RATE 2019 - 2020" "EXCEPT HIGH/PEAK SEASON"</span>
                        </div>

                        <div class="">
                            <span>EFF : 31 DEC 19 [BF] [ALL MARKET] "NEW YEAR EVE"</span>
                            <span class="span span-danger">NO CANCEL, NO REFUND AND NO AMEND</span>
                        </div>
                    </div>

                    <div class="w-8/12">
                        <div class="rowtable flex">
                            <div class="w-2/12">
                                SMART ROOM
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-1/12">
                                aaaaa
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-1/12">
                                aaaaa
                            </div>
                        </div>

                        <div class="rowtable flex">
                            <div class="w-2/12">
                                SMART SUPERIOR KINGSIZE
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-1/12">
                                aaaaa
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-1/12">
                                aaaaa
                            </div>
                        </div>

                        <div class="rowtable flex">
                            <div class="w-2/12">
                                SMART HOLLYWOOD
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-1/12">
                                aaaaa
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-1/12">
                                aaaaa
                            </div>
                        </div>

                        <div class="rowtable flex">
                            <div class="w-2/12">
                                SMART ROOM
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-1/12">
                                aaaaa
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-1/12">
                                aaaaa
                            </div>
                        </div>

                        <div class="rowtable flex">
                            <div class="w-2/12">
                                SMART SUPERIOR KINGSIZE
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-1/12">
                                aaaaa
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-1/12">
                                aaaaa
                            </div>
                        </div>

                        <div class="rowtable flex">
                            <div class="w-2/12">
                                SMART HOLLYWOOD
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-1/12">
                                aaaaa
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-2/12">
                                aaaaa
                            </div>
                            <div class="w-1/12">
                                aaaaa
                            </div>
                        </div>
                    </div>
                </div>




            </div>

        </div>
    </div>
    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>