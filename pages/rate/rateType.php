<html>

<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');

    ?>
    <br>
    <br>
    <br>
    <!-- isi content -->

    <div class="container w-11/12 mx-auto">

        <div class="container max-w-full">

            <div class="w-full border-0 border-b border-solid border-black">
                <div class="flex md-4">
                    <span class="span-title">
                        <h2>Rate Type List</h2>
                    </span>
                </div>
            </div>

        </div>

        <div class="container">
            <div class="flex md-4">

                <div class="container w-4/12">
                    <?php
                    include(base_path . '/component/padding.php');
                    ?>
                </div>

                <div class="container w-4/12 flex items-center justify-center">

                    <div class="text-small ">
                        Search : &nbsp
                    </div>

                    <div class="flex md-4">
                        <input type="text" class="input" placeholder="Hotel">
                        <button type="submit" class="fa fa-search ml-1" style="background-color: rgba(255, 255, 255, 0);border-color: rgba(255, 255, 255, 0)"></button>
                    </div>

                </div>

                <div class="flex w-4/12 items-center justify-end">
                    <button type="button" class="btn btn-sm btn-primary btn-rounded mr-3  " value="Reset"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>

            </div>
        </div>

        <div class="container max-w-full bg-white">
            <div class="flex md-4">

                <div class="container w-full">
                    <div class="flex ">

                        <div class="bg-gray-400 w-full ">
                            <div class="container">
                                <div class='headtable flex'>
                                    <div class='w-2/12'>No.</div>
                                    <div class='w-2/12'>Rate Code</div>
                                    <div class='w-2/12'>Rate Name</div>
                                </div>

                                <div class='flex rowtable'>
                                    <div class='w-2/12'>
                                        <div class='table_cell'>1</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class='table_cell'>
                                            <span onclick="window.location.href = '<?= root_path ?>/pages/rate/rateType_detail.php';" class="span span span-bluelink"> KTR5</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class='table_cell'>Contract Rate</div>
                                    </div>
                                </div>

                                <div class='flex rowtable'>
                                    <div class='w-2/12'>
                                        <div class='table_cell'>2</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class='table_cell'>
                                            <span class="span span span-bluelink"> KTR1</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class='table_cell'>Contract Rate</div>
                                    </div>
                                </div>

                                <div class='flex rowtable'>
                                    <div class='w-2/12'>
                                        <div class='table_cell'>3</div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class='table_cell'>
                                            <span class="span span span-bluelink"> KTR2</span>
                                        </div>
                                    </div>
                                    <div class='w-2/12'>
                                        <div class='table_cell'>Contract Rate</div>
                                    </div>
                                </div>


                            </div>
                        </div>


                    </div>
                </div>

            </div>
        </div>

    </div>

    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>