<html>
<?php
include(__DIR__ . '/../head.php');
?>

<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');
    ?>


    <!-- isi content -->

    <div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">

        <div class="w-full boxrow">
            <div class="flex md-4">
                <span class="span-title">
                    <h2>Master API User</h2>
                </span>
            </div>
        </div>

        <div class="flex md-4 mt-5">
            <div class="w-4/12 bg-red">

            </div>

            <div class="w-4/12 items-center justify-center flex">
                <div>
                    Search :
                    <input type="text" class="input">
                    <button class="btn btn-sm btn-rounded btn-light">Search</button>
                </div>
            </div>

            <div class="w-4/12 bg-blue items-center justify-end flex">
                <div class="mr-1">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/apiusers/apiuser_add.php';" class="btn btn-sm btn-rounded btn-success"><i class="fas fa-plus"></i> Add</button>
                    <button class="btn btn-sm btn-rounded btn-primary"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>
            </div>

        </div>

        <div class="w-full p-1  flex boxrow boxrow-gray mt-1">
            <span>Active Status : </span>
            <div class="w-1/2 flex text-sm">
                <div class=" items-center flex mx-2">
                    <input id="option1" type="radio" name="field" value="">
                    <label for="option1"><span><span></span></span>All
                    </label>
                </div>

                <div class=" items-center flex mx-2">
                    <input id="option2" type="radio" name="field" value="">
                    <label for="option2"><span><span></span></span>Active
                    </label>
                </div>

                <div class=" items-center flex mx-2">
                    <input id="option3" type="radio" name="field" value="">
                    <label for="option3"><span><span></span></span>Inactive
                    </label>
                </div>
            </div>

        </div>


        <div class="w-full mt-1 bg-gray-200">
            <div class="container">

                <div class='flex headtable'>
                    <div class='w-1/12'>No.</div>
                    <div class='w-2/12'>User ID</div>
                    <div class='w-2/12'>Name</div>
                    <div class='w-2/12'>Email</div>
                    <div class='w-2/12'>Company Name</div>
                    <div class='w-2/12'>Company ID</div>
                    <div class='w-2/12'>Role</div>
                    <div class='w-1/12'></div>
                </div>

                <div class='rowtable flex'>
                    <div class='w-1/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = '<?= root_path ?>/pages/apiusers/apiuser_detail.php';" class="span span span-bluelink">backoffice</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>Haryono Backoffice</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>milhanol068@gmail.com</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>NUMBER ONE</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>numberone</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>API User</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-pencil-alt"></i></button>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                </div>

                <div class='rowtable flex'>
                    <div class='w-1/12'>
                        <div class=''>2</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = 'user_detail.php';" class="span span span-bluelink">numberoneapi<span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>Number One</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>milhanol068z@gmail.com</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>NUMBER ONE</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>numberone</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>API User</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-pencil-alt"></i></button>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                </div>






            </div>
        </div>


    </div>



    <?php
    include(base_path . '/component/footer.php');
    ?>
</body>

</html>