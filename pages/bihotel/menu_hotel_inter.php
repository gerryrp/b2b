<html>

<?php
include(__DIR__ . '/../head.php');
?>

<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');
    ?>

    <div class="container w-11/12 mx-auto ">
        <?php include(base_path . '/component/vertical_tabs.php'); ?>
    </div>

    <div class="container w-11/12 mx-auto bg-white mt-5">
        <div class="flex">
            <div class="w-3/12">
                <!-- <div class="w-11/12 mx-auto border border-solid border-gray-500 rounded-t-lg">
                    <div class="bg-blue-700 rounded-t-lg font-bold p-1">
                        <span class="ml-2 text-white">Your Hotel Search</span>
                    </div>
                    <div class="mx-auto my-1">
                        <i class="fas fa-map-marker-alt"></i>
                        <input type="text" class="input w-48 mx-1" value="Surabaya, Indonesia">
                    </div>
                    <div class="mx-auto my-1">
                        <i class="fas fa-moon"></i>
                        <input type="text" class="input w-48 mx-1" value="2 night">
                    </div>
                    <div class="mx-auto my-1">
                        <i class="fas fa-bed"></i>
                        <input type="text" class="input w-48 mx-1" value="1 room">
                        <button><i class="fas fa-plus-square"></i></button>
                    </div>
                    <div class="mx-auto my-1">
                        <i class="fas fa-user-friends"></i>
                        <input type="text" class="input w-48 mx-1" value="1 adult">
                    </div>
                    <div class="mx-auto my-1">
                        <i class="fas fa-star"></i>
                        <input type="text" class="input w-48 mx-1" value="show all">
                    </div>
                    <div class="mx-auto my-1">
                        <button class="btn btn-rounded btn-primary">Modify Search</button>
                    </div>
                </div> -->

                <div class="w-11/12 mx-auto border border-solid border-gray-500 mt-3 rounded-t-lg">
                    <div class="bg-blue-700 rounded-t-lg p-1 flex">
                        <span class="ml-2 text-white font-bold w-6/12">Filter Search</span>
                        <div class="w-6/12 justify-end flex">
                            <button>
                                <span class="text-white text-xs">Clear All Filters</span>
                            </button>
                        </div>
                    </div>
                    <div class="w-full mb-1">
                        <div class="w-full flex bg-blue-300 mb-1">
                            <div class="w-full font-bold">Hotel Name</div>
                            <div class="w-full flex justify-end text-xs items-center span span-bluelink">Clear</div>
                        </div>
                        <input type="text" class="input w-48 mx-1" value="">
                        <button>
                            <i class="fas fa-search ml-1"></i>
                        </button>
                    </div>
                    <div class="w-full my-1">
                        <div class="w-full flex bg-blue-300 mb-1">
                            <div class="w-full font-bold">Budget</div>
                            <div class="w-full flex justify-end text-xs items-center span span-bluelink">Clear</div>
                        </div>
                        <input type="text" class="input w-48 mx-1" value="">
                    </div>
                    <div class="w-full my-1">
                        <div class="w-full flex bg-blue-300 mb-1">
                            <div class="w-full font-bold">Price</div>
                            <div class="w-full flex justify-end text-xs items-center span span-bluelink">Clear</div>
                        </div>
                        <div>
                            <input type="text" class="input w-32 mx-1" value="IDR 274,000"> -
                            <input type="text" class="input w-32 mx-1" value="IDR 8,590,780">
                        </div>
                    </div>
                    <div class="w-full my-1">
                        <div class="w-full flex bg-blue-300 mb-1">
                            <div class="w-full font-bold">Star Rating</div>
                            <div class="w-full flex justify-end text-xs items-center span span-bluelink">Clear</div>
                        </div>
                        <div class="w-full">
                            <div class="text-sm">
                                <input type="checkbox" class="check" id="star" data-label="to">
                                <label for="star">
                                    Unrated
                                </label>
                            </div>
                            <div class="span span-warning">
                                <input type="checkbox" class="check" id="star2" data-label="to">
                                <label for="star2">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </label>
                            </div>
                            <div class="span span-warning">
                                <input type="checkbox" class="check" id="star3" data-label="to">
                                <label for="star3">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </label>
                            </div>
                            <div class="span span-warning">
                                <input type="checkbox" class="check" id="star4" data-label="to">
                                <label for="star4">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </label>
                            </div>
                            <div class="span span-warning">
                                <input type="checkbox" class="check" id="star5" data-label="to">
                                <label for="star5">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="w-full my-1">
                        <div class="w-full flex bg-blue-300 mb-1">
                            <div class="w-full font-bold">TripAdvisor Rating </div>
                            <div class="font-bold text-base items-center flex"> <i class="fab fa-tripadvisor"></i></div>
                            <div class="w-full flex justify-end text-xs items-center span span-bluelink">Clear</div>
                        </div>
                        <div class="w-full">
                            <div class="text-sm">
                                <input type="checkbox" class="check" id="rating1" data-label="to">
                                <label for="rating1">
                                    Unrated
                                </label>
                            </div>
                            <div class="span span-success">
                                <input type="checkbox" class="check" id="rating2" data-label="to">
                                <label for="rating2">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star-half-alt"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                </label>
                            </div>
                            <div class="span span-success">
                                <input type="checkbox" class="check" id="rating3" data-label="to">
                                <label for="rating3">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="far fa-star"></i>
                                    <i class="far fa-star"></i>
                                </label>
                            </div>
                            <div class="span span-success">
                                <input type="checkbox" class="check" id="rating4" data-label="to">
                                <label for="rating4">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star-half-alt"></i>
                                    <i class="far fa-star"></i>
                                </label>
                            </div>
                            <div class="span span-success">
                                <input type="checkbox" class="check" id="rating5" data-label="to">
                                <label for="rating5">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="far fa-star"></i>
                                </label>
                            </div>
                            <div class="span span-success">
                                <input type="checkbox" class="check" id="rating6" data-label="to">
                                <label for="rating6">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star-half-alt"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="w-full my-1">
                        <div class="w-full flex bg-blue-300 mb-1">
                            <div class="w-full font-bold">Property Type</div>
                            <div class="w-full flex justify-end text-xs items-center span span-bluelink">Clear</div>
                        </div>
                        <div class="w-full">
                            <input type="checkbox" class="check" id="prototype" data-label="to">
                            <label for="prototype">
                                Hotel
                            </label>
                        </div>
                    </div>
                    <div class="w-full my-1">
                        <div class="w-full flex bg-blue-300 mb-1">
                            <div class="w-full font-bold">Location</div>
                            <div class="w-full flex justify-end text-xs items-center span span-bluelink">Clear</div>
                        </div>
                        <div class="w-full">
                            <div>
                                <input type="checkbox" class="check" id="loc1" data-label="to">
                                <label for="loc1">
                                    Near ...
                                </label>
                            </div>
                            <div>
                                <input type="checkbox" class="check" id="loc2" data-label="to">
                                <label for="loc2">
                                    Near ...
                                </label>
                            </div>
                            <div>
                                <input type="checkbox" class="check" id="loc3" data-label="to">
                                <label for="loc3">
                                    Near ...
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="w-9/12">
                <div class="flex w-full border border-solid border-gray-300 items-center">
                    <span class="w-1/12 border-0 border-r border-solid border-gray-300 font-bold">Sort by: </span>
                    <span class="w-1/12 border-0 border-r border-solid border-gray-300"><i class="fas fa-dollar-sign span span-warning"></i> Price</span>
                    <span class="w-1/12 border-0 border-r border-solid border-gray-300"><i class="fas fa-star span span-warning"></i> Star</span>
                    <span class="w-2/12 border-0 border-r border-solid border-gray-300"><i class="fas fa-percent span span-warning"></i> Offers/Promotion</span>
                    <span class="w-3/12 border-0 border-r border-solid border-gray-300 span span-primary text-base"> Showing 59 Result</span>
                    <div class="w-4/12 flex justify-end">
                        <button class="btn btn-rounded btn-primary"><i class="fas fa-sync-alt"></i> Reset</button>
                    </div>
                </div>

                <div class="flex w-full justify-end items-center border border-solid border-gray-300 my-1 ">
                    <div class="w-1/12 mr-2">
                        <span>Padding</span>
                    </div>
                    <div class="flex mt-3">
                        <form>
                            <select class="inputselect w-16">
                                <option value="">0</option>
                                <option value="">20</option>
                                <option value="">40</option>
                                <option value="">60</option>
                                <option value="">80</option>
                                <option value="">100</option>
                            </select>
                        </form>
                    </div>
                </div>

                <div class="w-full">

                    <div class="widefatbox mx-auto shadow-lg flex">

                        <div class="w-8/12">
                            <div class="w-full flex items-center">
                                <div class="w-6/12">
                                    <span class="font-bold text-lg span span-primary">
                                        ISTANA PERMATA NGAGEL
                                    </span>
                                </div>
                                <div class="w-1/12 flex">
                                    <span class="span span-warning text-lg"> <i class="fas fa-map-marker-alt"></i></span>
                                </div>
                                <div class="w-2/12 flex justify-start">
                                    <span class="span span-warning text-lg"> <i class="fas fa-star"></i></span>
                                    <span class="span span-warning text-lg"> <i class="fas fa-star"></i></span>
                                    <span class="span span-warning text-lg"> <i class="fas fa-star"></i></span>
                                </div>
                            </div>
                            <div class="w-11/12 mt-4">
                                <span class="text-base font-bold">Property Location</span><br>
                                <span class="text-xs">
                                    With a stay at Hotel Istana Permata Ngagel in Surabaya, you'll be convenient to Mpu Tantular
                                    Museum and Galaxy Mall. This hotel is within close proximity of Submarine Monument and Surabaya ...
                                    <button><i class="fas fa-angle-double-right span span-primary"></i></button>
                                </span>
                            </div>
                        </div>

                        <div class="w-4/12 bg-blue-200">
                            <div class="w-full flex justify-end mt-1">
                                <span class="badge badge-success text-xl"><i class="fab fa-tripadvisor text-xl"></i> 2.5/5</span>
                            </div>
                            <div class="w-full mt-4 ">
                                <div class="flex justify-end">
                                    <div class="span span-danger font-bold text-base">IDR 274,400</div><br>
                                </div>

                                <div class="flex justify-end">
                                    ($ 20,58)<i class="fas fa-coins"></i>
                                </div>

                                <div class="flex justify-end items-center">
                                    <span class="span span-navy text-xs">Min. Total Price (Incl. Taxes)</span>
                                </div>

                                <div class="flex justify-end items-center">
                                    <button onclick="window.location.href = 'select_hotel.php';" type="button" class="btn btn-rounded btn-primary text-base mt-2" style="float:right">select hotel</button>
                                </div>

                                <div class="flex justify-end items-center">
                                    <input type="checkbox" class="check" id="email1" data-label="to">
                                    <label for="email1">
                                        Email
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="widefatbox mx-auto shadow-lg flex">

                        <div class="w-8/12">
                            <div class="w-full flex items-center">
                                <div class="w-6/12">
                                    <span class="font-bold text-lg span span-primary">
                                        Grand Darmo
                                    </span>
                                </div>
                                <div class="w-1/12 flex">
                                    <span class="span span-warning text-lg"> <i class="fas fa-map-marker-alt"></i></span>
                                </div>
                                <div class="w-2/12 flex justify-start">
                                    <span class="span span-warning text-lg"> <i class="fas fa-star"></i></span>
                                    <span class="span span-warning text-lg"> <i class="fas fa-star"></i></span>
                                    <span class="span span-warning text-lg"> <i class="fas fa-star"></i></span>
                                    <span class="span span-warning text-lg"> <i class="fas fa-star"></i></span>
                                </div>
                            </div>
                            <div class="w-11/12 mt-4">
                                <span class="text-base font-bold">Property Location</span><br>
                                <span class="text-xs">
                                    Stay at Grand Darmo Suite by AMITHYA places you in the heart of Surabaya, wihtin a 15-minute
                                    walk of Mpu Tantular Museum and Surabaya Zoo. This 4-star hotel is 1.2
                                    <button><i class="fas fa-angle-double-right span span-primary"></i></button>
                                </span>
                            </div>
                        </div>

                        <div class="w-4/12 bg-blue-200">
                            <div class="w-full flex justify-end mt-1">
                                <span class="badge badge-success text-xl"><i class="fab fa-tripadvisor text-xl"></i> 4.5/5</span>
                            </div>
                            <div class="w-full mt-4 ">
                                <div class="flex justify-end">
                                    <div class="span span-danger font-bold text-base">IDR 790,440</div><br>
                                </div>

                                <div class="flex justify-end">
                                    ($ 59,28)<i class="fas fa-coins"></i>
                                </div>

                                <div class="flex justify-end items-center">
                                    <span class="span span-navy text-xs">Min. Total Price (Incl. Taxes)</span>
                                </div>

                                <div class="flex justify-end items-center">
                                    <button onclick="window.location.href = 'select_hotel.php';" type="button" class="btn btn-rounded btn-primary text-base mt-2" style="float:right">select hotel</button>
                                </div>

                                <div class="flex justify-end items-center">
                                    <input type="checkbox" class="check" id="email1" data-label="to">
                                    <label for="email1">
                                        Email
                                    </label>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>
    <?php
    include(base_path . '/component/footer.php');
    ?>



</body>

</html>