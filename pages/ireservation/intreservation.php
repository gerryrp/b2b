<html>
<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');
    ?>


    <!-- isi content -->

    <div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">

        <div class="w-full">
            <div class="flex md-4">


                <div class="my-2 px-1">
                    <input type="checkbox" class="check" id="Check11" data-label="to">
                    <label class="span span-blue1" for="Check11">
                        Only Me
                    </label>
                </div>

                <div class="my-2 px-1">
                    <input type="checkbox" class="check" id="Check13" data-label="to" checked>
                    <label class="span span-blue1" for="Check13">
                        Branch
                    </label>
                </div>

                <div class="my-2 px-1">
                    <input type="checkbox" class="check" id="Check14" data-label="to" checked>
                    <label class="span span-blue1" for="Check14">
                        Agent
                    </label>
                </div>

                <div class="my-2 px-1">
                    <input type="checkbox" class="check" id="Check15" data-label="to" checked>
                    <label class="span span-blue1" for="Check15">
                        Corporate
                    </label>
                </div>

                <div class="my-2 px-1">
                    <input type="checkbox" class="check" id="Check16" data-label="to" checked>
                    <label class="span span-blue1" for="Check16">
                        FIT
                    </label>
                </div>

                <div class="my-2 px-1">
                    <input type="checkbox" class="check" id="Check17" data-label="to" checked>
                    <label class="span span-blue1" for="Check17">
                        Spesial Customer
                    </label>
                </div>



            </div>
        </div>

        <div class="flex md-4">
            <div class="w-11/12 bg-red">
                <?php
                include(base_path . '/component/padding.php');
                ?>
            </div>

            <div class="w-1/12 bg-blue items-center flex justify-end items-center">

                <button class="btn btn-sm btn-rounded btn-primary"><i class="fas fa-sync-alt"></i> Reset</button>

            </div>

        </div>


        <div class="w-full mt-1 bg-gray-200">
            <div class="container">

                <div class=' flex headtable'>
                    <div class='w-2/12'>Booking Date.</div>
                    <div class='w-2/12'>Resv.#</div>
                    <div class='w-2/12'>Hotel</div>
                    <div class='w-2/12'>Country</div>
                    <div class='w-2/12'>Check-in</div>
                    <div class='w-2/12'>Check-out</div>
                    <div class='w-2/12'>Time Limit</div>
                    <div class='w-2/12'>Guest Name</div>
                    <div class='w-2/12'>Status</div>
                    <div class='w-2/12'>Booked by</div>
                    <div class='w-2/12'></div>
                </div>

                <div class='flex rowtable '>
                    <div class='w-2/12'>
                        <div class=''>14 Mar 2019 10:35</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = '<?= root_path ?>/pages/ireservation/intreservation_detail.php';" class="span span span-bluelink">19000004</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>Courtyard by Marriot Taipei</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>TW</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>17 Mar 2019</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>18 Mar 2019</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>14 Mar 2019 04:35</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>LIAU AH YANG</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="badge badge-primary">Issued</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>NIK
                            <span class="badge badge-primary">PDN</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="badge badge-danger">B</span>
                        </div>
                    </div>
                </div>

                <div class='flex rowtable'>
                    <div class='w-2/12'>
                        <div class=''>19 Feb 2019 10:35</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>19000003</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>wanda Vista Dongguan</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>CN</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>25 Feb 2019</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>27 Feb 2019</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>19 Feb 2019 08:46</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>ANG MENG FATT STANLEY</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="badge badge-primary">Issued</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>NIK
                            <span class="badge badge-primary">PDN</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="badge badge-danger">B</span>
                        </div>
                    </div>
                </div>

                <div class='flex rowtable '>
                    <div class='w-2/12'>
                        <div class=''>18 Feb 2019 12:02</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>19000002 </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''></div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>Novotel Suzhou SIP</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>CN</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>20 Feb 2019</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>18 Feb 2019 06:02</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>ANG MENG FATT STANLEY</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="badge badge-primary">Issued</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>NIK
                            <span class="badge badge-primary">PDN</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="badge badge-danger">B</span>
                        </div>
                    </div>
                </div>


            </div>
        </div>


    </div>



    <?php
    include(base_path . '/component/footer.php');
    ?>
</body>

</html>