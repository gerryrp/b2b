<html>
<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');
    ?>

    <!-- isi content -->

    <div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">
        <div class="w-full border-0 border-b border-solid border-black">
            <div class="flex md-4">
                <span class="span-title">
                    <h2>Booking Summarty</h2>
                </span>
            </div>
        </div>

        <div class="flex md-4 mt-8">

            <div class="w-full ">
                <span class="span-title">
                    <h2>ZEST HOTEL JEMURSARI X</h2><span>
            </div>

        </div>

        <div class="flex md-4 mt-4">

            <div class="w-full text-xs ">
                <h2 class="font-bold">JL.RAYA PRAPEN 266, SURABAYA</h2><br>
                <p>P: 031 - 9900 3888 <br>
                    F: 031 - 9901 3777 <br>
                    E: engineer@haryono.co.id<br></p>
                <a href="https://www.zesthotel.com/">W: <span class="span span span-bluelink">www.zesthotel.com</span></a>
            </div>

        </div>

        <div class="flex md-4 my-8">

            <div class="w-full bg-teal-200 p-1  ">
                <span class="span span-status">CANCELLATION 4 DAYS PRIOR ARRIVAL</span>
            </div>

        </div>

        <div class="container">
            <div class="w-full ">
                <span class="text-lg font-bold">
                    <h2>Please enter invoice & voucher detail </h2><span>
            </div>

            <div class="w-full">
                <div class="flex flex-wrap rowtable ">

                    <div class="w-2/12 text-left text-sm font-bold pt-2 flex items-center ">
                        Invoice Contract Person
                    </div>

                    <div class="w-2/12 items-center flex mr-5">
                        <input type="text" class="input " value="">
                    </div>

                    <div class="w-1/12 mt-3">
                        <form>
                            <select class="inputselect rounded">
                                <option value="Mr"> Mr</option>
                                <option value="Mrs">Mrs</option>
                                <option value="Mstr">Mstr</option>
                                <option value="Ms">Ms</option>
                                <option value="Miss">Miss</option>
                                <option value="Inf">Inf</option>
                                <option value="Grp">Grp</option>
                                <option value="Fam">Fam</option>
                                <option value="PT">PT</option>
                                <option value="CV">CV</option>
                            </select>
                        </form>
                    </div>

                </div>

                <div class="flex flex-wrap rowtable ">

                    <div class="w-2/12 text-left text-sm font-bold pt-2 ">
                        Company
                    </div>

                    <div class="w-3/12 items-center flex">
                        <input type="text" class="input " value="">
                    </div>

                </div>

                <div class="flex flex-wrap rowtable ">

                    <div class="w-2/12 text-left text-sm font-bold pt-2 ">
                        Phone
                    </div>

                    <div class="w-3/12 items-center flex">
                        <input type="text" class="input " value="">
                    </div>

                </div>

                <div class="flex flex-wrap rowtable ">

                    <div class="w-2/12 text-left text-sm font-bold pt-2 ">
                        Address
                    </div>

                    <div class="w-3/12 items-center flex">
                        <input type="text" class="input " value="">
                    </div>

                </div>

            </div>

            <div class="w-full mt-4 bg-gray-200 border-0 border-b border-gray-500 ">
                <div>
                    <h2> Wed, 19 Jun 2019 - Thu, 20 Jun 2019 ~ 1 Night(s)</h2>
                </div>

                <div class="container">

                    <div class='flex headtable'>
                        <div class='w-11/12 flex'>
                            <div class='w-1/12'>Qty.</div>
                            <div class='w-2/12'>Room Type & Facility</div>
                            <div class='w-2/12'>Price Per Unit</div>
                            <div class='w-2/12'>Room Price</div>
                            <div class='w-2/12'>Additional Fee</div>
                            <div class='w-2/12'>Discount</div>
                        </div>
                        <div class='w-3/12'>Guest Name & Special Request*</div>
                    </div>

                    <div class='flex rowtable rowtable-blue'>
                        <div class='w-11/12 flex'>
                            <div class='w-1/12'>
                                1
                            </div>
                            <div class='w-2/12'>
                                EXTRA BED & breakfast for 1
                            </div>
                            <div class='w-2/12'>
                                @ 205,000
                            </div>
                            <div class='w-2/12'>
                                205,000
                            </div>
                            <div class='w-2/12'>
                                <input type="text" class="input input-xs w-40" placeholder="0">
                                <i class="fas fa-info-circle"></i>
                            </div>
                            <div class='w-2/12'>
                                <input type="text" class="input input-xs" placeholder="0">
                            </div>
                        </div>
                        <div class='w-3/12'>
                            <div class="flex">
                                <input type="text" class="input input-xs" placeholder="0">

                                <form class="ml-2 h-3">
                                    <select class="inputselect inputselect-sm">
                                        <option value="Mr"> Mr</option>
                                        <option value="Mrs">Mrs</option>
                                        <option value="Mstr">Mstr</option>
                                        <option value="Ms">Ms</option>
                                        <option value="Miss">Miss</option>
                                        <option value="Inf">Inf</option>
                                        <option value="Grp">Grp</option>
                                        <option value="Fam">Fam</option>
                                        <option value="PT">PT</option>
                                        <option value="CV">CV</option>
                                    </select>
                                </form>
                            </div>

                            <div class="">
                                <form>
                                    <select class="inputselect">
                                        <option value="-- Spesial Request --"> -- Spesial Request --</option>
                                        <option value="Room Only">Room Only</option>
                                        <option value="Breakfast for">Breakfast for</option>
                                        <option value="Kingsize Bed">Kingsize Bed</option>
                                        <option value="Twin Bed">Twin Bed</option>
                                        <option value="Smooking Room">Smooking Room</option>
                                        <option value="Non-Smoking Room">Non-Smoking Room</option>
                                        <option value="Connenting Room">Connenting Room</option>
                                        <option value="Other Special Request">Other Special Request</option>
                                    </select>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class='flex rowtable rowtable-navy'>
                        <div class='w-11/12 flex'>
                            <div class='w-1/12'>
                                &nbsp
                            </div>
                            <div class='w-2/12'>
                                &nbsp
                            </div>
                            <div class='w-2/12'>
                                Sub Total :
                            </div>
                            <div class='w-2/12'>
                                205,000
                            </div>
                            <div class='w-2/12'>
                                0,00
                            </div>
                            <div class='w-2/12'>
                                0,00
                            </div>
                        </div>
                        <div class='w-3/12'>
                            &nbsp
                        </div>
                    </div>

                    <div class='flex rowtable rowtable-blue'>
                        <div class='w-11/12 flex'>
                            <div class='w-1/12'>
                                &nbsp
                            </div>
                            <div class='w-2/12'>
                                &nbsp
                            </div>
                            <div class='w-2/12'>
                                <span class="text-base font-bold">Grand Total: </span>
                            </div>
                            <div class='w-2/12'>
                                <span class="text-base font-bold">IDR 205,000</span>
                            </div>
                            <div class='w-2/12'>
                                &nbsp
                            </div>
                            <div class='w-2/12'>
                                &nbsp
                            </div>
                        </div>
                        <div class='w-3/12'>
                            &nbsp
                        </div>
                    </div>

                </div>
            </div>

            <div class="w-full flex border-0 border-b border-black border-solid mt-8 p-2 ">
                <div class="text-sm font-bold w-6/12">
                    Time to complete this reservation: 7 minutes
                </div>

                <div class="w-6/12 justify-end flex">
                    <span class="span span-primary">*Special requests are subject to availability upon check-in.</span>
                </div>
            </div>

            <div class="w-full flex border-0 border-b border-black border-solid mt-8 p-2 ">
                <div class="text-xs font-bold w-6/12">
                    <p>This reservation has passed hotel's cancelation policy time</p>
                    <p>If hotel confirm this reservation, then reservation <span class="span span-danger"> CANNOT BE CANCELED!!</span></p>
                    <p>(No Amend/Cancel/Refund)<p>

                </div>

                <div class="w-6/12 justify-end flex">
                    <div>
                        <span class="span span-blue">Email</span>
                    </div>
                </div>
            </div>


            <div class="container w-full mt-3 flex justify-center">

                <button onclick="window.location.href = 'master_hotelRoomType.php';" class="btn btn-rounded btn-success"><i class="fas fa-check"></i> ACCEPT LG & PROCEED</button>
                <button onclick="window.location.href = 'master_hotelRoomType.php';" class="btn btn-rounded btn-danger"><i class="fas fa-times"></i> REJECT LG & CANCEL</button>

            </div>

        </div>

    </div>



    <?php
    include(base_path . '/component/footer.php');
    ?>
</body>

</html>