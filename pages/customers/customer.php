<html>
<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');
    ?>


    <!-- isi content -->

    <div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">

        <div class="w-full boxrow">
            <div class="flex md-4">
                <span class="span-title">
                    <h2>Master Customer</h2>
                </span>
            </div>
        </div>

        <div class="flex md-4">
            <div class="w-4/12 bg-red">
                <?php
                include(base_path . '/component/padding.php');
                ?>
            </div>

            <div class="w-4/12 items-center justify-center flex">
                <div>
                    Search :
                    <input type="text" class="input">

                </div>
            </div>

            <div class="w-4/12 bg-blue items-center justify-end flex">
                <div class="mr-1">
                    <button class="btn btn-sm btn-rounded btn-primary"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>
            </div>

        </div>

        <div class="w-full bg-gray-300 p-3 flex">

            <div class="w-1/2 flex">
                <div class=" items-center flex mx-3">
                    <input id="option1" type="radio" name="field" value="option1">
                    <label for="option1"><span><span></span></span>All
                    </label>
                </div>

                <div class=" items-center flex mx-3">
                    <input id="option2" type="radio" name="field" value="option1">
                    <label for="option2">
                        <span><span></span></span> Agent
                    </label>
                </div>

                <div class=" items-center flex mx-3">
                    <input id="option3" type="radio" name="field" value="option1">
                    <label class="" for="option3">
                        <span><span></span></span> Corporate
                    </label>
                </div>

                <div class=" items-center flex mx-3">
                    <input id="option4" type="radio" name="field" value="option1">
                    <label class="" for="option4">
                        <span><span></span></span> Spesial Customer
                    </label>
                </div>

                <div class=" items-center flex mx-3">
                    <input id="option5" type="radio" name="field" value="option1">
                    <label class="" for="option5">
                        <span><span></span></span> FIT
                    </label>
                </div>
            </div>

            <div class="w-1/2 flex justify-end">
                <div class=" items-center flex mx-3">
                    <input id="option6" type="radio" name="field" value="option">
                    <label class="" for="option6">
                        <span><span></span></span> All
                    </label>
                </div>

                <div class=" items-center flex mx-3">
                    <input id="option7" type="radio" name="field" value="option">
                    <label class="" for="option7">
                        <span><span></span></span> Deposit
                    </label>
                </div>

                <div class=" items-center flex mx-3">
                    <input id="option8" type="radio" name="field" value="option">
                    <label class="" for="option8">
                        <span><span></span></span> Credit
                    </label>
                </div>
            </div>

        </div>


        <div class="w-full mt-1 bg-gray-200">
            <div class="container">

                <div class='flex headtable'>
                    <div class="w-1/12">No.</div>
                    <div class="w-1/12">Type</div>
                    <div class="w-2/12">Name</div>
                    <div class="w-1/12">Dpst</div>
                    <div class="w-1/12">Branch No</div>
                    <div class="w-1/12">Discount</div>
                    <div class="w-2/12">Address</div>
                    <div class="w-2/12">City</div>
                    <div class="w-2/12">Email</div>
                    <div class="w-2/12">Web Login</div>
                    <div class="w-1/12">User(s)</div>
                    <div class="w-1/12"></div>
                </div>

                <div class='rowtable flex'>
                    <div class='w-1/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <span class="span span-blue">A</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = '<?= root_path ?>/pages/customers/customer_detail.php';" class="span span-bluelink">CAHAYA MENTARI CEMERLANG (DP)</span>
                        </div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <i class="fas fa-check"></i>
                        </div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>01</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>0</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>RICH PALACE H10 JLN. MAYJEN SUNGKONO149-151 </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>SURABAYA </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>travelersind@yahoo.com</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>travelersind </div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-pencil-alt"></i></button>
                        </div>
                    </div>
                </div>






            </div>
        </div>

    </div>



    <?php
    include(base_path . '/component/footer.php');
    ?>
</body>

</html>