<html>

<?php
include(__DIR__ . "/../head.php");
?>



<body>

    <?php
    include(base_path . "/component/topnav.php");
    include(base_path . "/component/slider.php");

    ?>

    <!-- isi content adasd -->

    <div class="container bg-gray-200 mx-auto w-11/12 mt-10 p-5">

        <div class="container max-w-full ">
            <div class="w-full border-0 border-b border-solid border-black">
                <div class="flex md-4">
                    <span class="span-title">
                        <h2>Email Log</h2>
                    </span>
                </div>
            </div>
        </div>

        <div class="container max-w-full mt-4">
            <div class="flex md-4">

                <div class="w-6/12 flex md-4">
                    <?php
                    include("../../component/padding.php");
                    ?>
                </div>

                <div class="flex md-4 items-center w-full justify-end">

                    <div class="">
                        <button onclick="window.location.href = '#';" type="button" class="btn btn-sm text-sm btn-rounded btn-light" value="Accepted">Rejected and Bounced Mail</button>
                    </div>

                    <div class="ml-1">
                        <button onclick="window.location.href = '#';" type="button" class="btn btn-sm text-sm btn-rounded btn-primary" value="Rejected"><i class="fas fa-sync-alt"></i> Reset</button>
                    </div>

                </div>

            </div>
        </div>

        <div class="container  max-w-full bg-white mt-5">

            <div class="container max-w-full">
                <div class="flex">

                    <div class="w-full mx-auto bg-gray-400   ">
                        <div class="container">
                            <div class="headtable flex">
                                <div class="w-1/12">No.</div>
                                <div class="w-1/12">Time</div>
                                <div class="w-3/12">Recipient</div>
                                <div class="w-3/12">Subject</div>
                                <div class="w-3/12">Description</div>
                            </div>

                            <div class="flex rowtable ">
                                <div class="w-1/12">
                                    <div class="">1</div>
                                </div>
                                <div class="w-1/12">
                                    <div class="">26 Mar 2019 14:15</div>
                                </div>
                                <div class="w-3/12">
                                    <div class="">kelapagading@reservation.santika.com</div>
                                </div>
                                <div class="w-3/12">
                                    <div class="">Haryono: Voucher #19014220 - DODY JONATAN </div>
                                </div>
                                <div class="w-3/12">
                                    <div class="">Mail error: SMTP Error: Could not connect to SMTP host.</div>
                                </div>
                            </div>

                            <div class="flex rowtable ">
                                <div class="w-1/12">
                                    <div class="">2</div>
                                </div>
                                <div class="w-1/12">
                                    <div class="">20 Mar 2019 11:21 </div>
                                </div>
                                <div class="w-3/12">
                                    <div class="">reservation@resindahotel.com; rizky.pohan@resindahotel.com</div>
                                </div>
                                <div class="w-3/12">
                                    <div class="">Haryono: New Request - Reservation #19013164</div>
                                </div>
                                <div class="w-3/12">
                                    <div class="">Mail error: SMTP Error: The following recipients failed: reservation@resindahotel.com; rizky.pohan@resindahotel.com </div>
                                </div>
                            </div>

                            <div class="flex rowtable ">
                                <div class="w-1/12">
                                    <div class="">3</div>
                                </div>
                                <div class="w-1/12">
                                    <div class="">11 Mar 2019 17:18</div>
                                </div>
                                <div class="w-3/12">
                                    <div class="">JAKARTADOMESTIC@HARYONOTOURS..COM</div>
                                </div>
                                <div class="w-3/12">
                                    <div class="">Haryono: Voucher #19011320 - M FADRI AL BAIHAQI</div>
                                </div>
                                <div class="w-3/12">
                                    <div class="">Mail error: SMTP Error: The following recipients failed: JAKARTADOMESTIC@HARYONOTOURS..COM</div>
                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>


            <!-- <div class="overlay overlay-relative justify-center flex">
            <div class=" loading loading-lg mt-10">

            </div>
        </div> -->

        </div>

    </div>


    <?php
    include(base_path . "/component/footer.php");
    ?>

</body>

</html>