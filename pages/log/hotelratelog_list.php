<html>

<?php
include(__DIR__ . "/../head.php");
?>



<body>

    <?php
    include(base_path . "/component/topnav.php");
    include(base_path . "/component/slider.php");

    ?>

    <!-- isi content adasd -->

    <div class="container bg-gray-200 mx-auto w-11/12 mt-10 p-5">

        <div class="container max-w-full">
            <div class="w-full boxrow">
                <div class="flex md-4">
                    <span class="span-title">
                        <h2>Hotel Rate Log</h2>
                    </span>
                </div>
            </div>
        </div>

        <div class="container max-w-full mt-3">
            <div>

                <div class="w-7/12 items-center flex md-4">
                    <span class="w-2/12"> Hotel :</span>
                    <div class="w-full ml-5">

                        <input type="text" class="input">
                        <button class="btn btn-sm btn-rounded btn-light">clear</button>
                    </div>
                </div>

                <div class="w-7/12 items-center flex">
                    <span class="w-2/12 ">Rate Code : </span>
                    <div class="w-full flex ml-5">
                        <form>
                            <select class="inputselect">
                                <option value="">1</option>
                                <option value="">2</option>
                                <option value="">3</option>
                                <option value="">4</option>
                                <option value="">5</option>
                            </select>
                        </form>
                    </div>
                </div>

            </div>
        </div>

        <div class="container max-w-full mt-5">
            <div class="flex md-4">

                <div class="flex md-4 w-10/12">
                    <?php
                    include("../../component/padding.php");
                    ?>
                </div>


                <div class="flex md-4 items-center w-full justify-end">

                    <div class="ml-5">
                        <button onclick="window.location.href = '#';" type="button" class="btn btn-sm text-sm btn-rounded btn-primary" value="Rejected"><i class="fas fa-sync-alt"></i> Reset</button>
                    </div>

                </div>

            </div>
        </div>

        <div class="container  max-w-full relative mt-5">

            <div class="md-4">

                <div class="container">
                    <div class="flex">

                        <div class="w-full mx-auto bg-gray-400   ">
                            <div class="container">
                                <div class="flex headtable">
                                    <div class="w-1/12 ">No.</div>
                                    <div class="w-2/12 ">Date</div>
                                    <div class="w-2/12 ">Hotel</div>
                                    <div class="w-1/12 ">Rate Code</div>
                                    <div class="w-2/12 ">Rate Name</div>
                                    <div class="w-2/12 ">Description</div>
                                    <div class="w-1/12 ">User</div>
                                    <div class="w-1/12 ">IP Address</div>

                                </div>

                                <div class="flex rowtable">
                                    <div class="w-1/12">
                                        <div class="">1</div>
                                    </div>
                                    <div class="w-2/12">
                                        <div class="">2019-05-29 14:26:57</div>
                                    </div>
                                    <div class="w-2/12">
                                        <div class="">ZEST HOTEL JEMURSARI X</div>
                                    </div>
                                    <div class="w-1/12">
                                        <div class="">FR 10</div>
                                    </div>
                                    <div class="w-2/12">
                                        <div class="">CONTRACT RATE 2019 - 2020</div>
                                    </div>
                                    <div class="w-2/12">
                                        <div class="">Delete exclusive customer</div>
                                    </div>
                                    <div class="w-1/12">
                                        <div class="">PR2</div>
                                    </div>
                                    <div class="w-1/12">
                                        <div class="">127.0.0.1</div>
                                    </div>


                                </div>

                                <div class="flex rowtable">
                                    <div class="w-1/12">
                                        <div class="">2</div>
                                    </div>
                                    <div class="w-2/12">
                                        <div class="">2019-05-29 14:19:35</div>
                                    </div>
                                    <div class="w-2/12">
                                        <div class="">ZEST HOTEL JEMURSARI X</div>
                                    </div>
                                    <div class="w-1/12">
                                        <div class="">FR 10</div>
                                    </div>
                                    <div class="w-2/12">
                                        <div class="">CONTRACT RATE 2019 - 2020</div>
                                    </div>
                                    <div class="w-2/12">
                                        <div class="">Add exclusive customer: DAIKIN APPLIED SOLUTIONS INDONESIA</div>
                                    </div>
                                    <div class="w-1/12">
                                        <div class="">PR 2</div>
                                    </div>
                                    <div class="w-1/12">
                                        <div class="">127.0.0.1</div>
                                    </div>


                                </div>

                                <div class="flex rowtable">
                                    <div class="w-1/12">
                                        <div class="">3</div>
                                    </div>
                                    <div class="w-2/12">
                                        <div class="">2019-05-29 14:19:28</div>
                                    </div>
                                    <div class="w-2/12">
                                        <div class="">ZEST HOTEL JEMURSARI X</div>
                                    </div>
                                    <div class="w-1/12">
                                        <div class="">FR 10</div>
                                    </div>
                                    <div class="w-2/12">
                                        <div class="">CONTRACT RATE 2019 - 2020</div>
                                    </div>
                                    <div class="w-2/12">
                                        <div class="">Delete exclusive customer</div>
                                    </div>
                                    <div class="w-1/12">
                                        <div class="">PR2</div>
                                    </div>
                                    <div class="w-1/12">
                                        <div class="">127.0.0.1</div>
                                    </div>
                                </div>




                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <!-- <div class="overlay overlay-relative justify-center flex">
            <div class=" loading loading-lg mt-10">

            </div>
        </div> -->

        </div>

    </div>


    <?php
    include(base_path . "/component/footer.php");
    ?>

</body>

</html>