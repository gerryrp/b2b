<html>

<?php
include(__DIR__ . "/../head.php");
?>



<body>

    <?php
    include(base_path . "/component/topnav.php");
    include(base_path . "/component/slider.php");

    ?>

    <!-- isi content adasd -->

    <div class="container bg-gray-200 mx-auto w-11/12 mt-10 p-5">

        <div class="container max-w-full">
            <div class="w-full border-0 border-b border-solid border-black">
                <div class="flex md-4">
                    <span class="span-title">
                        <h2>Publik Link Logs</h2>
                    </span>
                </div>
            </div>
        </div>

        <div class="container max-w-full mt-5">
            <div class="flex ">

                <div class="flex  w-4/12">
                    <?php
                    include("../../component/padding.php");
                    ?>
                </div>

                <div class="w-4/12 items-center justify-center flex">
                    <div>
                        Search :
                        <input type="text" class="input">
                        <button class="btn btn-sm btn-rounded btn-light">Search</button>
                    </div>
                </div>

                <div class="flex w-4/12  items-center justify-end">

                    <div class="ml-5">
                        <button onclick="window.location.href = '#';" type="button" class="btn btn-sm text-sm btn-rounded btn-primary" value="Rejected"><i class="fas fa-sync-alt"></i> Reset</button>
                    </div>

                </div>

            </div>
        </div>

        <div class="container  max-w-full relative mt-5">

            <div class="md-4">

                <div class="container">
                    <div class="flex">

                        <div class=" w-full mx-auto bg-gray-400">
                            <div class="container">
                                <div class="flex headtable">
                                    <div class="w-1/12">No.</div>
                                    <div class="w-2/12">Time</div>
                                    <div class="w-2/12">IP Address</div>
                                    <div class="w-3/12">Hotel</div>
                                    <div class="w-2/12">Reserv No.</div>
                                    <div class="w-2/12">Description</div>
                                </div>

                                <div class="flex rowtabel ">
                                    <div class="w-1/12 p-1">
                                        <div class="">1</div>
                                    </div>
                                    <div class="w-2/12 p-1">
                                        <div class="">2019-03-31 13:19:54</div>
                                    </div>
                                    <div class="w-2/12 p-1">
                                        <div class="">66.96.244.92 </div>
                                    </div>
                                    <div class="w-3/12 p-1">
                                        <div class="">THE SHALIMAR BOUTIQUE HOTEL</div>
                                    </div>
                                    <div class="w-2/12 p-1">
                                        <div class="">19014433</div>
                                    </div>
                                    <div class="w-2/12 p-1">
                                        <div class=""></div>
                                    </div>
                                </div>


                                <div class="flex rowtabel ">
                                    <div class="w-1/12 p-1">
                                        <div class="">2</div>
                                    </div>
                                    <div class="w-2/12 p-1">
                                        <div class="">2019-03-31 12:57:56 </div>
                                    </div>
                                    <div class="w-2/12 p-1">
                                        <div class="">222.165.222.202 </div>
                                    </div>
                                    <div class="w-3/12 p-1">
                                        <div class="">CIPUTRA JAKARTA HOTEL</div>
                                    </div>
                                    <div class="w-2/12 p-1">
                                        <div class="">19014946</div>
                                    </div>
                                    <div class="w-2/12 p-1">
                                        <div class=""></div>
                                    </div>
                                </div>

                                <div class="flex rowtabel ">
                                    <div class="w-1/12 p-1">
                                        <div class="">3</div>
                                    </div>
                                    <div class="w-2/12 p-1">
                                        <div class="">2019-03-31 12:57:56</div>
                                    </div>
                                    <div class="w-2/12 p-1">
                                        <div class="">222.165.222.202 </div>
                                    </div>
                                    <div class="w-3/12 p-1">
                                        <div class="">CIPUTRA JAKARTA HOTEL</div>
                                    </div>
                                    <div class="w-2/12 p-1">
                                        <div class="">19014946</div>
                                    </div>
                                    <div class="w-2/12 p-1">
                                        <div class="">Update success</div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <!-- <div class="overlay overlay-relative justify-center flex">
            <div class=" loading loading-lg mt-10">

            </div>
        </div> -->

        </div>

    </div>


    <?php
    include(base_path . "/component/footer.php");
    ?>

</body>

</html>