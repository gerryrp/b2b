<html>
<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');
    ?>


    <!-- isi content -->

    <div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">

        <div class="w-full boxrow">
            <div class="flex md-4">
                <span class="span-title">
                    <h2>Master State</h2>
                </span>
            </div>
        </div>

        <div class="flex md-4 mt-5">
            <div class="w-4/12 bg-red">
                <?php
                include(base_path . '/component/padding.php');
                ?>
            </div>

            <div class="w-4/12 items-center justify-center flex">
                <div>
                    Search :
                    <input type="text" class="input">
                    <button class="btn btn-sm btn-rounded btn-light">Search</button>
                </div>
            </div>

            <div class="w-4/12  bg-blue items-center justify-end flex">
                <div class="mr-1">
                    <button onclick="window.location.href = '';" class="btn btn-sm btn-rounded btn-success"><i class="fas fa-plus"></i> Add</button>
                    <button class="btn btn-sm btn-rounded btn-danger"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>
            </div>

        </div>


        <div class="w-full mt-1 bg-gray-200">
            <div class="container">

                <div class='flex headtable'>
                    <div class='w-2/12'>No.</div>
                    <div class='w-2/12'>Provience</div>
                    <div class='w-2/12'>Country</div>
                    <div class='w-2/12'>UpdDate</div>
                    <div class='w-2/12'>UpdDate</div>

                </div>

                <div class='flex rowtable'>
                    <div class='w-2/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = 'branch_detail.php';" class="span span span-bluelink">AARGAU</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>SWITZERLAND</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>1990-01-01</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''></div>
                    </div>

                </div>

                <div class='flex rowtable'>
                    <div class='w-2/12'>
                        <div class=''>2</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = 'APIuser_detail.php';" class="span span span-bluelink">ABU DHABI</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>UNITED ARAB EMIRATES</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>2014-06-04</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>SOF</div>
                    </div>

                </div>

            </div>

        </div>
    </div>


    </div>



    <?php
    include(base_path . '/component/footer.php');
    ?>
</body>

</html>