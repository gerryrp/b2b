<html>
<?php
include(__DIR__ . '/../head.php');
?>

<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');
    ?>


    <!-- isi content -->

    <div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">

        <div class="w-full boxrow">
            <div class="flex md-4">
                <span class="span-title">
                    <h2>Invoice List</h2>
                </span>
            </div>
        </div>

        <div class="flex md-4">
            <div class="w-4/12 bg-red">
                <?php
                include(base_path . '/component/padding.php');
                ?>
            </div>

            <div class="w-4/12  items-center justify-center flex">
                <div class="">
                    Invoice No:
                    <input type="text" class="input input-sm">

                </div>
            </div>

            <div class="w-4/12 bg-blue items-center justify-end flex">
                <div class="mr-1">
                    <button class="btn btn-sm btn-rounded btn-primary"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>
            </div>

        </div>

        <div class="w-full mt-1 ">
            <div class="container">

                <div class='flex headtable'>
                    <div class='w-1/12'>No.</div>
                    <div class='w-2/12'>Invoice #</div>
                    <div class='w-2/12'>Invoice Date</div>
                    <div class='w-2/12'>Hotel</div>
                    <div class='w-2/12'>Guest Name</div>
                    <div class='w-2/12'>Voucher #</div>
                    <div class='w-2/12'>Resv. #</div>
                    <div class='w-2/12'>Curr</div>
                    <div class='w-2/12'>Total Price</div>
                    <div class='w-2/12'>Issued By</div>
                </div>

                <div class='flex rowtable '>
                    <div class='w-1/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = '<?= root_path ?>/pages/invoice/invoice_detail.php';" class="span span span-bluelink">HW0119004746</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>31 Mar 2019 19:37</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>POP! HOTEL AIRPORT JAKARTA</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>LUKAS DJUNANTO Mr</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>VW0119005367</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = '<?= root_path ?>/pages/reservation/reservation_detail.php';" class="span span span-bluelink">19014960</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>IDR</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>315,000</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>NISA</div>
                    </div>
                </div>



                <div class='flex rowtable '>
                    <div class='w-1/12'>
                        <div class=''>2</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="span span span-bluelink">HW0119004745</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>31 Mar 2019 12:47</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>GOLDEN TULIP HOLLAND RESORT BATU</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>ROBBY LINDARTANTO, Mr</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>VW0119005366</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="span span span-bluelink">19014959</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>IDR</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>925,000</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>ULIK</div>
                    </div>
                </div>




            </div>
        </div>

    </div>



    <?php
    include(base_path . '/component/footer.php');
    ?>
</body>

</html>