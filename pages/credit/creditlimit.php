<html>
<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');
    ?>


    <!-- isi content -->

    <div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">

        <div class="w-full boxrow">
            <div class="flex md-4">
                <span class="span-title">
                    <h2>Credit Limit Authorization List</h2>
                </span>
            </div>
        </div>

        <div class="flex md-4">
            <div class="w-4/12 bg-red">
                <?php
                include(base_path . '/component/padding.php');
                ?>
            </div>

            <div class="w-4/12 justify-center items-center flex">
                <div>
                    Search
                    <input type="text" class="input">
                    <button class="btn btn-sm btn-rounded btn-light">Search</button>
                </div>
            </div>

            <div class="w-4/12 bg-blue justify-end items-center flex">
                <div class="mr-1">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/credit/creditlimit_add.php';" class="btn btn-sm btn-rounded btn-success"><i class="fas fas fa-plus"></i> Add</button>
                </div>

                <div class="mr-1">
                    <button class="btn btn-sm btn-rounded btn-primary"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>
            </div>

        </div>


        <div class="w-full mt-1 bg-gray-200">
            <div class="container">

                <div class='flex headtable'>
                    <div class='w-1/12'>No.</div>
                    <div class='w-3/12'>Customer</div>
                    <div class='w-1/12'>Cust No.</div>
                    <div class='w-2/12'>Amount</div>
                    <div class='w-1/12'>Username</div>
                    <div class='w-3/12'>Time</div>
                    <div class='w-2/12'></div>
                </div>

                <div class='flex rowtable'>
                    <div class='w-1/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>
                            <span onclick="window.location.href = '<?= root_path ?>/pages/credit/creditlimit_detail.php';" class="span span span-bluelink">AMMAN MINERAL NUSA TENGGARA</span>
                        </div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>4395</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>433.000.000</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>FAN</div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>2019-03-31 20:15:16.086601</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <button class="btn btn-sm btn-light"><i class="fas fa-window-close"></i></button>
                            <button class="btn btn-sm btn-light"><i class="fas fa-pencil-alt"></i></button>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>



    <?php
    include(base_path . '/component/footer.php');
    ?>
</body>

</html>