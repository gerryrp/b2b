<html>

<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');

    ?>

    <!-- isi content -->

    <div class="container bg-gray-200 mx-auto w-11/12 mt-10 p-5">

        <div class="container max-w-full ">
            <div class="w-full boxrow">
                <div class="flex md-4">
                    <span class="span-title">
                        <h2>Master Allotment</h2>
                    </span>
                </div>
            </div>
        </div>

        <div class="container max-w-full ml-5 mt-5">
            <div class="flex md-4">
                <div class="mx-auto flex md-4 items-center">
                    <div class="container">
                        <div class="text-small text-center">
                            Hotel
                        </div>

                        <div class="flex md-4 justify-center items-center">
                            <input type="text" class="input" placeholder="Hotel">
                            <button type="submit" class="fa fa-search ml-1" style="background-color: rgba(255, 255, 255, 0);border-color: rgba(255, 255, 255, 0)"></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="flex md-4">
                <div class="flex md-4 w-10/12">
                    <?php
                    include(base_path . '/component/padding.php');
                    ?>
                </div>
                <div class="flex md-4 w-1/4 items-center justify-end ">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/allotment/hotelAllotment_add.php';" type="button" class="btn btn-sm btn-rounded btn-success mr-3 " value="Sign Up"><i class="fas fa-plus"></i> Add</button>
                    <button type="button" class="btn btn-sm btn-rounded btn-danger mr-3 " value="Sign Up"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>
            </div>
        </div>

        <div class="container  max-w-full">
            <div class="flex">
                <div class="container w-full bg-gray-400 mx-auto  ">
                    <div class='flex headtable'>
                        <div class='w-1/12'>No.</div>
                        <div class='w-3/12'>Name</div>
                        <div class='w-4/12'>Address</div>
                        <div class='w-2/12'>City</div>
                        <div class='w-2/12'>Email</div>
                        <div class='w-1/12'>Star</div>
                        <div class='w-1/12'>&nbsp</div>
                    </div>
                    <div class='flex rowtable'>

                        <div class='w-1/12'>
                            <div class=''>1</div>
                        </div>

                        <div class='w-3/12'>
                            <div class=''>
                                <span onclick="window.location.href = '<?= root_path ?>/pages/allotment/hotelAllotment_detail.php';" class="span span span-bluelink">FAVE HOTEL LOSARI MAKASSAR (FORMERLY FAVE DAENG TOMPO)</span>
                            </div>
                        </div>

                        <div class=' w-4/12'>
                            <div class=''>JL. DAENG TOMPO 28-36 MAKASSAR</div>
                        </div>

                        <div class=' w-2/12'>
                            <div class=''>Makassar</div>
                        </div>

                        <div class=' w-2/12'>
                            <div class=''> engineer@haryono.co.id
                                <span class="badge badge-primary">email</span>
                            </div>
                        </div>

                        <div class=' w-1/12'>
                            <div class=' flex'>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                        </div>

                        <div class=' w-1/12'>
                            <div class=''>
                                <button onclick="window.location.href = 'hotelrate_detail.php';" class="btn btn-sm btn-light">Rate</button>

                            </div>
                        </div>

                    </div>
                    <div class='flex rowtable'>

                        <div class='w-1/12'>
                            <div class=''>2</div>
                        </div>

                        <div class=' w-3/12'>
                            <div class=''>
                                <span class="span span span-bluelink">Grand Clarion Hotel & Convention Kendari</span>
                            </div>
                        </div>

                        <div class=' w-4/12'>
                            <div class=''>Jalan Edy Sabara No.89, Kendari Barat, Lahundape, Kendari Bar., Kota Kendari, Sulawesi Tenggara 93121</div>
                        </div>

                        <div class=' w-2/12'>
                            <div class=''>Kendari</div>
                        </div>

                        <div class=' w-2/12'>
                            <div class=''>engineer@haryono.co.id
                                <span class="badge badge-primary">email</span>
                            </div>
                        </div>

                        <div class=' w-1/12'>
                            <div class=''>

                            </div>
                        </div>

                        <div class=' w-1/12'>
                            <div class=''>
                                <button class="btn btn-sm btn-light">Rate</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>


    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>