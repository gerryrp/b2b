<html>

<?php
include(__DIR__ . '/../head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');

    ?>
    <br>
    <br>
    <br>
    <!-- isi content -->

    <div class="container w-11/12 mx-auto">

        <div class="container max-w-full">

            <div class="w-full border-0 border-b border-solid border-gray">
                <div class="flex md-4">
                    <span class="span-title">
                        <h2>Room Type Group List</h2>
                    </span>
                </div>
            </div>

        </div>

        <div class="container">
            <div class="flex md-4">

                <div class="container w-2/5">
                    <?php
                    include(base_path . '/component/padding.php');
                    ?>
                </div>

                <div class="container w-2/5 flex items-center">

                    <div class="text-small ">
                        Hotel : &nbsp
                    </div>

                    <div class="flex md-4">
                        <input type="text" class="input" placeholder="Hotel">
                        <button type="submit" class="fa fa-search ml-1" style="background-color: rgba(255, 255, 255, 0);border-color: rgba(255, 255, 255, 0)"></button>
                    </div>

                </div>

                <div class="flex w-1/5 items-center justify-end">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/allotment/roomTypeGroup_add.php';" type="button" class="btn btn-sm btn-rounded btn-primary mr-3  " value="Sign Up"><i class="fas fa-plus"></i> Add</button>
                    <button type="button" class="btn btn-sm btn-danger btn-rounded mr-3  " value="Sign Up"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>

            </div>
        </div>

        <div class="container max-w-full bg-white">
            <div class="flex md-4">

                <div class="container bg-gray-400 w-full">
                    <div class='flex headtable'>
                        <div class='w-4/12'>No.</div>
                        <div class='w-4/12'>Group Type</div>
                        <div class='w-4/12'>&nbsp</div>
                    </div>

                    <div class='flex rowtable'>
                        <div class='w-4/12'>
                            1
                        </div>
                        <div class='w-4/12'>
                            <span class="span span span-bluelink"></span>
                        </div>
                        <div class='w-4/12'>
                            <button><i class="fas fa-pencil-alt"></i></button>
                        </div>
                    </div>

                    <div class='flex rowtable'>
                        <div class='w-4/12'>
                            2
                        </div>
                        <div class='w-4/12'>
                            <span class="span span span-bluelink">--</span>
                        </div>
                        <div class='w-4/12'>
                            <button><i class="fas fa-pencil-alt"></i></button>
                        </div>
                    </div>

                    <div class='flex rowtable'>
                        <div class='w-4/12'>
                            3
                        </div>
                        <div class='w-4/12'>
                            <span class="span span span-bluelink">--</span>
                        </div>
                        <div class='w-4/12'>
                            <button><i class="fas fa-pencil-alt"></i></button>
                        </div>
                    </div>

                    <div class='flex rowtable'>
                        <div class='w-4/12'>
                            4
                        </div>
                        <div class='w-4/12'>
                            <span class="span span span-bluelink">--</span>
                        </div>
                        <div class='w-4/12'>
                            <button><i class="fas fa-pencil-alt"></i></button>
                        </div>
                    </div>

                    <div class='flex rowtable'>
                        <div class='w-4/12'>
                            5
                        </div>
                        <div class='w-4/12'>
                            <span onclick="window.location.href = '<?= root_path ?>/pages/allotment/roomTypeGroup_detail.php';" class="span span span-bluelink">2 BED MANGGAR VILLAS</span>
                        </div>
                        <div class='w-4/12'>
                            <button><i class="fas fa-pencil-alt"></i></button>
                        </div>
                    </div>

                    <div class='flex rowtable'>
                        <div class='w-4/12'>
                            6
                        </div>
                        <div class='w-4/12'>
                            <span class="span span span-bluelink">1 BED ROOM VILLA</span>
                        </div>
                        <div class='w-4/12'>
                            <button><i class="fas fa-pencil-alt"></i></button>
                        </div>
                    </div>

                    <div class='flex rowtable'>
                        <div class='w-4/12'>
                            7
                        </div>
                        <div class='w-4/12'>
                            <span class="span span span-bluelink">1 BED</span>
                        </div>
                        <div class='w-4/12 justify-end flex'>
                            <button><i class="fas fa-pencil-alt"></i></button>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>