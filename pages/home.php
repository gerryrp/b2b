<html>
<?php
include('head.php');
?>


<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');
    ?>

    <div class="container w-11/12 mx-auto">

        <?php include(base_path . '/component/vertical_tabs.php'); ?>

    </div>

    <!-- isi content-->

    <div class="container max-w-full bg-white mt-5 ">

        <div class="container max-w-full ml-5">
            <div class="flex md-4">
                <div class="w-1/5  h-10">
                    <span style="font-size:20px; font-weight:bold;">
                        <h2>Order Pool</h2>
                    </span>
                </div>
            </div>
        </div>



        <div class="w-11/12 mx-auto bg-gray-400 ">

            <div class="container">

                <div class='flex headtable'>
                    <div class='w-2/12'>No.</div>
                    <div class='w-full'>Booking Hotel</div>
                    <div class='w-full'>Hotel</div>
                    <div class='w-full'>City</div>
                    <div class='w-full'>Guest Name</div>
                    <div class='w-full'>Booked by</div>
                </div>

                <div class='flex rowtable '>
                    <div class='w-2/12'>
                        1.
                    </div>
                    <div class='w-full'>
                        22 may 2014 11:36
                    </div>
                    <div class='w-full'>
                        AMARTERRA VILLAS BALI NUSA DUA (EX. M GALLERY)
                    </div>
                    <div class='w-full'>
                        Bali
                    </div>
                    <div class='w-full'>
                        Test
                    </div>
                    <div class='w-full'>
                        <div class="badge badge-danger"> Canceled by OP</div>
                    </div>
                </div>

                <div class='flex rowtable '>
                    <div class='w-2/12'>
                        2.
                    </div>
                    <div class='w-full'>
                        23 may 2014 09:36
                    </div>
                    <div class='w-full'>
                        AMARTERRA VILLAS BALI NUSA DUA (EX. M GALLERY)
                    </div>
                    <div class='w-full'>
                        Bali
                    </div>
                    <div class='w-full'>
                        Test
                    </div>
                    <div class='w-full'>
                        <div class="badge badge-danger"> Canceled by OP</div>
                    </div>
                </div>

            </div>

        </div>



        <div class="container max-w-full ml-5 mt-3">
            <div class="flex md-4 ">
                <div class=" w-1/5  h-10">
                    <span style="font-size:20px; font-weight:bold;">
                        <h2>Inbox</h2>
                    </span>
                </div>
            </div>
        </div>


        <div class="w-11/12 mx-auto bg-gray-400 ">

            <div class="container">

                <div class='flex headtable'>
                    <div class='w-2/12'>No.</div>
                    <div class='w-full'>Booking Hotel</div>
                    <div class='w-full'>Hotel</div>
                    <div class='w-full'>City</div>
                    <div class='w-full'>Guest Name</div>
                    <div class='w-full'>Booked by</div>
                </div>

                <div class='flex rowtable '>
                    <div class='w-2/12'>
                        1.
                    </div>
                    <div class='w-full'>
                        22 may 2014 11:36
                    </div>
                    <div class='w-full'>
                        AMARTERRA VILLAS BALI NUSA DUA (EX. M GALLERY)
                    </div>
                    <div class='w-full'>
                        Bali
                    </div>
                    <div class='w-full'>
                        Test
                    </div>
                    <div class='w-full'>
                        <div class="badge badge-danger"> Canceled by OP</div>
                    </div>
                </div>

                <div class='flex rowtable '>
                    <div class='w-2/12'>
                        2.
                    </div>
                    <div class='w-full'>
                        23 may 2014 12:36
                    </div>
                    <div class='w-full'>
                        AMARTERRA VILLAS BALI NUSA DUA (EX. M GALLERY)
                    </div>
                    <div class='w-full'>
                        Bali
                    </div>
                    <div class='w-full'>
                        Test
                    </div>
                    <div class='w-full'>
                        <div class="badge badge-danger"> Canceled by OP</div>
                    </div>
                </div>

            </div>

        </div>


    </div>




    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>