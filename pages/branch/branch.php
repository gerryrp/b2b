<html>
<?php
include(__DIR__ . '/../head.php');
?>

<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');
    ?>


    <!-- isi content -->

    <div class="container w-11/12 bg-gray-200 mx-auto mt-10 p-5">

        <div class="w-full boxrow">
            <div class="flex md-4">
                <span class="span-title">
                    <h2>Master Branch</h2>
                </span>
            </div>
        </div>

        <div class="flex md-4 mt-5">
            <div class="w-4/12 bg-red">

            </div>

            <div class="w-4/12 items-center justify-center flex">
                <div>
                    Search :
                    <input type="text" class="input">
                    <button class="btn btn-sm btn-rounded btn-light">Search</button>
                </div>
            </div>

            <div class="w-4/12 bg-blue items-center justify-end flex">
                <div class="mr-1">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/branch/branch_add.php';" class="btn btn-sm btn-rounded btn-success"><i class="fas fa-plus"></i> Add</button>
                    <button class="btn btn-sm btn-rounded btn-primary"><i class="fas fa-sync-alt"></i> Reset</button>
                </div>
            </div>

        </div>

        <div class="w-full mt-1 bg-gray-200">
            <div class="container">

                <div class='headtable flex'>
                    <div class='w-1/12'>No.</div>
                    <div class='w-2/12'>Branch</div>
                    <div class='w-3/12'>Address</div>
                    <div class='w-3/12'>Email</div>
                    <div class='w-2/12'>VC Code</div>
                    <div class='w-1/12'></div>
                </div>

                <div class='flex rowtable'>
                    <div class='w-1/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = '<?= root_path ?>/pages/branch/branch_detail.php';" class="span span span-bluelink">Haryono SBY - HO</span>
                        </div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>Sulawesi 27-29, Surabaya 60281</div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>info@haryonotours.com</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>01</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-pencil-alt"></i></button>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                </div>

                <div class='flex rowtable'>
                    <div class='w-1/12'>
                        <div class=''>2</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = 'APIuser_detail.php';" class="span span span-bluelink">Haryono JKT- KBR </span>
                        </div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>Kebon Sirih Raya 9K, Jakarta Pusat</div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>jakarta@haryonotours.com</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>02</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-pencil-alt"></i></button>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                </div>

                <div class='flex rowtable'>
                    <div class='w-1/12'>
                        <div class=''>3</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = 'APIuser_detail.php';" class="span span span-bluelink">Haryono MLG - KHR</span>
                        </div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>Kahuripan 22, Malang</div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>malang@haryonotours.com</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>03</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-pencil-alt"></i></button>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                </div>

                <div class='flex rowtable'>
                    <div class='w-1/12'>
                        <div class=''>4</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = 'APIuser_detail.php';" class="span span span-bluelink">Haryono SRG - PDN</span>
                        </div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>Thamrin 89, Semarang</div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>semarang@haryonotours.com</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>04</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-pencil-alt"></i></button>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                </div>

                <div class='flex rowtable'>
                    <div class='w-1/12'>
                        <div class=''>5</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = 'APIuser_detail.php';" class="span span span-bluelink">Haryono SBY - PSD</span>
                        </div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>Panglima Sudirman 93-II, Surabaya 60271</div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>pangsud@haryonotours.com</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>05</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-pencil-alt"></i></button>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                </div>

                <div class='flex rowtable'>
                    <div class='w-1/12'>
                        <div class=''>6</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = 'APIuser_detail.php';" class="span span span-bluelink">Haryono JKT - KLP</span>
                        </div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>Boulevard Raya blok PA 19/8 Pegangsaan 2, Kelapa Gading, Jakarta Utara</div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>kelapagading@haryonotours.com</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>06</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-pencil-alt"></i></button>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                </div>

                <div class='flex rowtable'>
                    <div class='w-1/12'>
                        <div class=''>7</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span onclick="window.location.href = 'APIuser_detail.php';" class="span span span-bluelink">Haryono SBY - RPL</span>
                        </div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>Ruko Rich Palace C/8, Mayjend. Sungkono, Surabaya 60225</div>
                    </div>
                    <div class='w-3/12'>
                        <div class=''>richpalace@haryonotours.com</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>07</div>
                    </div>
                    <div class='w-1/12'>
                        <div class=''>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-pencil-alt"></i></button>
                            <button onclick="window.location.href = '';" type="button"><i class="fas fa-times"></i></button>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>



    <?php
    include(base_path . '/component/footer.php');
    ?>
</body>

</html>