<html>

<?php
include(__DIR__ . '/../head.php');
?>



<body>

    <?php
    include(base_path . '/component/topnav.php');
    include(base_path . '/component/slider.php');

    ?>

    <!-- isi content adasd -->

    <div class="container bg-gray-200 mx-auto w-11/12 mt-10 p-5">

        <div class="container mt-4 ">

            <div class="flex md-4">

                <div class="flex md-4 ">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/intinvoice/intinvoice.php';" type="button" class="btn btn-sm btn-rounded btn-light mr-3 " value="Sign Up"><i class="fas fa-angle-double-left"></i> Invoice List</button>
                </div>

                <div class="flex md-4 ">
                    <button onclick="window.location.href = '<?= root_path ?>/pages/ireservation/intreservation_detail.php';" type="button" class="btn btn-sm btn-rounded btn-light mr-3 " value="Sign Up"><i class="fas fa-angle-double-left"></i> Reservation Detail</button>
                </div>

            </div>

        </div>

        <div class="container max-w-full mt-3 flex">
            <div class="w-full">

                <h4 class="headtable headtable-navy">Invoice</h4>

                <div class="flex flex-wrap rowtable ">

                    <div class="w-2/12 text-left text-xs pt-2 font-bold ">
                        Invoice No.
                    </div>

                    <div class="w-2/12 text-xs items-center flex">
                        <span class="">HW0119004746</span>
                    </div>

                    <div class="w-6/12 items-center flex">

                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-print"></i> Incoive</button>

                        <button class="btn btn-sm btn-rounded btn-light"><i class="fas fa-print"></i> Voucher</button>

                    </div>

                    <div class="w-2/12 justify-end flex">
                        <button class="btn btn-sm btn-rounded btn-danger"><i class="fas fa-times"></i> VOID</button>
                    </div>

                </div>

                <div class="flex flex-wrap rowtable ">

                    <div class="w-2/12 text-left text-xs pt-2 font-bold ">
                        Invoice Date
                    </div>

                    <div class="w-2/12 text-xs items-center flex">
                        <span class="">14 Mar 2019 10:35</span>
                    </div>

                    <div class="w-8/12 text-xs items-center justify-end flex">
                        <button class="btn btn-sm btn-rounded btn-warning"><i class="fas fa-history"></i> REFUND</button>
                    </div>

                </div>

                <div class="flex flex-wrap rowtable ">

                    <div class="w-2/12 text-left text-xs pt-2 font-bold ">
                        Hotel
                    </div>

                    <div class="w-2/12 text-xs items-center flex">
                        <span class="">Courtyard by Marriott Taipei</span>
                    </div>

                </div>

                <div class="flex flex-wrap rowtable ">

                    <div class="w-2/12 text-left text-xs pt-2 font-bold ">
                        Guest Name
                    </div>

                    <div class="w-2/12 text-xs items-center flex">
                        <span class="">LIAU AH YANG</span>
                    </div>

                </div>



            </div>

        </div>

        <div class="w-full mt-8 bg-gray-200 border-0 border-b border-gray-500 ">
            <div>
                <span class="font-bold">
                    <h2>Check-in: 17 March 2019 - Check-out: 18 March 2019 ~ 1 night(s)</h2>
                </span>
            </div>

            <div class="container">

                <div class='headtable flex'>
                    <div class='w-2/12'>Sun Booking No.</div>
                    <div class='w-2/12'>Qty.</div>
                    <div class='w-2/12'>Room Type</div>
                    <div class='w-2/12'>Meal</div>
                    <div class='w-2/12'>Special Request</div>
                    <div class='w-2/12'></div>
                    <div class='w-2/12'>Total</div>
                </div>

                <div class='rowtable flex'>
                    <div class='w-2/12'>
                        <div class=''>SH7278225</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>1</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>Twin/Double room - De Luxe</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>No Meals</div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>

                            <div class="">
                                Kingsize Bed
                            </div>

                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''></div>
                    </div>
                    <div class='w-2/12'>
                        <div class=' text-right'>
                            <div class="">
                                3,385,000
                            </div>
                        </div>
                    </div>

                </div>

                <div class='rowtable flex'>
                    <div class='w-2/12'>
                        <div class=''></div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''></div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''>
                            <span class="span span-danger"></span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=''></div>
                    </div>
                    <div class='w-2/12'>
                        <div class=' text-left'></div>
                    </div>
                    <div class='w-2/12'>
                        <div class=' text-right'>
                            <span class="font-bold">Grand Total</span>
                        </div>
                    </div>
                    <div class='w-2/12'>
                        <div class=' text-right'>
                            <span class="font-bold">IDR 3,385,000</span>

                        </div>
                    </div>

                </div>




            </div>
        </div>


    </div>


    <?php
    include(base_path . '/component/footer.php');
    ?>

</body>

</html>