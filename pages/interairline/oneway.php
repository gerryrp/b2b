<html>
<?php
include(__DIR__ . "/../head.php");
?>

<body>
    <?php
    include(base_path . "/component/topnav.php");
    include(base_path . "/component/slider.php");
    ?>

    <div class="container w-11/12 mx-auto ">
        <?php include(base_path . '/component/vertical_tabs.php'); ?>
    </div>

    <div class="container w-11/12 mx-auto mt-5 p-5">
        <div class="w-full">
            Available fare<br>
            <span class="span span-primary text-2xl">1. Jakarta to Singapore</span>
        </div>

        <div class=" mt-5 flex items-center">
            <div class="w-4/12">
                <?php
                include(base_path . '/component/padding.php');
                ?>
            </div>

            <div class="w-8/12 flex items-center justify-end">
                <span class="mx-3">SORT BY: </span>
                <button class="mx-3">TRAVEL DURATION </button>
                <button class="mx-3">PRICE </button>
                <button class="mx-3">ARRIVAL TIME </button>
                <button class="mx-3">DEPARTURE TIME </button>
                <button class="ml-3">SQ FLIGHT </button>
            </div>

        </div>

        <div class="widefatbox border border-solid border-gray-600 mx-auto flex">
            <div class="w-9/12">
                <span class="span span-status text-base">Non-stop • 1hr 50mins</span><br>
                <div class="w-full flex">
                    <div class="w-4/12">
                        <span class="span span-primary text-3xl">CGK 17:00</span><br>
                        <span class="span span-status">Jakarta</span><br>
                        <span class="text-gray-700 text-sm">12 SEP (Thu)</span><br>
                        <div id="">
                            <span class="text-gray-700 text-sm">Soekarno Intl</span><br>
                            <span class="text-gray-700 text-sm">Terminal 3</span><br>
                        </div>
                    </div>
                    <div class="w-4/12">
                        <span class="span span-primary text-3xl">SIN 19:50</span><br>
                        <span class="span span-status">Singapore</span><br>
                        <span class="text-gray-700 text-sm">12 SEP (Thu)</span><br>
                        <div id="">
                            <span class="text-gray-700 text-sm">Changi</span><br>
                        </div>
                    </div>
                    <div class="w-4/12">
                        <span class="span span-status text-base">Singapore Airlines • SQ 961</span><br>
                        <span id="" class="text-gray-700 text-sm">Boeing 777-300</span><br>
                        <span id="" class="span span-success text-sm">Economy</span><br>
                        <span onclick="hidedetail()" class="span span-bluelink"><i class="fas fa-angle-right"></i> View Detail</span><br>
                    </div>
                </div>
            </div>
            <div class="w-3/12 bg-blue-300">
                <div class="justify-center flex">ECONOMY</div>
                <div class="justify-center flex mt-8 text-xs">FROM IDR</div>
                <div class="justify-center flex text-2xl">7,930,000</div>
                <div class="justify-center flex text-xs">PER ADULT</div>
                <div onclick="hideinfo()" class="justify-center flex text-2xl"><button><i class="fas fa-angle-down"></i></button></div>
            </div>
        </div>

        <div id="test" class="container border-2 border-solid border-blue-400 hidden">
            <div class="bg-blue-400 p-5 font-bold">
                ECONOMY FLEXI FARE CONDITIONS
            </div>
            <div class=" p-5 flex">
                <div class="w-5/12 flex items-center border-0 border-r-2 border-solid border-gray-400">
                    <div class="w-full">
                        <div class="flex">
                            <div class="w-6/12 text-blue-600 text-lg"><i class="fas fa-suitcase-rolling"></i> Baggage</div>
                            <div class="w-6/12 text-blue-600 text-lg"> 35kg</div>
                        </div>
                        <div class="flex">
                            <div class="w-6/12 text-blue-600 text-lg"><i class="fas fa-suitcase-rolling"></i> Seat selection at booking</div>
                            <div class="w-6/12 text-lg span span-success"> Complimentary (Standard & Forward Zone Seats)</div>
                        </div>
                        <div class="flex">
                            <div class="w-6/12 text-blue-600 text-lg"><i class="fas fa-gift"></i> Earn KrisFlyer miles</div>
                            <div class="w-3/12 text-blue-600 text-lg"> 100%</div>
                            <button class="w-3/12"><i class="fas fa-info-circle"></i></button>
                        </div>
                        <div class="flex">
                            <div class="w-6/12 text-blue-600 text-lg"><i class="fas fa-arrow-circle-up"></i> Upgrade with miles</div>
                            <div class="w-3/12 text-blue-600 text-lg"> Allowed</div>
                            <button class="w-3/12"><i class="fas fa-info-circle"></i></button>
                        </div>
                    </div>
                </div>

                <div class="w-4/12 ml-10">
                    <div class="">
                        <div class="flex  my-3">
                            <div class="w-7/12 text-blue-600 text-lg"><i class="fas fa-window-close"></i> Cancellation</div>
                            <div class="w-5/12 text-blue-600 text-lg"> IDR 750,000</div>
                        </div>
                        <div class="flex my-3">
                            <div class="w-7/12 text-blue-600 text-lg"><i class="fas fa-retweet"></i> Booking change</div>
                            <div class="w-4/12 text-lg span span-success"> Complimentary</div>
                        </div>
                        <div class="flex my-3">
                            <div class="w-7/12 text-blue-600 text-lg"><i class="fas fa-times-circle"></i> No show</div>
                            <div class="w-5/12 text-blue-600 text-lg"> IDR 1,500,000 </div>
                        </div>
                    </div>
                </div>

                <div class="w-3/12 items-center">
                    <div class="w-full">
                        <div class="justify-center flex text-2xl">IDR 7,930,000</div><br>
                    </div>
                    <div class="w-full justify-center flex ">
                        <button onclick="hidefare()" class="btn btn-lg btn-rounded btn-primary">SELECT</button>
                    </div>
                </div>
            </div>
            <div class="bg-blue-300 w-3/12 p-2 items-center flex ml-2 mb-2">
                <i class="fas fa-medal text-4xl"></i>
                <span> View PPS Club / KrisFlyer privileges </span>
            </div>
        </div>
    </div>

    <div id="hidden" class="container w-11/12 mx-auto  mt-3 p-5 hidden">
        <div class="widefatbox bg-gray-100 shadow">
            <span class="span span-primary text-2xl">Here's a summary of fare conditions</span><br>
            <span>For more details on Travel Itinerary Sequence and No Show, please refer to the full fare conditions.</span>

            <div id="" class="container border-2 border-solid border-blue-400 mt-3">
                <div class="w-full container bg-gray-200">
                    <div class="flex headtable h-12 items-center">
                        <div class="w-6/12">FARE CONDITIONS</div>
                        <div class="w-6/12">
                            <div class="w-full justify-center flex">CGK - SIN</div>
                            <div class="w-full justify-center flex">Economy Flexi</div>
                        </div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-6/12">
                            <div class="span span-primary text-lg mt-3"><i class="fas fa-suitcase-rolling"></i> Baggage</div>
                            <div class="span span-primary text-lg my-3">Seat selection at booking</div>
                        </div>
                        <div class="w-6/12">
                            <div class="justify-center flex text-lg span span-primary font-bold mt-3">35kg</div>
                            <div class="justify-center flex text-lg span span-success font-bold my-3">Complimentary (Standard & Forward Zone Seats)</div>
                        </div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-6/12">
                            <span class="flex items-center span span-primary text-lg mt-3"><i class="fas fa-gift mr-1"></i> Earn KrisFlyer miles</span>
                            <span class="flex items-center span span-primary text-lg my-3"><i class="fas fa-arrow-circle-up mr-1"></i> Upgrade with miles</span>
                        </div>
                        <div class="w-6/12">
                            <span class="justify-center items-center flex span span-primary text-lg font-bold mt-3">100% </span>
                            <span class="justify-center items-center flex span span-primary text-lg font-bold my-3">Allowed </span>
                        </div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-6/12">
                            <span class="flex items-center span span-primary text-lg mt-3"><i class="fas fa-window-close  mr-1"></i> Cancellation</span>
                            <span class="flex items-center span span-primary text-lg my-3"><i class="fas fa-retweet mr-1"></i> Booking change</span>
                            <span class="flex items-center span span-primary text-lg mb-3"><i class="fas fa-times-circle mr-1"></i> No show</span>
                        </div>
                        <div class="w-6/12">
                            <span class="justify-center flex span span-primary text-lg font-bold mt-3">IDR 750,000</span>
                            <span class="justify-center flex span span-primary text-lg font-bold my-3">IDR Complimentary</span>
                            <span class="justify-center flex span span-primary text-lg font-bold mb-3">IDR 1,500,000</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-5">Fees shown here are indicative. As the original fees are based in US dollars, the actual fees may vary due to exchange rate fluctuations. The original fees can be found on the ticket of a confirmed booking.</div>
            <div class="span span-bluelink text-base mt-5"><i class="fas fa-angle-right"></i> Full fare rules and conditions</div>
            <div class="span span-status text-lg mt-5">Fares are not guaranteed until payment is completed</div>
        </div>
    </div>

    <?php
    include(base_path . "/component/footer.php");
    ?>

    <script>
        function hideinfo() {
            var x = document.getElementById("test");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

        function hidefare() {
            var y = document.getElementById("hidden");
            if (y.style.display === "none") {
                y.style.display = "block";
            } else {
                y.style.display = "none";
            }
        }
    </script>
</body>

</html>