<html>
<?php
include(__DIR__ . "/../head.php");
?>

<body>
    <?php
    include(base_path . "/component/topnav.php");
    include(base_path . "/component/slider.php");
    ?>

    <div class="container w-11/12 mx-auto ">
        <?php include(base_path . '/component/vertical_tabs.php'); ?>
    </div>

    <div class="container w-11/12 mx-auto mt-5 p-5">
        <div>
            Lowest available fare<br>
            <span class="span span-primary text-2xl">1. Jakarta to Singapore</span>
        </div>
        <div class=" mt-5 flex items-center">
            <div class="w-4/12">
                <?php
                include(base_path . '/component/padding.php');
                ?>
            </div>

            <div class="w-8/12 flex items-center justify-end">
                <span class="mx-3">SORT BY: </span>
                <button class="mx-3">TRAVEL DURATION </button>
                <button class="mx-3">PRICE </button>
                <button class="mx-3">ARRIVAL TIME </button>
                <button class="mx-3">DEPARTURE TIME </button>
                <button class="ml-3">SQ FLIGHT </button>
            </div>
        </div>

        <div class="w-full container">
            <div class="widefatbox border border-solid border-gray-600 mx-auto flex">
                <div class="w-9/12">
                    <span class="span span-status text-base">Non-stop • 1hr 50mins</span><br>
                    <div class="w-full flex">
                        <div class="w-4/12">
                            <span class="span span-primary text-3xl">CGK 18:05</span><br>
                            <span class="span span-status">Jakarta</span><br>
                            <span class="text-gray-700 text-sm">12 SEP (Thu)</span><br>
                            <div id="hidden">
                                <span class="text-gray-700 text-sm">Soekarno Intl</span><br>
                                <span class="text-gray-700 text-sm">Terminal 3</span><br>
                            </div>
                        </div>
                        <div class="w-4/12">
                            <span class="span span-primary text-3xl">SIN 20:55</span><br>
                            <span class="span span-status">Singapore</span><br>
                            <span class="text-gray-700 text-sm">12 SEP (Thu)</span><br>
                            <div id="hidden">
                                <span class="text-gray-700 text-sm">Changi</span><br>
                                <span class="text-gray-700 text-sm">Terminal 3</span>
                            </div>
                        </div>
                        <div class="w-4/12">
                            <span class="span span-status text-base">Singapore Airlines • SQ 963</span><br>
                            <span onclick="hideviewdetail()" class="span span-bluelink"><i class="fas fa-angle-right"></i> View Detail</span><br>
                            <span id="hidden" class="text-gray-700 text-sm">Airbus A350-900</span><br>
                            <span id="hidden" class="span span-success text-sm">Economy</span><br>
                        </div>
                    </div>
                </div>
                <div class="w-3/12 bg-blue-300">
                    <div class="justify-center flex">ECONOMY</div>
                    <div class="justify-center flex mt-8 text-xs">FROM IDR</div>
                    <div class="justify-center flex text-2xl">4,385,200</div>
                    <div class="justify-center flex text-xs">PER ADULT</div>
                    <div onclick="hideinfo()" class="justify-center flex text-2xl"><button><i class="fas fa-angle-down"></i></button></div>
                </div>
            </div>

            <div id="test" class="container border-2 border-solid border-blue-400 hidden">
                <div class="w-full container bg-gray-200">
                    <div class="flex headtable h-10 items-center">
                        <div class="w-4/12 flex justify-center">FARE CONDITIONS</div>
                        <div class="w-4/12 flex justify-center">ECONOMY STANDARD</div>
                        <div class="w-4/12 flex justify-center">ECONOMY FLEXI</div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-4/12">
                            <div class="span span-primary text-base mt-3"><i class="fas fa-suitcase-rolling"></i> Baggage</div>
                            <div class="span span-primary text-base my-3">Seat selection at booking</div>
                        </div>
                        <div class="w-4/12">
                            <div class="justify-center flex text-base span span-primary font-bold mt-3">30kg</div>
                            <div class="justify-center flex text-base span span-success font-bold my-3">Complimentary (Standard Seats)</div>
                        </div>
                        <div class="w-4/12">
                            <div class="justify-center flex text-base font-bold span span-primary mt-3">35kg</div>
                            <div class="justify-center flex text-base font-bold span span-success my-3">Complimentary (Standard & Forward Zone Seats)</div>
                        </div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-4/12">
                            <span class="flex items-center span span-primary text-base mt-3"><i class="fas fa-gift mr-1"></i> Earn KrisFlyer miles</span>
                            <span class="flex items-center span span-primary text-base my-3"><i class="fas fa-arrow-circle-up mr-1"></i> Upgrade with miles</span>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center items-center flex span span-primary text-base font-bold mt-3">75% <i class="fas fa-info-circle ml-3"></i></span>
                            <span class="justify-center items-center flex span span-primary text-base font-bold my-3">Allowed <i class="fas fa-info-circle ml-3"></i></span>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center items-center flex span span-primary text-base font-bold mt-3">100% <i class="fas fa-info-circle ml-3"></i></span>
                            <span class="justify-center items-center flex span span-primary text-base font-bold my-3">Allowed <i class="fas fa-info-circle ml-3"></i></span>
                        </div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-4/12">
                            <span class="flex items-center span span-primary text-base mt-3"><i class="fas fa-window-close  mr-1"></i> Cancellation</span>
                            <span class="flex items-center span span-primary text-base my-3"><i class="fas fa-retweet mr-1"></i> Booking change</span>
                            <span class="flex items-center span span-primary text-base mb-3"><i class="fas fa-times-circle mr-1"></i> No show</span>
                            <div class="bg-blue-300 w-8/12 p-2 items-center flex ml-2 my-5">
                                <i class="fas fa-medal text-4xl"></i>
                                <span> View PPS Club / KrisFlyer privileges </span>
                            </div>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center flex span span-primary text-base font-bold mt-3">IDR 2,250,000</span>
                            <span class="justify-center flex span span-primary text-base font-bold my-3">IDR 300,000</span>
                            <span class="justify-center flex span span-primary text-base font-bold mb-3">IDR 1,500,000</span>
                            <span class="justify-center flex span span-primary text-2xl font-bold my-5">IDR 3,093,900</span>
                            <div class="flex justify-center">
                                <button class="btn btn-primary">SELECT</button>
                            </div>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center flex span span-primary text-base font-bold mt-3">IDR 750,000</span>
                            <span class="justify-center flex span span-success text-base font-bold my-3">Complimentary</span>
                            <span class="justify-center flex span span-primary text-base font-bold mb-3">IDR 1,500,000</span>
                            <span class="justify-center flex span span-primary text-2xl font-bold my-5">IDR 5,068,900</span>
                            <div class="flex justify-center">
                                <button class="btn btn-primary">SELECT</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="w-full container">
            <div class="widefatbox border border-solid border-gray-600 mx-auto flex">
                <div class="w-9/12">
                    <span class="span span-status text-base">Non-stop • 1hr 50mins</span><br>
                    <div class="w-full flex">
                        <div class="w-4/12">
                            <span class="span span-primary text-3xl">CGK 20:15</span><br>
                            <span class="span span-status">Jakarta</span><br>
                            <span class="text-gray-700 text-sm">12 SEP (Thu)</span><br>
                            <div id="hidden">
                                <span class="text-gray-700 text-sm">Soekarno Intl</span><br>
                                <span class="text-gray-700 text-sm">Terminal 3</span><br>
                            </div>
                        </div>
                        <div class="w-4/12">
                            <span class="span span-primary text-3xl">SIN 23:05</span><br>
                            <span class="span span-status">Singapore</span><br>
                            <span class="text-gray-700 text-sm">12 SEP (Thu)</span><br>
                            <div id="hidden">
                                <span class="text-gray-700 text-sm">Changi</span><br>
                                <span class="text-gray-700 text-sm">Terminal 3</span>
                            </div>
                        </div>
                        <div class="w-4/12">
                            <span class="span span-status text-base">Singapore Airlines • SQ 967</span><br>
                            <span onclick="hideviewdetail()" class="span span-bluelink"><i class="fas fa-angle-right"></i> View Detail</span><br>
                            <span id="hidden" class="text-gray-700 text-sm">Airbus 777-300</span><br>
                            <span id="hidden" class="span span-success text-sm">Economy</span><br>
                        </div>
                    </div>
                </div>
                <div class="w-3/12 bg-blue-300">
                    <div class="justify-center flex">ECONOMY</div>
                    <div class="justify-center flex mt-8 text-xs">FROM IDR</div>
                    <div class="justify-center flex text-2xl">3,543,900</div>
                    <div class="justify-center flex text-xs">PER ADULT</div>
                    <div onclick="hideinfo4()" class="justify-center flex text-2xl"><button><i class="fas fa-angle-down"></i></button></div>
                </div>
            </div>

            <div id="test4" class="container border-2 border-solid border-blue-400 hidden">
                <div class="w-full container bg-gray-200">
                    <div class="flex headtable h-10 items-center">
                        <div class="w-4/12 flex justify-center">FARE CONDITIONS</div>
                        <div class="w-4/12 flex justify-center">ECONOMY STANDARD</div>
                        <div class="w-4/12 flex justify-center">ECONOMY FLEXI</div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-4/12">
                            <div class="span span-primary text-base mt-3"><i class="fas fa-suitcase-rolling"></i> Baggage</div>
                            <div class="span span-primary text-base my-3">Seat selection at booking</div>
                        </div>
                        <div class="w-4/12">
                            <div class="justify-center flex text-base span span-primary font-bold mt-3">30kg</div>
                            <div class="justify-center flex text-base span span-success font-bold my-3">Complimentary (Standard Seats)</div>
                        </div>
                        <div class="w-4/12">
                            <div class="justify-center flex text-base font-bold span span-primary mt-3">35kg</div>
                            <div class="justify-center flex text-base font-bold span span-success my-3">Complimentary (Standard & Forward Zone Seats)</div>
                        </div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-4/12">
                            <span class="flex items-center span span-primary text-base mt-3"><i class="fas fa-gift mr-1"></i> Earn KrisFlyer miles</span>
                            <span class="flex items-center span span-primary text-base my-3"><i class="fas fa-arrow-circle-up mr-1"></i> Upgrade with miles</span>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center items-center flex span span-primary text-base font-bold mt-3">75% <i class="fas fa-info-circle ml-3"></i></span>
                            <span class="justify-center items-center flex span span-primary text-base font-bold my-3">Allowed <i class="fas fa-info-circle ml-3"></i></span>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center items-center flex span span-primary text-base font-bold mt-3">100% <i class="fas fa-info-circle ml-3"></i></span>
                            <span class="justify-center items-center flex span span-primary text-base font-bold my-3">Allowed <i class="fas fa-info-circle ml-3"></i></span>
                        </div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-4/12">
                            <span class="flex items-center span span-primary text-base mt-3"><i class="fas fa-window-close  mr-1"></i> Cancellation</span>
                            <span class="flex items-center span span-primary text-base my-3"><i class="fas fa-retweet mr-1"></i> Booking change</span>
                            <span class="flex items-center span span-primary text-base mb-3"><i class="fas fa-times-circle mr-1"></i> No show</span>
                            <div class="bg-blue-300 w-8/12 p-2 items-center flex ml-2 my-5">
                                <i class="fas fa-medal text-4xl"></i>
                                <span> View PPS Club / KrisFlyer privileges </span>
                            </div>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center flex span span-primary text-base font-bold mt-3">IDR 2,250,000</span>
                            <span class="justify-center flex span span-primary text-base font-bold my-3">IDR 300,000</span>
                            <span class="justify-center flex span span-primary text-base font-bold mb-3">IDR 1,500,000</span>
                            <span class="justify-center flex span span-primary text-2xl font-bold my-5">IDR 3,543,900</span>
                            <div class="flex justify-center">
                                <button class="btn btn-primary">SELECT</button>
                            </div>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center flex span span-primary text-base font-bold mt-3">IDR 750,000</span>
                            <span class="justify-center flex span span-success text-base font-bold my-3">Complimentary</span>
                            <span class="justify-center flex span span-primary text-base font-bold mb-3">IDR 1,500,000</span>
                            <span class="justify-center flex span span-primary text-2xl font-bold my-5">IDR 5,068,900</span>
                            <div class="flex justify-center">
                                <button class="btn btn-primary">SELECT</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="container w-11/12 mx-auto mt-5 p-5">
        <div>
            Lowest available fare<br>
            <span class="span span-primary text-2xl">2. Singapore to Jakarta</span>
        </div>
        <div class=" mt-5 flex items-center">
            <div class="w-4/12">
                <?php
                include(base_path . '/component/padding.php');
                ?>
            </div>

            <div class="w-8/12 flex items-center justify-end">
                <span class="mx-3">SORT BY: </span>
                <button class="mx-3">TRAVEL DURATION </button>
                <button class="mx-3">PRICE </button>
                <button class="mx-3">ARRIVAL TIME </button>
                <button class="mx-3">DEPARTURE TIME </button>
                <button class="ml-3">SQ FLIGHT </button>
            </div>

        </div>

        <div class="w-full container">
            <div class="widefatbox border border-solid border-gray-600 mx-auto flex">
                <div class="w-9/12">
                    <span class="span span-status text-base">Non-stop • 1hr 45mins</span><br>
                    <div class="w-full flex">
                        <div class="w-4/12">
                            <span class="span span-primary text-3xl">SIN 06:20</span><br>
                            <span class="span span-status">Singapore</span><br>
                            <span class="text-gray-700 text-sm">21 SEP (Sat)</span><br>
                            <div id="hidden">
                                <span class="text-gray-700 text-sm">Changi</span><br>
                                <span class="text-gray-700 text-sm">Terminal 2</span><br>
                            </div>
                        </div>
                        <div class="w-4/12">
                            <span class="span span-primary text-3xl">CGK 07:05</span><br>
                            <span class="span span-status">Jakarta</span><br>
                            <span class="text-gray-700 text-sm">21 SEP (Sat)</span><br>
                            <div id="hidden">
                                <span class="text-gray-700 text-sm">Soekarno Intl</span><br>
                                <span class="text-gray-700 text-sm">Terminal 3</span><br>
                            </div>
                        </div>
                        <div class="w-4/12">
                            <span class="span span-status text-base">Singapore Airlines • SQ 961</span><br>
                            <span onclick="hidedetail()" class="span span-bluelink"><i class="fas fa-angle-right"></i> View Detail</span><br>
                            <span id="hidden" class="text-gray-700 text-sm">Boeing 777-300</span><br>
                            <span id="hidden" class="span span-success text-sm">Economy</span><br>
                        </div>
                    </div>
                </div>
                <div class="w-3/12 bg-blue-300">
                    <div class="justify-center flex">ECONOMY</div>
                    <div class="justify-center flex mt-8 text-xs">FROM IDR</div>
                    <div class="justify-center flex text-2xl">+ 0</div>
                    <div class="justify-center flex text-xs">PER ADULT</div>
                    <div onclick="hideinfo2()" class="justify-center flex text-2xl"><button><i class="fas fa-angle-down"></i></button></div>
                </div>
            </div>

            <div id="test2" class="container border-2 border-solid border-blue-400 hidden">
                <div class="w-full container bg-gray-200">
                    <div class="flex headtable h-10 items-center">
                        <div class="w-3/12 flex justify-center">FARE CONDITIONS</div>
                        <div class="w-3/12 flex justify-center">ECONOMY LITE</div>
                        <div class="w-3/12 flex justify-center">ECONOMY STANDARD</div>
                        <div class="w-3/12 flex justify-center">ECONOMY FLEXI</div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-3/12">
                            <div class="span span-primary text-base mt-3"><i class="fas fa-suitcase-rolling"></i> Baggage</div>
                            <div class="span span-primary text-base my-3">Seat selection at booking</div>
                        </div>
                        <div class="w-3/12">
                            <div class="justify-center flex text-base span span-primary font-bold mt-3">30kg</div>
                            <div class="justify-center flex text-base span span-success font-bold my-3">From IDR 69,750</div>
                        </div>
                        <div class="w-3/12">
                            <div class="justify-center flex text-base span span-primary font-bold mt-3">30kg</div>
                            <div class="justify-center flex text-base span span-success font-bold my-3">Complimentary (Standard Seats)</div>
                        </div>
                        <div class="w-3/12">
                            <div class="justify-center flex text-base font-bold span span-primary mt-3">35kg</div>
                            <div class="justify-center flex text-base font-bold span span-success my-3">Complimentary (Standard & Forward Zone Seats)</div>
                        </div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-3/12">
                            <span class="flex items-center span span-primary text-base mt-3"><i class="fas fa-gift mr-1"></i> Earn KrisFlyer miles</span>
                            <span class="flex items-center span span-primary text-base my-3"><i class="fas fa-arrow-circle-up mr-1"></i> Upgrade with miles</span>
                        </div>
                        <div class="w-3/12">
                            <span class="justify-center items-center flex span span-primary text-base font-bold mt-3">50% <i class="fas fa-info-circle ml-3"></i></span>
                            <span class="justify-center items-center flex text-gray-600 text-base font-bold my-3">Not Allowed <i class="fas fa-info-circle ml-3"></i></span>
                        </div>
                        <div class="w-3/12">
                            <span class="justify-center items-center flex span span-primary text-base font-bold mt-3">75% <i class="fas fa-info-circle ml-3"></i></span>
                            <span class="justify-center items-center flex span span-primary text-base font-bold my-3">Allowed <i class="fas fa-info-circle ml-3"></i></span>
                        </div>
                        <div class="w-3/12">
                            <span class="justify-center items-center flex span span-primary text-base font-bold mt-3">100% <i class="fas fa-info-circle ml-3"></i></span>
                            <span class="justify-center items-center flex span span-primary text-base font-bold my-3">Allowed <i class="fas fa-info-circle ml-3"></i></span>
                        </div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-3/12">
                            <span class="flex items-center span span-primary text-base mt-3"><i class="fas fa-window-close  mr-1"></i> Cancellation</span>
                            <span class="flex items-center span span-primary text-base my-3"><i class="fas fa-retweet mr-1"></i> Booking change</span>
                            <span class="flex items-center span span-primary text-base mb-3"><i class="fas fa-times-circle mr-1"></i> No show</span>
                            <div class="bg-blue-300 w-8/12 p-2 items-center flex ml-2 my-5">
                                <i class="fas fa-medal text-4xl"></i>
                                <span> View PPS Club / KrisFlyer privileges </span>
                            </div>
                        </div>
                        <div class="w-3/12">
                            <span class="justify-center flex text-gray-600 text-base font-bold mt-3">Not Allowed</span>
                            <span class="justify-center flex span span-primary text-base font-bold my-3">IDR 750,000</span>
                            <span class="justify-center flex span span-primary text-base font-bold mb-3">IDR 1,500,000</span>
                            <span class="justify-center flex span span-primary text-2xl font-bold my-5">IDR + 0</span>
                            <div onclick="hidefare()" class="flex justify-center">
                                <button class="btn btn-primary">SELECT</button>
                            </div>
                        </div>
                        <div class="w-3/12">
                            <span class="justify-center flex span span-primary text-base font-bold mt-3">IDR 2,250,000</span>
                            <span class="justify-center flex span span-primary text-base font-bold my-3">IDR 300,000</span>
                            <span class="justify-center flex span span-primary text-base font-bold mb-3">IDR 1,500,000</span>
                            <span class="justify-center flex span span-primary text-2xl font-bold my-5">+IDR 2,208,000</span>
                            <div class="flex justify-center">
                                <button class="btn btn-primary">SELECT</button>
                            </div>
                        </div>
                        <div class="w-3/12">
                            <span class="justify-center flex span span-primary text-base font-bold mt-3">IDR 750,000</span>
                            <span class="justify-center flex span span-success text-base font-bold my-3">Complimentary</span>
                            <span class="justify-center flex span span-primary text-base font-bold mb-3">IDR 1,500,000</span>
                            <span class="justify-center flex span span-primary text-2xl font-bold my-5">+IDR 4,677,000</span>
                            <div class="flex justify-center">
                                <button class="btn btn-primary">SELECT</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="w-full container">
            <div class="widefatbox border border-solid border-gray-600 mx-auto flex">
                <div class="w-9/12">
                    <span class="span span-status text-base">Non-stop • 1hr 45mins</span><br>
                    <div class="w-full flex">
                        <div class="w-4/12">
                            <span class="span span-primary text-3xl">SIN 07:40</span><br>
                            <span class="span span-status">Singapore</span><br>
                            <span class="text-gray-700 text-sm">21 SEP (Sat)</span><br>
                            <div id="hidden">
                                <span class="text-gray-700 text-sm">Changi</span><br>
                                <span class="text-gray-700 text-sm">Terminal 2</span><br>
                            </div>
                        </div>
                        <div class="w-4/12">
                            <span class="span span-primary text-3xl">CGK 08:25</span><br>
                            <span class="span span-status">Jakarta</span><br>
                            <span class="text-gray-700 text-sm">21 SEP (Sat)</span><br>
                            <div id="hidden">
                                <span class="text-gray-700 text-sm">Soekarno Intl</span><br>
                                <span class="text-gray-700 text-sm">Terminal 3</span><br>
                            </div>
                        </div>
                        <div class="w-4/12">
                            <span class="span span-status text-base">Singapore Airlines • SQ 952</span><br>
                            <span onclick="hidedetail()" class="span span-bluelink"><i class="fas fa-angle-right"></i> View Detail</span><br>
                            <span id="hidden" class="text-gray-700 text-sm">Boeing 777-300</span><br>
                            <span id="hidden" class="span span-success text-sm">Economy</span><br>
                        </div>
                    </div>
                </div>
                <div class="w-3/12 bg-blue-300">
                    <div class="justify-center flex">ECONOMY</div>
                    <div class="justify-center flex mt-8 text-xs">FROM IDR</div>
                    <div class="justify-center flex text-2xl">+ 2,208,000</div>
                    <div class="justify-center flex text-xs">PER ADULT</div>
                    <div onclick="hideinfo3()" class="justify-center flex text-2xl"><button><i class="fas fa-angle-down"></i></button></div>
                </div>
            </div>

            <div id="test3" class="container border-2 border-solid border-blue-400 hidden">
                <div class="w-full container bg-gray-200">
                    <div class="flex headtable h-10 items-center">
                        <div class="w-4/12 flex justify-center">FARE CONDITIONS</div>
                        <div class="w-4/12 flex justify-center">ECONOMY STANDARD</div>
                        <div class="w-4/12 flex justify-center">ECONOMY FLEXI</div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-4/12">
                            <div class="span span-primary text-base mt-3"><i class="fas fa-suitcase-rolling"></i> Baggage</div>
                            <div class="span span-primary text-base my-3">Seat selection at booking</div>
                        </div>
                        <div class="w-4/12">
                            <div class="justify-center flex text-base span span-primary font-bold mt-3">30kg</div>
                            <div class="justify-center flex text-base span span-success font-bold my-3">Complimentary (Standard Seats)</div>
                        </div>
                        <div class="w-4/12">
                            <div class="justify-center flex text-base font-bold span span-primary mt-3">35kg</div>
                            <div class="justify-center flex text-base font-bold span span-success my-3">Complimentary (Standard & Forward Zone Seats)</div>
                        </div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-4/12">
                            <span class="flex items-center span span-primary text-base mt-3"><i class="fas fa-gift mr-1"></i> Earn KrisFlyer miles</span>
                            <span class="flex items-center span span-primary text-base my-3"><i class="fas fa-arrow-circle-up mr-1"></i> Upgrade with miles</span>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center items-center flex span span-primary text-base font-bold mt-3">75% <i class="fas fa-info-circle ml-3"></i></span>
                            <span class="justify-center items-center flex span span-primary text-base font-bold my-3">Allowed <i class="fas fa-info-circle ml-3"></i></span>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center items-center flex span span-primary text-base font-bold mt-3">100% <i class="fas fa-info-circle ml-3"></i></span>
                            <span class="justify-center items-center flex span span-primary text-base font-bold my-3">Allowed <i class="fas fa-info-circle ml-3"></i></span>
                        </div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-4/12">
                            <span class="flex items-center span span-primary text-base mt-3"><i class="fas fa-window-close  mr-1"></i> Cancellation</span>
                            <span class="flex items-center span span-primary text-base my-3"><i class="fas fa-retweet mr-1"></i> Booking change</span>
                            <span class="flex items-center span span-primary text-base mb-3"><i class="fas fa-times-circle mr-1"></i> No show</span>
                            <div class="bg-blue-300 w-8/12 p-2 items-center flex ml-2 my-5">
                                <i class="fas fa-medal text-4xl"></i>
                                <span> View PPS Club / KrisFlyer privileges </span>
                            </div>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center flex span span-primary text-base font-bold mt-3">IDR 2,250,000</span>
                            <span class="justify-center flex span span-primary text-base font-bold my-3">IDR 300,000</span>
                            <span class="justify-center flex span span-primary text-base font-bold mb-3">IDR 1,500,000</span>
                            <span class="justify-center flex span span-primary text-2xl font-bold my-5">+IDR 2,208,000</span>
                            <div class="flex justify-center">
                                <button class="btn btn-primary">SELECT</button>
                            </div>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center flex span span-primary text-base font-bold mt-3">IDR 750,000</span>
                            <span class="justify-center flex span span-success text-base font-bold my-3">Complimentary</span>
                            <span class="justify-center flex span span-primary text-base font-bold mb-3">IDR 1,500,000</span>
                            <span class="justify-center flex span span-primary text-2xl font-bold my-5">+IDR 4,677,000</span>
                            <div class="flex justify-center">
                                <button class="btn btn-primary">SELECT</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div id="hiddenfare" class="container w-11/12 mx-auto  mt-3 p-5 hidden">
        <div class="widefatbox bg-gray-100 shadow">
            <span class="span span-primary text-2xl">Here's a summary of fare conditions</span><br>
            <span>For more details on Travel Itinerary Sequence and No Show, please refer to the full fare conditions.</span>

            <div id="" class="container border-2 border-solid border-blue-400 mt-3">
                <div class="w-full container bg-gray-200">
                    <div class="flex headtable h-12 items-center">
                        <div class="w-4/12">FARE CONDITIONS</div>
                        <div class="w-4/12">
                            <div class="w-full justify-center flex">CGK - SIN</div>
                            <div class="w-full justify-center flex">Economy Flexi</div>
                        </div>
                        <div class="w-4/12">
                            <div class="w-full justify-center flex">SIN - CGK</div>
                            <div class="w-full justify-center flex">Economy Lite</div>
                        </div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-4/12">
                            <div class="span span-primary text-lg mt-3"><i class="fas fa-suitcase-rolling"></i> Baggage</div>
                            <div class="span span-primary text-lg my-3">Seat selection at booking</div>
                        </div>
                        <div class="w-4/12">
                            <div class="justify-center flex text-lg span span-primary font-bold mt-3">35kg</div>
                            <div class="justify-center flex text-lg span span-success font-bold my-3">Complimentary (Standard & Forward Zone Seats)</div>
                        </div>
                        <div class="w-4/12">
                            <div class="justify-center flex text-lg span span-primary font-bold mt-3">30kg</div>
                            <div class="justify-center flex text-lg span span-primary font-bold my-3">From IDR 70,500</div>
                        </div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-4/12">
                            <span class="flex items-center span span-primary text-lg mt-3"><i class="fas fa-gift mr-1"></i> Earn KrisFlyer miles</span>
                            <span class="flex items-center span span-primary text-lg my-3"><i class="fas fa-arrow-circle-up mr-1"></i> Upgrade with miles</span>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center items-center flex span span-primary text-lg font-bold mt-3">100% </span>
                            <span class="justify-center items-center flex span span-primary text-lg font-bold my-3">Allowed </span>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center items-center flex span span-primary text-lg font-bold mt-3">50% </span>
                            <span class="justify-center items-center flex text-gray-500 text-lg font-bold my-3">Not Allowed </span>
                        </div>
                    </div>

                    <div class="flex rowtable">
                        <div class="w-4/12">
                            <span class="flex items-center span span-primary text-lg mt-3"><i class="fas fa-window-close  mr-1"></i> Cancellation</span>
                            <span class="flex items-center span span-primary text-lg my-3"><i class="fas fa-retweet mr-1"></i> Booking change</span>
                            <span class="flex items-center span span-primary text-lg mb-3"><i class="fas fa-times-circle mr-1"></i> No show</span>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center flex span span-primary text-lg font-bold mt-3">Not Allowed</span>
                            <span class="justify-center flex span span-primary text-lg font-bold mt-3">IDR 750,000</span>
                            <span class="justify-center flex span span-primary text-lg font-bold mt-3">IDR 1,500,000</span>
                        </div>
                        <div class="w-4/12">
                            <span class="justify-center flex span span-primary text-lg font-bold mt-3">Not Allowed</span>
                            <span class="justify-center flex span span-primary text-lg font-bold mt-3">IDR 750,000</span>
                            <span class="justify-center flex span span-primary text-lg font-bold mt-3">IDR 1,500,000</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-6 pl-4"> * Each of the flight segments you've selected comes with its own fare conditions.
                When you mix fare types, whether within the same cabin class or across cabin classes,
                fare conditions for cancellation, booking change and no show will follow the more restrictive fare type.</div>
            <div class="mt-2">Fees shown here are indicative. As the original fees are based in US dollars,
                the actual fees may vary due to exchange rate fluctuations.
                The original fees can be found on the ticket of a confirmed booking.</div>
            <div class="span span-bluelink text-base mt-5"><i class="fas fa-angle-right"></i> Full fare rules and conditions</div>
            <div class="span span-status text-lg mt-5">Fares are not guaranteed until payment is completed</div>
        </div>
    </div>

    <?php
    include(base_path . "/component/footer.php");
    ?>

    <script>
        function hideinfo() {
            var x = document.getElementById("test");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }

        function hideinfo2() {
            var y = document.getElementById("test2");
            if (y.style.display === "none") {
                y.style.display = "block";
            } else {
                y.style.display = "none";
            }
        }

        function hideinfo3() {
            var z = document.getElementById("test3");
            if (z.style.display === "none") {
                z.style.display = "block";
            } else {
                z.style.display = "none";
            }
        }

        function hideinfo4() {
            var v = document.getElementById("test4");
            if (v.style.display === "none") {
                v.style.display = "block";
            } else {
                v.style.display = "none";
            }
        }

        function hideviewdetail() {
            var w = document.getElementById("hidden");
            if (w.style.display === "none") {
                w.style.display = "block";
            } else {
                w.style.display = "none";
            }
        }

        function hidefare() {
            var v = document.getElementById("hiddenfare");
            if (v.style.display === "none") {
                v.style.display = "block";
            } else {
                v.style.display = "none";
            }
        }
    </script>
</body>

</html>