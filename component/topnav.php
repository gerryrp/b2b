<nav class="topnav">
    <!-- <div id="logo">Logo</div> -->

    <label for="drop" class="toggle">Menu ☰</label>
    <input type="checkbox" id="drop" />

    <ul class="menu">
        <li><a href="<?= root_path ?>/pages/home.php">Home</a></li>
        <li>
            <label for="drop-1" class="toggle">Archives <i class="fas fa-angle-down ml-1 my-auto"></i></label>
            <a href="#">Archives <i class="fas fa-angle-down ml-1 my-auto"></i></a>
            <input type="checkbox" id="drop-1" />
            <ul>
                <li><a href="<?= root_path ?>/pages/reservation/reservation.php">Reservation List</a></li>
                <li><a href="<?= root_path ?>/pages/ireservation/intreservation.php">Int Reservation List</a></li>
                <li><a href="<?= root_path ?>/pages/alreservation/alreservation.php">Airline Reservation List</a></li>
                <li><a href="<?= root_path ?>/pages/invoice/invoice.php">Invoice List</a></li>
                <li><a href="<?= root_path ?>/pages/intinvoice/intinvoice.php">Int. Htl. List</a></li>
                <li><a href="<?= root_path ?>/pages/alinvoice/alinvoice.php">Airline Invoice List</a></li>
                <li><a href="<?= root_path ?>/pages/credit/creditlimit.php">Credit Limit List</a></li>
                <li><a href="<?= root_path ?>/pages/topup/transfer.php">Top Up List</a></li>
                <li><a href="<?= root_path ?>/pages/session/userSession_list.php">User Session List</a></li>
                <li><a href="<?= root_path ?>/pages/signup/signup_list.php">Sign Up List</a></li>
                <li><a href="#">Search Flight</a></li>
            </ul>
        </li>
        <li>
            <label for="drop-2" class="toggle">Report <i class="fas fa-angle-down ml-1 my-auto"></i></label>
            <a href="#">Report <i class="fas fa-angle-down ml-1 my-auto"></i></a>
            <input type="checkbox" id="drop-2" />
            <ul>
                <li><a href="<?= root_path ?>/pages/reporth/reservation_report.php">Haryono Sales</a></li>
                <li><a href="#">Operation Production </a></li>
                <li>
                    <label for="drop-3" class="toggle">Customer Production <i class="fas fa-angle-down ml-1 my-auto"></i></label>
                    <a href="#">Customer Production <i class="fas fa-angle-right ml-1 my-auto"></i></a>
                    <input type="checkbox" id="drop-3" />
                    <ul>
                        <li><a href="#">Booker Incentive</a></li>
                    </ul>
                </li>
                <li>
                    <label for="drop-4" class="toggle">Hotel Production <i class="fas fa-angle-down ml-1 my-auto"></i></label>
                    <a href="#">Hotel Production <i class="fas fa-angle-right ml-1 my-auto"></i></a>
                    <input type="checkbox" id="drop-4" />
                    <ul>
                        <li><a href="#">Hotel Check-in</a></li>
                        <li><a href="#">Hotel Allotment</a></li>
                    </ul>
                </li>
                <li><a href="<?= root_path ?>/pages/bincentive/bincentive_list.php">Incentives Claim</a></li>
                <li>
                    <label for="drop-5" class="toggle">Analytics <i class="fas fa-angle-down ml-1 my-auto"></i></label>
                    <a href="#">Analytics <i class="fas fa-angle-right ml-1 my-auto"></i></a>
                    <input type="checkbox" id="drop-5" />
                    <ul>
                        <li><a href="#">Issued</a></li>
                        <li><a href="#">Confirmation Speed</a></li>
                        <li><a href="#">Activity</a></li>
                        <li><a href="#">Security</a></li>
                        <li><a href="#">User Booking Statistic</a></li>
                        <li><a href="#">Customers Sales</a></li>
                        <li><a href="#">Hotel Sales </a></li>
                        <li><a href="#">All Sales</a></li>
                    </ul>
                </li>
                <li>
                    <label for="drop-6" class="toggle">Log <i class="fas fa-angle-down ml-1 my-auto"></i></label>
                    <a href="#">Log <i class="fas fa-angle-right ml-1 my-auto"></i></a>
                    <input type="checkbox" id="drop-6" />
                    <ul>
                        <li><a href="<?= root_path ?>/pages/log/emaillog_list.php">Email Log</a></li>
                        <li><a href="<?= root_path ?>/pages/log/sessionlog_list.php">Session Log</a></li>
                        <li><a href="<?= root_path ?>/pages/log/allotmentlog_list.php">Allotment Log</a></li>
                        <li><a href="<?= root_path ?>/pages/log/hotelratelog_list.php">Hotel Rate Log</a></li>
                        <li><a href="<?= root_path ?>/pages/log/creditlog_list.php">Credit Limit Log</a></li>
                        <li><a href="<?= root_path ?>/pages/log/depositlog_list.php">Deposit Log</a></li>
                        <li><a href="<?= root_path ?>/pages/log/publiclinklog_list.php">Public Link Log</a></li>
                        <li><a href="<?= root_path ?>/pages/log/mandrillmanager_list.php">Mandrill</a></li>
                    </ul>
                </li>
                <li><a href="<?= root_path ?>/pages/pricelist/price_list.php">Price List</a></li>
            </ul>
        </li>
        <li>
            <label for="drop-7" class="toggle">Master <i class="fas fa-angle-down ml-1 my-auto"></i></label>
            <a href="#">Master <i class="fas fa-angle-down ml-1 my-auto"></i></a>
            <input type="checkbox" id="drop-7" />
            <ul>
                <li>
                    <label for="drop-8" class="toggle">Hotel <i class="fas fa-angle-down ml-1 my-auto"></i></label>
                    <a href="<?= root_path ?>/pages/hotel/hotel.php">Hotel <i class="fas fa-angle-right ml-1 my-auto"></i></a>
                    <input type="checkbox" id="drop-8" />
                    <ul>
                        <li><a href="<?= root_path ?>/pages/hotel/hotelGroup.php">Hotel Group</a></li>
                        <li><a href="<?= root_path ?>/pages/hotel/roomType.php">Room Type</a></li>
                        <li><a href="<?= root_path ?>/pages/hotel/roomFacility.php">Room Facility</a></li>
                    </ul>
                </li>
                <li>
                    <label for="drop-9" class="toggle">Rate <i class="fas fa-angle-down ml-1 my-auto"></i></label>
                    <a href="<?= root_path ?>/pages/rate/hotelRate.php">Rate <i class="fas fa-angle-right ml-1 my-auto"></i></a>
                    <input type="checkbox" id="drop-9" />
                    <ul>
                        <li><a href="<?= root_path ?>/pages/rate/rateType.php">Rate Type</a></li>
                    </ul>
                </li>
                <li>
                    <label for="drop-10" class="toggle">Allotment <i class="fas fa-angle-down ml-1 my-auto"></i></label>
                    <a href="<?= root_path ?>/pages/allotment/hotelAllotment.php">Allotment <i class="fas fa-angle-right ml-1 my-auto"></i></a>
                    <input type="checkbox" id="drop-10" />
                    <ul>
                        <li><a href="<?= root_path ?>/pages/allotment/roomTypeGroup.php">Rate Type Group</a></li>
                    </ul>
                </li>
                <li><a href="<?= root_path ?>/pages/customers/customer.php">Customers</a></li>
                <li><a href="<?= root_path ?>/pages/users/user.php">User</a></li>
                <li><a href="<?= root_path ?>/pages/apiusers/apiuser.php">API User</a></li>
                <li><a href="<?= root_path ?>/pages/branch/branch.php">Branch</a></li>
                <li>
                    <label for="drop-11" class="toggle">Location <i class="fas fa-angle-down ml-1 my-auto"></i></label>
                    <a href="<?= root_path ?>/pages/location/city.php">Location <i class="fas fa-angle-right ml-1 my-auto"></i></a>
                    <input type="checkbox" id="drop-11" />
                    <ul>
                        <li><a href="<?= root_path ?>/pages/location/city.php">City</a></li>
                        <li><a href="<?= root_path ?>/pages/location/state.php">State</a></li>
                        <li><a href="<?= root_path ?>/pages/location/country.php">Country</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>
            <label for="drop-12" class="toggle">Setting <i class="fas fa-angle-down ml-1 my-auto"></i></label>
            <a href="#">Setting <i class="fas fa-angle-down ml-1 my-auto"></i></a>
            <input type="checkbox" id="drop-12" />
            <ul>
                <li><a href="<?= root_path ?>/pages/setting/system.php">System</a></li>
                <li><a href="<?= root_path ?>/pages/setting/arlsetting.php">Airlines System</a></li>
                <li><a href="<?= root_path ?>/pages/setting/intsetting.php">Int Hotel</a></li>
                <li><a href="<?= root_path ?>/pages/setting/imarksetting.php">Int Htl Mark Up</a></li>
                <li><a href="<?= root_path ?>/pages/setting/apisetting.php">API</a></li>
                <li><a href="<?= root_path ?>/pages/setting/role.php">User Role</a></li>
                <li>
                    <label for="drop-13" class="toggle">Reservation <i class="fas fa-angle-down ml-1 my-auto"></i></label>
                    <a href="<?= root_path ?>/pages/setting/emailtemplate.php">Reservation <i class="fas fa-angle-right ml-1 my-auto"></i></a>
                    <input type="checkbox" id="drop-13" />
                    <ul>
                        <li><a href="<?= root_path ?>/pages/setting/emailtemplate.php">Email Template</a></li>
                        <li><a href="<?= root_path ?>/pages/setting/bookingstatus.php">Booking Status</a></li>
                    </ul>
                </li>
                <li>
                    <label for="drop-14" class="toggle">News <i class="fas fa-angle-down ml-1 my-auto"></i></label>
                    <a href="<?= root_path ?>/pages/setting/news.php">News <i class="fas fa-angle-right ml-1 my-auto"></i></a>
                    <input type="checkbox" id="drop-14" />
                    <ul>
                        <li><a href="<?= root_path ?>/pages/setting/frontpage.php">Front Page</a></li>
                    </ul>
                </li>
                <li>
                    <label for="drop-15" class="toggle">Menu <i class="fas fa-angle-down ml-1 my-auto"></i></label>
                    <a href="<?= root_path ?>/pages/menu/menu_list.php">Menu <i class="fas fa-angle-right ml-1 my-auto"></i></a>
                    <input type="checkbox" id="drop-15" />
                    <ul>
                        <li><a href="<?= root_path ?>/pages/page/page_list.php">Page</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="xl:mr-64 lg:mr-32">
            <label for="drop-" class="toggle">Profile <i class="fas fa-angle-down ml-1 my-auto"></i></label>
            <a href="#">Profile <i class="fas fa-angle-down ml-1 my-auto"></i></a>
            <input type="checkbox" id="drop-" />
            <ul>
                <li><a href="#">Edit Profile</a></li>
                <li><a href="#">Switch User Role</a></li>
                <li><a href="#">Log In As(Do Not Book)</a></li>
            </ul>
        </li>
        <li class="xl:ml-64 lg:ml-32"><a href="<?= root_path ?>/pages/login.php">Logout</a></li>
    </ul>
</nav>