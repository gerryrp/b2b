<html>

<body>
    <div class="container w-full p-1 shadow-md">

        <div class="container w-full">

            <div class="flex">
                <div class="tab-container w-3/12">
                    <div class="tab">
                        <input type="text" class="input mb-2" placeholder="Registered Cust">
                        <button class="tablinks mb-1" onclick="openCity(event, 'Dom')" id="defaultOpen"> <i class="fas fa-hotel"></i> Domestic</button>
                        <button class="tablinks mb-1" onclick="openCity(event, 'Inter')"><i class="fas fa-hotel"></i> International</button>
                        <button class="tablinks mb-1" onclick="openCity(event, 'Oneway')"><i class="fas fa-plane"></i> Airlines One Way</button>
                        <button class="tablinks mb-1" onclick="openCity(event, 'Roundtrip')"><i class="fas fa-plane"></i> Airlines Round Trip</button>
                    </div>
                </div>

                <div class="w-full ">

                    <!-- dom content -->
                    <div class="container max-w-full">

                        <div id="Dom" class="tabcontent ">

                            <div class="flex  mt-1">

                                <div class="w-1/3">

                                    <div class="text-small text-center">
                                        Hotel or city name
                                    </div>

                                    <div class="flex justify-center items-center">
                                        <input type="text" class="input  " placeholder="hotel or city name">
                                        <button type="submit" class="fa fa-search ml-1" style="background-color: rgba(255, 255, 255, 0);border-color: rgba(255, 255, 255, 0)"></button>
                                    </div>

                                </div>

                                <div class="w-1/3 ">

                                    <div class="text-small text-center">
                                        Check-in
                                    </div>

                                    <div class="flex justify-center items-center ">
                                        <input id="datepickr2" type="text" class="flatpickr flatpickr-input input  " placeholder="select date">
                                        &nbsp <i class="far fa-calendar-alt"></i>
                                    </div>

                                </div>

                                <div class="w-1/3 ">

                                    <div class="text-small text-center">
                                        Check-out
                                    </div>

                                    <div class="flex justify-center items-center">
                                        <input id="datepickr3" type="text" class="flatpickr flatpickr-input input  " placeholder="select date">
                                        &nbsp <i class="far fa-calendar-alt"></i>
                                    </div>

                                </div>

                            </div>

                            <div class="flex justify-center items-center">

                                <div class="w-4/12  p-3">

                                    <div class="flex justify-center collapsible1 ">
                                        <button class="span span-blue1 ">
                                            Advance Search</button><i class="fas fa-angle-down ml-1 my-auto"></i>
                                    </div>

                                    <div class="collapsecontent">

                                        <div class="flex items-center">
                                            <div>
                                                <span class=>Star</span>
                                            </div>
                                            <div class="ml-3">
                                                <form>
                                                    <select class="inputselect inputselect-sm">
                                                        <option value="All">All</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select>
                                                </form>
                                            </div>
                                        </div>

                                        <div>
                                            <div class="flex mb-1">
                                                <div class="mr-1">
                                                    Price <input type="text" class=" input  ">
                                                </div>

                                                <div class="flex items-center">
                                                    <input type="checkbox" class="check" id="dropdownCheck1" data-label="to">
                                                    <label class="span span-blue1" for="dropdownCheck1"> to </label>
                                                </div>
                                            </div>

                                            <div>
                                                Price <input type="text" class=" input  ">
                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <div class="w-4/12 flex justify-center  items-center  ">
                                    <button onclick="window.location.href = '<?= root_path ?>/pages/bhotel/menu_hotel_dom.php';" type="button" class="btn btn-sm btn-rounded btn-light mr-5 " value="Sign Up"><i class="fas fa-search"></i> Search</button>

                                    <button type="button" class="btn btn-sm btn-rounded btn-primary " value="Sign Up"><i class="fas fa-sync-alt"></i> Reset</button>
                                </div>

                            </div>

                        </div>

                    </div>

                    <!-- inter content -->
                    <div class="container max-w-full">

                        <div id="Inter" class="tabcontent">

                            <div class="w-full flex mt-1">

                                <div class="w-3/12 mr-1">
                                    <div class="justify-center flex">
                                        Hotel or City Name
                                    </div>
                                    <div class="items-center justify-center flex">
                                        <div class="items-center flex">
                                            <i class="fas fa-bed "></i>
                                        </div>
                                        <input type="text" class="input w-48 mx-1" placeholder="Enter City or Hotel Name">
                                        <div class="items-center flex">
                                            <i class="fas fa-map-marker-alt "></i>
                                        </div>
                                    </div>
                                </div>

                                <div class="w-4/12 mx-2">
                                    <div class="justify-center flex">
                                        <span>Check in-out</span>
                                        <div class="items-center flex ml-2">
                                            <i class="fas fa-calendar-alt "></i>
                                        </div>
                                    </div>
                                    <div class="flex justify-center">
                                        <input id="datepickr4" type="text" class="flatpickr flatpickr-input input w-48 mx-1" placeholder="Check-in">
                                        <input id="datepickr5" type="text" class="flatpickr flatpickr-input input w-48" placeholder="Check-out">
                                    </div>
                                </div>

                                <div class="w-4/12  ">
                                    <div class="flex justify-center">
                                        <span>Guest & Rooms</span>
                                    </div>
                                    <div class="flex items-center justify-center border border-solid border-gray-600 rounded">
                                        <i class="fas fa-user-plus "></i>
                                        <span class=" mx-1">6 Rooms (16 Adults, 2 Children)</span>
                                        <i onclick="hiddRoom()" class="fas fa-angle-down"></i>
                                    </div>

                                    <div id="rooms" class="bg-blue-100 absolute z-10 border border-solid border-gray-500 p-1 hidden" style="width:314px;">
                                        <div class="flex">
                                            <span class="w-4/12">No. of Rooms</span>
                                            <form>
                                                <select class="inputselect rounded w-46">
                                                    <option value="1 Rooms">1 Rooms</option>
                                                    <option value="2 Rooms">2 Rooms</option>
                                                    <option value="3 Rooms">3 Rooms</option>
                                                    <option value="4 Rooms">4 Rooms</option>
                                                    <option value="5 Rooms">5 Rooms</option>
                                                    <option value="6 Rooms" selected>6 Rooms</option>
                                                </select>
                                            </form>
                                        </div>
                                        <div class="flex">
                                            <span class="w-4/12">Rooms 1</span>
                                            <form>
                                                <select class="inputselect rounded w-24">
                                                    <option value="1 Adult">1 Adult</option>
                                                    <option value="2 Adult" selected>2 Adult</option>
                                                </select>
                                            </form>
                                            <form>
                                                <select class="inputselect rounded w-24">
                                                    <option value="1">1</option>
                                                    <option value="2" selected>2</option>
                                                </select>
                                            </form>
                                        </div>
                                        <div class="flex">
                                            <span class="w-4/12"></span>
                                            <span class="w-24">Child 1</span>
                                            <form>
                                                <select class="inputselect rounded w-24">
                                                    <option value="Age ?">Age ?</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </form>
                                        </div>
                                        <div class="flex">
                                            <span class="w-4/12"></span>
                                            <span class="w-24">Child 2</span>
                                            <form>
                                                <select class="inputselect rounded w-24">
                                                    <option value="Age ?">Age ?</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                </select>
                                            </form>
                                        </div>
                                        <div class="flex">
                                            <span class="w-4/12">Rooms 2</span>
                                            <form>
                                                <select class="inputselect rounded w-24">
                                                    <option value="1 Adult">1 Adult</option>
                                                    <option value="2 Adult">2 Adult</option>
                                                </select>
                                            </form>
                                            <form>
                                                <select class="inputselect rounded w-24">
                                                    <option value="Children">Children</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                </select>
                                            </form>
                                        </div>
                                        <div class="flex">
                                            <span class="w-4/12">Rooms 3</span>
                                            <form>
                                                <select class="inputselect rounded w-24">
                                                    <option value="1 Adult">1 Adult</option>
                                                    <option value="2 Adult">2 Adult</option>
                                                </select>
                                            </form>
                                            <form>
                                                <select class="inputselect rounded w-24">
                                                    <option value="Children">Children</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                </select>
                                            </form>
                                        </div>
                                        <div class="flex">
                                            <span class="w-4/12">Rooms 4</span>
                                            <form>
                                                <select class="inputselect rounded w-24">
                                                    <option value="1 Adult">1 Adult</option>
                                                    <option value="2 Adult">2 Adult</option>
                                                </select>
                                            </form>
                                            <form>
                                                <select class="inputselect rounded w-24">
                                                    <option value="Children">Children</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                </select>
                                            </form>
                                        </div>
                                        <div class="flex">
                                            <span class="w-4/12">Rooms 5</span>
                                            <form>
                                                <select class="inputselect rounded w-24">
                                                    <option value="1 Adult">1 Adult</option>
                                                    <option value="2 Adult">2 Adult</option>
                                                </select>
                                            </form>
                                            <form>
                                                <select class="inputselect rounded w-24">
                                                    <option value="Children">Children</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                </select>
                                            </form>
                                        </div>
                                        <div class="flex">
                                            <span class="w-4/12">Rooms 6</span>
                                            <form>
                                                <select class="inputselect rounded w-24">
                                                    <option value="1 Adult">1 Adult</option>
                                                    <option value="2 Adult">2 Adult</option>
                                                </select>
                                            </form>
                                            <form>
                                                <select class="inputselect rounded w-24">
                                                    <option value="Children">Children</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                </select>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="w-1/12 flex items-center">
                                    <button onclick="window.location.href = '<?= root_path ?>/pages/bihotel/menu_hotel_inter.php';" class="btn btn-rounded btn-light"><i class="fas fa-search"></i>Search</button>
                                </div>

                            </div>

                            <div class="w-full flex items-center mt-5 relative">

                                <div class="w-3/12">
                                    <span class="text-gray-600 text-xs">Nationality</span>
                                    <div class="flex justify-center">
                                        <i class="fas fa-flag mr-1"></i>
                                        <form>
                                            <select class="inputselect rounded">
                                                <option value="Indonesia">Indonesia</option>
                                                <option value="Malaysia">Malaysia</option>
                                            </select>
                                        </form>
                                    </div>
                                </div>

                                <div class="w-3/12">
                                    <span class="text-gray-600 text-xs">Star Rating</span>
                                    <div class="flex justify-center">
                                        <i class="fas fa-star mr-1"></i>
                                        <form>
                                            <select class="inputselect rounded">
                                                <option value="1 Star">1 Star</option>
                                                <option value="2 Star">2 Star</option>
                                                <option value="3 Star">3 Star</option>
                                                <option value="4 Star">4 Star</option>
                                                <option value="4 Star or More">4 Star or More</option>
                                            </select>
                                        </form>
                                    </div>
                                </div>

                                <div class="w-3/12">
                                    <span class="text-gray-600 text-xs">Budget</span>
                                    <div class="flex items-center ml-3">
                                        <i class="fas fa-suitcase mr-1"></i>
                                        <input class="input" style="width:165px" placeholder="Budget (IDR)" />
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                    <!-- airline one way content -->
                    <div class="container max-w-full">

                        <div id="Oneway" class="tabcontent">

                            <div class="container mt-1">
                                <div class="w-full mx-auto">

                                    <!-- <div class="text-center">
                                &nbsp
                                </div> -->

                                    <div class="container">
                                        <div class="flex">

                                            <!--                                             
                                        <div class="mx-auto">
                                            <div class="justify-center flex">
                                                <input class="items-center flex checked "  id="option1 " type="radio" name="field" value="option1" checked>
                                                <label for="option1" class="items-center flex "><span><span></span></span>One Way Trip</label>
                                            </div>                                      
                                        </div>
                                                
                                        <div onclick="myFunction()" class="mx-auto">
                                            <div class="justify-center flex">
                                                <input class="items-center flex"  id="option2 " type="radio" name="field" value="option2">
                                                <label for="option2 " class="items-center flex"><span><span></span></span>Round Trip</label>
                                            </div>                                                                             
                                        </div> -->

                                        </div>


                                        <!-- one way content -->
                                        <div>

                                            <div class="flex justify-center  mt-2">

                                                <div class="w-3/12">

                                                    <div class="text-small text-center">
                                                        Departure date:
                                                    </div>

                                                    <div class="flex justify-center items-center">
                                                        <input id="datepickr6" type="text" class="flatpickr flatpickr-input input " placeholder="select date">
                                                        &nbsp <i class="far fa-calendar-alt"></i>
                                                    </div>

                                                </div>

                                                <div class="w-3/12">

                                                    <div class="text-small text-center">
                                                        From
                                                    </div>

                                                    <div class="flex justify-center items-center">
                                                        <input type="text" class="input " placeholder="Origin">
                                                        <button type="submit" class="fa fa-search ml-1" style="background-color: rgba(255, 255, 255, 0);border-color: rgba(255, 255, 255, 0)"></button>
                                                    </div>

                                                </div>

                                                <div class="w-3/12">

                                                    <div class="text-small text-center">
                                                        To
                                                    </div>

                                                    <div class="flex justify-center items-center">
                                                        <input type="text" class="input input-sm " placeholder="Destination">
                                                        <button type="submit" class="fa fa-search ml-1" style="background-color: rgba(255, 255, 255, 0);border-color: rgba(255, 255, 255, 0)"></button>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="flex justify-center items-center">

                                                <div class="p-4">
                                                    <span>
                                                        Adult:
                                                    </span>

                                                    <div class="justify-center flex">

                                                        <form>
                                                            <select class="inputselect inputselect-sm">
                                                                <option value="">1</option>
                                                                <option value="">2</option>
                                                                <option value="">3</option>
                                                                <option value="">4</option>
                                                                <option value="">5</option>
                                                            </select>
                                                        </form>

                                                    </div>

                                                </div>

                                                <div class="p-4">
                                                    <span>
                                                        Children:
                                                    </span>

                                                    <div class="justify-center flex">

                                                        <form>
                                                            <select class="inputselect inputselect-sm">
                                                                <option value="">0</option>
                                                                <option value="">1</option>
                                                                <option value="">2</option>
                                                                <option value="">3</option>
                                                                <option value="">4</option>
                                                                <option value="">5</option>
                                                            </select>
                                                        </form>

                                                    </div>

                                                </div>


                                                <div class="p-4">
                                                    <span>
                                                        Infant
                                                    </span>

                                                    <div class="justify-center flex">

                                                        <form>
                                                            <select class="inputselect inputselect-sm">
                                                                <option value="">0</option>
                                                                <option value="">1</option>
                                                                <option value="">2</option>
                                                                <option value="">3</option>
                                                                <option value="">4</option>
                                                                <option value="">5</option>
                                                            </select>
                                                        </form>

                                                    </div>

                                                </div>


                                                <div class="w-1/5 flex justify-center  items-center  ">
                                                    <button onclick="window.location.href = '<?= root_path ?>/pages/bairline/menu_airlines.php';" type="button" class="btn btn-sm btn-rounded btn-light mr-5 " value="Sign Up"><i class="fas fa-search"></i> Search</button>

                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>



                        </div>

                    </div>

                    <!-- airline round trip content -->
                    <div class="container max-w-full">

                        <div id="Roundtrip" class="tabcontent">

                            <div class="container mt-1">
                                <div class="w-full mx-auto">

                                    <!-- <div class="text-center">
                                &nbsp
                                </div> -->

                                    <div class="container">

                                        <!-- roundtrip content -->
                                        <div>

                                            <div class="flex justify-center  mt-2">

                                                <div class="w-3/12">

                                                    <div class="text-small text-center">
                                                        Departure date:
                                                    </div>

                                                    <div class="flex justify-center items-center">
                                                        <input id="datepickr8" type="text" class="flatpickr flatpickr-input input " placeholder="select date">
                                                        &nbsp <i class="far fa-calendar-alt"></i>
                                                    </div>

                                                </div>

                                                <div class="w-3/12">

                                                    <div class="text-small text-center">
                                                        Return date:
                                                    </div>

                                                    <div class="flex justify-center items-center">
                                                        <input id="datepickr7" type="text" class="flatpickr flatpickr-input input " placeholder="select date">
                                                        &nbsp <i class="far fa-calendar-alt"></i>
                                                    </div>

                                                </div>

                                                <div class="w-3/12">

                                                    <div class="text-small text-center">
                                                        From
                                                    </div>

                                                    <div class="flex justify-center items-center">
                                                        <input type="text" class="input " placeholder="Origin">
                                                        <button type="submit" class="fa fa-search ml-1" style="background-color: rgba(255, 255, 255, 0);border-color: rgba(255, 255, 255, 0)"></button>
                                                    </div>

                                                </div>

                                                <div class="w-3/12">

                                                    <div class="text-small text-center">
                                                        To
                                                    </div>

                                                    <div class="flex justify-center items-center">
                                                        <input type="text" class="input input-sm " placeholder="Destination">
                                                        <button type="submit" class="fa fa-search ml-1" style="background-color: rgba(255, 255, 255, 0);border-color: rgba(255, 255, 255, 0)"></button>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="flex justify-center items-center">

                                                <div class="p-4">
                                                    <span>
                                                        Adult:
                                                    </span>

                                                    <div class="justify-center flex">

                                                        <form>
                                                            <select class="inputselect inputselect-sm">
                                                                <option value="">1</option>
                                                                <option value="">2</option>
                                                                <option value="">3</option>
                                                                <option value="">4</option>
                                                                <option value="">5</option>
                                                            </select>
                                                        </form>

                                                    </div>

                                                </div>

                                                <div class="p-4">
                                                    <span>
                                                        Children:
                                                    </span>

                                                    <div class="justify-center flex">

                                                        <form>
                                                            <select class="inputselect inputselect-sm">
                                                                <option value="">0</option>
                                                                <option value="">1</option>
                                                                <option value="">2</option>
                                                                <option value="">3</option>
                                                                <option value="">4</option>
                                                                <option value="">5</option>
                                                            </select>
                                                        </form>

                                                    </div>

                                                </div>


                                                <div class="p-4">
                                                    <span>
                                                        Infant
                                                    </span>

                                                    <div class="justify-center flex">

                                                        <form>
                                                            <select class="inputselect inputselect-sm">
                                                                <option value="">0</option>
                                                                <option value="">1</option>
                                                                <option value="">2</option>
                                                                <option value="">3</option>
                                                                <option value="">4</option>
                                                                <option value="">5</option>
                                                            </select>
                                                        </form>

                                                    </div>

                                                </div>


                                                <div class="w-1/5 flex justify-center  items-center  ">
                                                    <button onclick="window.location.href = '<?= root_path ?>/pages/bairline/roundtrip.php';" type="button" class="btn btn-sm btn-rounded btn-light mr-5 " value="Sign Up"><i class="fas fa-search"></i> Search</button>

                                                </div>

                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>



                        </div>

                    </div>

                </div>
            </div>

        </div>

    </div>

    <!-- datepicekr -->
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

    <script>
        // datepickr
        var dateelement = document.getElementById("datepickr2")
        dateelement.flatpickr({
            altInput: true,
            altFormat: "F j, Y",
            dateFormat: "Y-m-d",
            minDate: "today",
        })

        var dateelement = document.getElementById("datepickr3")
        dateelement.flatpickr({
            altInput: true,
            altFormat: "F j, Y",
            dateFormat: "Y-m-d",
            minDate: "today",
        })

        var dateelement = document.getElementById("datepickr4")
        dateelement.flatpickr({
            altInput: true,
            altFormat: "F j, Y",
            dateFormat: "Y-m-d",
            minDate: "today",
        })

        var dateelement = document.getElementById("datepickr5")
        dateelement.flatpickr({
            altInput: true,
            altFormat: "F j, Y",
            dateFormat: "Y-m-d",
            minDate: "today",
        })

        var dateelement = document.getElementById("datepickr6")
        dateelement.flatpickr({
            altInput: true,
            altFormat: "F j, Y",
            dateFormat: "Y-m-d",
            minDate: "today",
        })

        var dateelement = document.getElementById("datepickr7")
        dateelement.flatpickr({
            altInput: true,
            altFormat: "F j, Y",
            dateFormat: "Y-m-d",
            minDate: "today",
        })

        var dateelement = document.getElementById("datepickr8")
        dateelement.flatpickr({
            altInput: true,
            altFormat: "F j, Y",
            dateFormat: "Y-m-d",
            minDate: "today",
        })
    </script>

    <!--  vertical_tabs -->
    <script>
        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();
    </script>

    <!-- collapse -->
    <script>
        var coll = document.getElementsByClassName("collapsible1");
        var i;

        for (i = 0; i < coll.length; i++) {
            coll[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var content = this.nextElementSibling;
                if (content.style.display === "block") {
                    content.style.display = "none";
                } else {
                    content.style.display = "block";
                }
            });
        }
    </script>

    <script>
        function hiddRoom() {
            var x = document.getElementById("rooms");
            if (x.style.display === "none") {
                x.style.display = "block";
            } else {
                x.style.display = "none";
            }
        }
    </script>

</body>

</html>